package p1;

public class C1 {

	public void mVoid() {
		System.out.println("mVoid");
		System.out.println("mVoid");
		System.out.println("mVoid");
		System.out.println("mVoid");
		System.out.println("mVoid");
	}
	
	public int mInt() {
		System.out.println("mInt");
		System.out.println("mInt");
		System.out.println("mInt");
		System.out.println("mInt");
		return 13;
	}
	
	public void mArgs(String arg0, int arg1) {
		System.out.println("mArgString");
		System.out.println("mArgString");
		System.out.println(arg0);
		System.out.println(arg1);
	}

	public void m1() {
		System.out.println("m1");
		System.out.println("m1");
		System.out.println("m1");
		int x = this.mInt();
		System.out.println(x);
	}

	public void m2() {
		System.out.println("m2");
		System.out.println("m2");
		System.out.println("m2");
		this.mVoid();
		System.out.println("m2");
	}

	public void m3() {
		System.out.println("m2");
		System.out.println("m2");
		System.out.println("m2");
		if (true)
			this.mVoid();
		System.out.println("m2");
	}

	public void m4() {
		System.out.println("m4");
		System.out.println("m4");
		System.out.println("m4");
		this.mArgs(this.myString(), 2 + 3);
		System.out.println("m4");
	}

	public void m5() {
		System.out.println("m4");
		System.out.println("m4");
		System.out.println("m4");
		String myVar = this.myString();
		this.mArgs(myVar, 3);
		System.out.println("m4");
	}

	public void m6() {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("m6");
				System.out.println("m6");
				System.out.println("m6");
				String myVar = myString();
				mArgs(myVar, 3);
				System.out.println("m6");
			}
		};
	}

	private String myString() {
	    return "danilo";
    }

    static public ThreadLocal m7 = new ThreadLocal() {
        public Object initialValue() {
        	System.out.println("m7");
			System.out.println("m7");
			System.out.println("m7");
			staticMethod();
            return Boolean.FALSE;
        }
    };

    public static void staticMethod() {
    	System.out.println("staticMethod");
    	System.out.println("staticMethod");
    	System.out.println("staticMethod");
    	System.out.println("staticMethod");
    }
}
