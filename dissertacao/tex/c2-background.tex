\chapter{Background}
\label{background}

%Section~\ref{central_concepts} presents central concepts for the understanding of our approach, such as refactoring and recommendations systems.
In this chapter, we discuss background and work related to this dissertation.
First, Section~\ref{rsse} presents a brief introduction on \emph{Recommendation Systems for Software Engineering}, since our approach falls in this category of system.
Next, we present previous work in the area of refactoring recommendation systems, dedicating special attention to two tools: JDeodorant (Section~\ref{jdeodorant}), a state-of-the-art refactoring recommendation system that we compared our approach with, and JMove (Section~\ref{jmove}), a Move Method recommendation system that our technique for ranking Extract Method candidates is inspired by.
Section~\ref{others} briefly presents other approaches related to the identification of Extract Method refactoring opportunities.
Last, Section~\ref{backgroundremarks} concludes this chapter with some final remarks.
%summarizing the main differences and similarities between our approach and the state-of-the-art in the area.

%We start by presenting central concepts in Section~\ref{central_concepts}. First, we discuss the definition of refactoring and, more specifically, the definition of Extract Method refactoring, which is the main focus of this dissertation. Second, we briefly discuss the topic of Recommendation Systems for Software Engineering, since our approach falls in this category of tool.
%In Section~\ref{related}, we present previous work in the area of refactoring recommendation systems. We dedicate more attention to two tools, since they are closely related to this work: The first is JDeodorant (Section~\ref{jdeodorant}), a state-of-the-art refactoring recommendation system that we compared our approach with. The second is JMove (Section~\ref{jmove}), a Move Method recommendation system. Although JMove is not related to Extract Method refactoring, our approach is inspired by its technique for computing the similarity between methods of a system. Besides these two tools, we briefly present other approaches related to the identification of refactoring opportunities (Section~\ref{others}). Last, Section~\ref{backgroundremarks} concludes this chapter summarizing the main differences and similarities between our approach and the state-of-the-art in the area.


%\section{Central Concepts}
%\label{central_concepts}

%This section presents central concepts related to this work. First, we discuss the definition of refactoring and, more specifically, the definition of Extract Method refactoring, which is the main focus of this dissertation. Second, we briefly discuss the topic of Recommendation Systems for Software Engineering, since our approach falls in this category of tool.



\section{Recommendation Systems for Software Engineering}
\label{rsse}

Recommendation Systems for Software Engineering (RSSEs) is an emerging research area~\citep{rsse2010}. A RSSE is a software application that provides potential valuable information for a software engineering task in a given context. For example, the solution we propose in this dissertation provides information to aid the task of decomposing a method into smaller parts, in the form of code fragments recommended for extraction.
Moreover, such systems may employ a variety of techniques, e.g., there are approaches based on search algorithms~\citep{keeffe06, Seng2006}, Relational Topic Model (RTM)~\citep{methodbook_tse}, metrics-based rules~\citep{Marinescu:2004:DSM:1018431.1021443}, clustering~\citep{nicolas99, bunch}, etc.

Finally, RSSEs can help developers in a wide range of activities, besides refactoring-related tasks. For example, current RSSEs can recommend relevant source code fragments to help developers to use frameworks and APIs~(CodeBroker~\citep{codebroker}, Strathcona~\citep{contextmatching}, and APIMiner~\citep{joao_wcre12}), software artifacts that must be changed together~(eRose~\citep{guidechanges}), parts of the software that should be tested more cautiously~(Suade~\citep{suade}), and tasks for repairing software architectures (ArchFix~\citep{spe2013}).


%\section{Related Work}
%\label{related}

%\textcolor{red}{TODO}


\section{JDeodorant}
\label{jdeodorant}
% 4 pg

% plug-in for Eclipse IDE that is able to suggest
JDeodorant is a RSSE that suggests and applies refactoring operations on Java systems, aiming to solve common code smells, such as \emph{Feature Envy}, \emph{Long Method}, and \emph{God Class}~\citep{movemethod, tsantalis11, jdeodorant3}. Specifically, JDeodorant employs an automated approach to identify Extract Method refactoring opportunities, proposed by \cite{tsantalis11}.
Since existing IDEs focus only on automating the extraction of statements indicated by the developer, the authors aim to fill the gap by recommending code fragments that could benefit from decomposition.\\
% \cite{tsantalis11} proposed a methodology to identify opportunities to decompose long methods and integrated it to the JDeodorant tool.

{\noindent \bf Identification of Code Slices:} JDeodorant employs backward slicing, a static analysis technique to identify the slice of code that may affect a variable at a given point. This technique relies on the Program Dependence Graph (PDG) to represent the methods under analysis. In a PDG, nodes represent statements and edges represent dependencies (control or data).
Therefore, backward slicing consists on selecting statements connected in the PDG, starting with a set of seed statements. %, which are referred as the slicing criterion.

The identification of data dependencies is crucial to generate precise and correct slices, as well as to preserve program's behavior after code extraction. Thus, the authors employ a variety of program analysis techniques to construct the PDG, considering issues such as polymorphic method calls, unstructured control flow, exceptions, and aliasing.

While traditional slicing algorithms consider the entire method body as a region where the slice may expand, the authors adopt the concept of block-based slicing, introduced by \cite{maruyama2001}. By employing this concept, it is possible to produce more than one slice for a given slicing criterion, constrained by different block-based regions of a method.
The authors propose two algorithms, based on different criteria, to select the slices to be extracted:
\begin{itemize}
  \item {\em Complete computation slice:} It generates a slice that modifies the value of a variable. The statements that assign a value to a given variable are initially selected as seeds, which are expanded using backward slicing.
  
  \item {\em Object state slice:} It generates a slice that affects the state of an object reference. The statements that affect the state of the object are selected as seeds, which are also expanded using backward slicing.
\end{itemize}
Figure~\ref{fig:slices} exemplifies both slicing criteria, the first using variable \mcode{dy} and the second using the object reference \mcode{fold}. 
By selecting such slices, it is possible to decompose the method by applying an Extract Method refactoring.
%Embora esses critérios sejam capazes de extrair métodos coesos, eles não são suficientes para cobrir todas as possibilidades de decomposição de método.

\begin{figure}[htb]
\centering
\vspace{0.5cm}
\includegraphics[width=1\textwidth]{img/jdeodorant-slices.png}
\vspace{-0.5cm}
\caption{\emph{Complete computation slice} and \emph{Object state slice} examples}
\label{fig:slices}
\end{figure}

It is worth noting that the selected slices may contain statements that cannot be removed from the original method, for example, when they are also required for the computation of other variable or other object states. In this case, these statements must be duplicated, i.e., they should be present both in the extracted method and the original method.
However, such duplication may not preserve behavior. For this reason, the authors defined a set of rules to prohibit harmful duplication scenarios. For example, a statement that modifies the state of an object cannot be duplicated.\\

{\noindent \bf Evaluation \#1:} The authors considered several aspects of the recommendations, such as precision, usefulness, behavior-preservation, and impact on source code quality metrics. In a first experiment on JFreeChart system, an independent expert answered the following questions regarding the recommendations triggered by JDeodorant:
\begin{enumerate}
  \item \emph{\small{Does the code fragment suggested to be extracted as a separate method have a distinct and independent functionality compared to the rest of the original method? %If yes, describe its functionality by providing the name of the extracted method. If no, provide the reason for which the refactoring suggestion is not acceptable.
}}
  \item \emph{\small{Does the application of the suggested refactoring solve an existing design flaw (e.g., by decomposing a complex method, removing a code fragment that is duplicated among several methods, or extracting a code fragment suffering from Feature Envy)?}}
\end{enumerate}

From the 64 evaluated recommendations, the expert agreed on 57~{\small(89\%)} in the first question and on 27~{\small(42\%)} in the second question. From such results, the authors argued that the extracted methods showed to be useful and cohesive. For the behavior preservation concern, the authors relied on the existing automated tests, which covered 41 from the 64 triggered recommendations. All tests ran without errors after the application of the suggested refactoring opportunities, which indicates with a relative certainty that no bugs were introduced by the code transformations, an essential premise for refactoring.

Furthermore, the authors verified that the recommended refactorings had a positive impact on the slice based cohesion metrics proposed by \cite{ott1993}, namely \emph{Overlap}, \emph{Tightness}, and \emph{Coverage}.
Specifically, the authors applied the refactorings approved by the expert evaluator and compared: (i) metric values before the refactoring and (ii) average metric values for the original and extracted method after the refactoring. The average increase was $+0.303$ for \emph{Overlap}, $+0.319$ for \emph{Tightness}, and $+0.113$ for \emph{Coverage}.\footnote{Metric values range over the $[0, 1]$ interval.} \\

{\noindent \bf Evaluation \#2:} In this evaluation, the suggestions triggered by the tool were contrasted with suggestions identified by expert developers.
More specifically, two experts analyzed the source code of two systems (they had previously worked on) to identify method decomposition opportunities, which in this case were considered the ideal answer. Thereby, the experts received as input only a list of methods to analyze, which was a sample of the methods that had at least one recommendation triggered by JDeodorant. %However, the experts had no previous knowledge of the approach or the output of the tool.
In this scenario, JDeodorant achieved the precision and recall of 51\% and 69\%, respectively. Therefore, the authors concluded that the tool is able to identify refactoring opportunities that are usually found by human experts.



\section{JMove}
\label{jmove}

%The scoring strategy we adopted in our Extract Method recommendation approach is influenced by the work of \cite{jmove}, that proposed the JMove tool.
JMove is a RSSE, proposed by \cite{jmove}, that recommends Move Method refactoring opportunities for Java systems, i.e.,~it detects methods located in incorrect classes and then suggests moving such methods to more suitable ones.
JMove is influenced by the work of \cite{movemethod} that follows a classical heuristic to detect \emph{Feature Envy} bad smells: a method $m$ envies a class $C'$ when $m$ accesses more services from $C'$ than from its own class.
However, the authors proposed a novel heuristic to detect misplaced methods, based on the similarity of dependency sets.\\

{\noindent \bf Dependency Sets:} The proposed technique first retrieves the set of structural dependencies established by a given method~$m$ located in a class~$C$.
The set of structural dependencies, which are used to compute the similarity, includes the types that are referenced in the source code. A method establishes a dependency on a certain type $C$ when:
\begin{itemize}
\item Calls a method defined in type $C$.
\item Accesses a field defined in type $C$.
\item Instantiates an object of type $C$.
\item Declares a variable or parameter of type $C$.
\item Declares $C$ as the return type of a method.
\item Handles an exception of type $C$.
\item Annotates the code with an annotation type $C$.
\end{itemize}

Next, it computes two similarity coefficients: (a)~the average similarity between the set of dependencies established by $m$ and by the remaining methods in $C$; and (b)~the average similarity between the dependencies established by $m$ and by the methods in another class $C_i$. If the similarity measured in step (b) is greater than the similarity measured in (a), it is inferred that $m$ is more similar to the methods in $C_i$ than to the methods in its current class~$C$. Therefore, $C_i$ is a candidate class to receive $m$.
By computing this similarity for every class $C_i$ of the system, the tool is able to suggest the best class to receive the method.\\
%We adapted the idea of using the similarity between entity sets to our Extract Method recommendation approach, applied in the context of computing the dissimilarity between code fragments. However, besides including types, we extended the notion of dependencies to include local entities, such as variables and parameters, and higher level entities, such as packages.

% \cite{jmove}
{\noindent \bf Similarity Coefficients:} To measure the similarity between the sets of dependencies established by two methods the authors rely on similarity coefficients, which are usually employed to measure the similarity between two generic sets. The authors investigated the use of 18 different similarity coefficients, using JHotDraw system, to choose the most suitable one. As the result, they decided for the use of \emph{Sokal and Sneath 2} coefficient.\\
% We conducted a similar study to investigate the impact of different similarity coefficients in our approach, but \emph{Kulczynski} showed to be the best option in our scenario.\\


{\noindent \bf Evaluation:} The authors evaluated the approach using 14 open-source Java systems. They introduced Move Method opportunities in those systems by applying random Move Method operations. More specifically, they randomly selected a sample of 3\% of the classes in each system and manually moved a method of them to new classes, also randomly selected. After that, the authors compared the set of suggestions triggered by \emph{JMove} with the set of synthesized Move Method instances, achieving an average precision of 60.63\% and an average recall of 81.07\%.
%By contrasting the findings of the tool with the set of synthesized Move Method instances they reported an average precision of 60.63\% and an average recall of 81.07\%, achieving an improvement over JDeodorant, the tool used as a baseline.
 %Such results are, respectively, 129\% and 49\% better than the results achieved by JDeodorant, the tool used as a baseline.


\section{Other Approaches}
\label{others}

%Besides the aforementioned approaches, we can find a variety of studies in the literature related to Extract Method identification or application. In this section we present some of them, 

\subsection{Slicing and PDG Based Code Extraction}

%\cite{object_slicing} We have presented an approach to extend the SDG to represent object-oriented programs. Compared to other existing approaches, our representation support more precise slicing: it distinguishes data members belonging to different objects; it represents data members even when objects are used as parameters; it considers the effect of polymorphism on callsites and parameters

% System Dependence Graph (SDG) proposed by \cite{horwitz1990interprocedural}
%SDG para OO \citep{larsen1996slicing, tonella1997flow, object_slicing}

\cite{weiser1981program} formally defines slicing as a method for automatically decomposing programs by analyzing their data and control flow, represented usually in a Program Dependency Graph (PDG).
Most studies found in the literature for function (or method) extraction are based on the concept of program slicing.
While not a comprehensive list, we may cite the work of \cite{gallagher1991using, cimitile1996specification, lanubile1997extracting, lakhotia1998, maruyama2001, komondoor2003effective, harman2004amorphous, abadi2009}; and \cite{ettinger_ecoop}. Nevertheless, these approaches extract code slices based on some kind of user input (e.g., a certain variable or point of interest in the code). As presented in Section~\ref{jdeodorant}, JDeodorant~\citep{tsantalis11} is also centered on slicing. However, in contrast to the aforementioned approaches, it provides automated identification of Extract Method refactoring opportunities.

%\cite{gallagher1991using} introduce the concept of decomposition slice as a slice that captures all computation on a given variable

%\cite{cimitile1996specification} proposed a specification driven slicing process for identifying reusable functions based on the precondition and postcondition of a given function

%\cite{lanubile1997extracting} introduced the notion of transform slicing as a method for extracting reusable functions. A transform slice includes the statements which contribute directly or indirectly to the transformation of a set of input variables into a set of output variables.

%\cite{lakhotia1998} proposed a transformation, called Tuck, which can be used to restructure a program by breaking its large functions into smaller ones. The tuck transformation consists of three steps: Wedge, Split, and Fold.

%\cite{maruyama2001} simplified an interprocedural slicing algorithm proposed by Larsen and Harrold (1996) by making it intraprocedural and then introduced the concept of block-based region into the resulting algorithm.

%\cite{komondoor2003effective} proposed an algorithm that takes as input the control flow graph of a procedure and a set of statements to be extracted (marked statements) and applies semantics-preserving transformations to make the marked statements form a contiguous, well-structured block that is suitable for extraction.

%\cite{harman2004amorphous} Harman et al. (2004) introduced a variation of the algorithm proposed by Komondoor and Horwitz (2003) which is based on amorphous procedure extraction


%For example, \cite{abadi2009, extract_method_abadi2} proposed the use of fine slicing to support the Extract Method refactoring. The method computes executable program slices that can be finely tuned, and can be then used to extract non-contiguous fragments of code.
%Software evolution often requires the untangling of code. Particularly challenging and error-prone is the task of separating computations that are intertwined in a loop. The lack of automatic tools for such transformations complicates maintenance and hinders reuse. We present a theory and implementation of fine slicing, a method for computing executable program slices that can be finely tuned, and can be used to extract non-contiguous pieces of code and untangle loops. Unlike previous solutions, it supports temporal abstraction of series of values computed in a loop in the form of newly-created sequences. Fine slicing has proved useful in capturing meaningful subprograms and has enabled the creation of an advanced computation-extraction algorithm and its implementation in a prototype refactoring tool for Cobol and Java.

While not directly based on slicing, \cite{sharma2012} also proposes an approach to identify Extract Method refactoring opportunities. Specifically, the author employ a \emph{longest edge removal algorithm} in a Data and Structure Dependency (DSD) graph, which is a combination of a data flow graph and a structure dependency graph. In a DSD graph, edges may represent: (i) data dependencies or (ii) structural dependencies, i.e.,~a dependency between a statement and its enclosing block statement (e.g.,~\mcode{if}, \mcode{for}, \mcode{while}, etc.).
The author proposes a technique to partition the DSD graph, forming disconnected sub-graphs that correspond to code fragments candidates for extraction. Specifically, the approach employs a heuristic to remove edges iteratively from the DSD graph. This heuristic is centered on the idea that two statements connected by a data dependency edge and placed closer in a DSD graph are likely to be related than two connected statements that are placed farther in the DSD graph. Therefore, the longer the edge, the lower the likelihood that the connected nodes should reside in a single method.


\subsection{Visualization Based Approaches}

Visualization techniques may be employed to aid the identification of Extract Method opportunities. For example, \cite{kanemitsu2011} propose a visual representation of the PDG of a method that includes the notion of the weight of edges, which reflects as the distance between nodes.
The authors define the distance between every pair of nodes having a data dependency in terms of three possible scenarios:
\begin{itemize}
\item \emph{Atomic Data Dependency:} A statement defines (initializes) a variable, and a single statement references the variable.
\item \emph{Spread Data Dependency:} A statement defines a variable, and many other statements reference the variable.
\item \emph{Gathered Data Dependency:} A statement references many variables that are defined in other statements.
\end{itemize}
A screenshot of their supporting tool, called ReAF, is presented in Figure~\ref{img_kanemitsu_reaf}.
ReAF allows developers to visualize the PDG and select nodes for extraction.
ReAF can also automatically merge nodes in the PDG visualization taking as input a threshold value for the weight of the edges. More important, these merged nodes corresponds to Extract Method refactoring opportunities.
%Besides, it is possible to control the weights used to compute the distance for each of the three types of relationships.

\begin{figure}[htpb]
%\centering
\includegraphics[width=1.0\textwidth]{img/reaf.png}
\vspace{-0.5cm}
\caption{Screenshot of ReAF tool \citep{kanemitsu2011}}
\label{img_kanemitsu_reaf}
\end{figure}


Similarly, \cite{kaya2013} propose an approach, based on a treemap visualization, that uses density of variable references in code fragments to identify main tasks that could be extracted from large methods, composed of several blocks. Their treemap visualization tool helps developers to observe and analyze suggested refactorings.
In the proposed technique, an input method is represented with a placement tree where each node represents a different scope. Scopes are defined by all code enclosed within braces. A scope may include children scopes, forming a hierarchical structure.
The authors define the notion of dominant variables, which are the most referenced ones inside a particular scope. A single dominant variable is assigned for each scope using a set of proposed rules to cover the cases of ties.
In the proposed visualization, illustrated in Figure~\ref{img_kaya_treemap}, the nodes of the treemap are colored according to their dominant variable. %a different color is assigned to each variable and 

Moreover, the authors propose the extraction of code when any of the two following patterns are observed: (i) a large code fragment with a color different from its parent and (ii) consecutive sibling nodes with the same color.
The authors evaluated their tool on C++ systems and reported cases of code fragments extracted from long methods with the aid of their visualization approach.

\begin{figure}[htpb]
%\centering
\includegraphics[width=1.0\textwidth]{img/kaya-treemap.png}
\vspace{-0.5cm}
\caption{Treemap visualization proposed by \cite{kaya2013}}
\label{img_kaya_treemap}
\end{figure}


\subsection{Combining Structural and Linguistic Information}

Another line of research aims to combine structural and linguistic information extracted from source code to identify groups of related statements. \cite{sridhara2011} propose an approach to: (i) automatically identify code fragments that represent a high level action and (ii) describe it using natural language. Their technique may be applied, for example, to automatically generate descriptive source code comments.
The authors propose a set of heuristics, considering structural and linguistic information, to group related statements that fall into one of the following patterns: %Their approach relies on the Software Word Usage Model (SWUM), which summarizes structural and linguistic information extracted from source code. Similar statements constituting predefined patterns are then grouped and summarized by applying a set of proposed rules to create a natural language description.
\begin{enumerate}
\item A sequence of similar statements that represents a single action.
\item A conditional block that performs an action with subtle variations based on its condition.
\item Loop constructs that implement a set of predefined actions, e.g.,~find an element in a collection.
\end{enumerate}
For each recognized pattern, the authors define specific rules to generate a phrase, in natural language, describing the action.
The authors evaluated their approach with a sample of 75 summarized code fragments and 15 human evaluators, reporting that: (i) in 94.6\% of the cases evaluators agreed that the identified code fragments actually represented a high level action; and (ii) in 89.7\% of the cases evaluators agreed that the description generated by the approach was appropriate.

%To evaluate their approach, the authors investigated 1,000 open-source Java programs, confirming that the proposed patterns are frequently identified. Moreover, it was possible summarize code fragments to natural language descriptions of about 25\% of their original size. More important, a study with 

%Moreover, 75 summarized code fragments were analyzed by independent evaluators to asses that: (i) the identified code fragments actually represented a high level action; and (ii) the description generated by the tool was appropriate for the code fragment. The authors reported for these two questions 94.6\% and 89.7\% of positive answers, respectively.

Although the authors proposed their technique originally for automatic documentation generation, they also mention other usage scenarios, including the identification of Extract Method opportunities. However, its applicability is limited to the patterns the technique implements.


\cite{wang2014} presents the SEGMENT tool, which also combines structural and linguistic information.
SEGMENT separates meaningful blocks within a method, by inserting blank lines, to increase readability.
The tool uses a heuristic based on both program structure and naming information to identify meaningful blocks, i.e., consecutive statements that logically implement a high level action.
The proposed approach is composed by the following phases:
\begin{enumerate}
\item \emph{Identify Initial Blocks:} It groups related statements to form blocks of code. These statements may be related by: (i)~syntactical similarities (SynS blocks); (ii)~a data-flow chain (DFC blocks); and (iii)~compound statements including \{\mcode{switch}, \mcode{while}, \mcode{if}, \mcode{for}, \mcode{try}, \mcode{do}, \mcode{synchronized}\} (E-SWIFT blocks).

\item \emph{Remove Block Overlap:} It joins consecutive overlapping blocks according to a set of proposed rules.

\item \emph{Refine SynS Blocks:} It splits large SynS blocks using additional information extracted from word usage and naming conventions.

\item \emph{Merge Short Blocks:} It merges very small blocks or individual statements with other blocks, also using linguistic information. %This phase segments large SynS blocks depends on (1) the length of the block and (2) whether there exist consecutive subsequences with different similarity levels
\end{enumerate}
In the final output, blank lines are inserted between each identified block, aiding the comprehension of the code. The authors report that SEGMENT’s effectiveness from a code reader’s perspective achieved a precision of 66\% and a recall of 87.6\%. Although their approach does not focus on the identification of Extract Method opportunities, their technique can be adapted to it.

%This suggests that SEGMENT is good at inserting blank lines in the correct locations but appears to be over-inserting blank lines according to newcomers reading the code.


\subsection{Leveraging Current Refactoring Tools}

Besides automated identification of Extract Method opportunities, some studies focus on issues of current refactoring tools.
For example, \cite{barriers_murphy} reported a study to investigate the usability of current refactoring tools.
The authors observed several usability issues in a study where 11 experienced programmers performed a number of Extract Method refactoring tasks using Eclipse IDE.
Specifically, the study revealed that: (i) programmers need support in making a valid selection to prevent errors; and (ii) programmers need expressive and understandable feedback to recover from violated preconditions.
As result of this study, the authors defined a set of guidelines that could improve the user experience using refactoring tools. Moreover, the authors designed three refactoring assisting tools according to the proposed guidelines to demonstrate how speed, accuracy, and user satisfaction can be significantly increased.
% However, the described tools do not aim to automate the identification of Extract Method refactoring opportunities.

On the other hand, \cite{reconciling_refactoring} tackle the automatic refactoring tools underuse problem. The authors found that one cause of this problem is that  developers sometimes fail to recognize that they are going to refactor. For this reason, the authors propose \mbox{BeneFactor}, a refactoring tool that detects developers' manual refactoring, reminds them that automatic refactoring is available, and can automatically complete their refactoring. Specifically, they rely on a set of manual refactoring workflow patterns, including those frequently adopted by developers when performing Extract Method, to detect such scenarios.



%\subsubsection{Search-Based Remodularization Approaches}

%One possible way to handle the problem of software remodularization is using search-based approaches. \cite{keeffe06} propose the \emph{Code-Imp} tool, that is a platform to execute automatic refactoring-based search in Java systems, i.e., refactorings are applied to explore a search space of possible system designs. The goal is to maximize the value of an objective function, which is a combination of 27 source code quality metrics. The tool supports traditional strategies such as \emph{hill-climbing} and \emph{simulated annealing} to search for a solution. The authors evaluated the approach with 6 kinds of refatoring related to inheritance, but the tool is general enough to be extended with other refactorings.
%A related approach proposed by \cite{Seng2006} also faces software remodularization as a search problem. However, the authors employ an evolutionary algorithm to search for solutions. In evolutionary algorithms, each solution is considered an individuals. An initial population is randomly generated and evolve through a series of generations, combining and mutating their genes to become better fitted individuals. In the proposed approach, a sequence of refactoring operations is mapped to an individual, and the fitness functions is a source code quality metric. The authors evaluated their approach with the Move Method refactoring, but it may also be extended with other refactorings. One problem regarding search-based remodularization approaches is that the quality of the solution relies on the ability of the objective function to indicate the quality of the modularization of the system, which is usually hard to express.


%\subsubsection{Remodularization Approaches Using Clustering}

%Clustering is frequently used to generate visualizations or propose the remodularization of software systems. Such solutions usually focus on higher-level entities entities of the system, e.g., classes, packages and modules. \cite{nicolas99} report a study of the use of clustering in software remodularization, evaluating different algorithms and metrics. \cite{bunch} present the \emph{Bunch}, that models the system as a graph of entities an decompose it in partitions.

%\subsubsection{Architectural Recovery}

%Other existent line of research is focused on using refactoring to repair the architecture of a system. \cite{spe2014} present a tool that receives as input a set of architectural violations e suggests a set of refactorings to fix then. In this approach, it is first necessary to run an architectural conformance check, specifying a set of architectural rules, to identify violations in the subject system. Then, the tool analyzes the violations and tries to map then to any of the 32 different refactorings proposed by the authors.


\section{Final Remarks}
\label{backgroundremarks}

%The approach we propose in this dissertation, which we introduced in Section~\ref{sec:approach_overview}, differs from traditional approaches in two main points:
This chapter presented an overview of the state-of-the-art in refactoring recommendations system, focusing on Extract Method refactorings.
Even though most approaches are based on the concept of slicing and PDGs, such as JDeodorant, recent studies also explore structural and linguistic information, visualization techniques, and density of variable references.

In the next chapter, we detail our Extract Method refactoring recommendation approach. However, we can already outline two main differences from the state-of-the-art approaches:
\begin{enumerate}
\item We do not rely on slicing techniques to identify related statements to be extracted. Rather, we rely on the similarity between dependency sets, influenced by JMove's technique to measure similarity between methods. Nevertheless, we extended JMove's notion of dependencies to better fit our problem.

\item We propose a heuristic for ranking Extract Method opportunities, i.e., we define a scoring function that aims to express the relevance of each opportunity.
\end{enumerate}

%On the other hand, our scoring function is influenced by JMove's technique to measure similarity between methods.
%Specifically, to identify Extract Method refactoring opportunities, we claim that the extracted statements and the remaining statements should be as dissimilar as possible. Therefore, we adapted the idea of using the similarity between dependency sets and extended the notion of dependencies to better fit our problem.
