\chapter{Evaluation}
\label{sec_evaluation}

In Chapter~\ref{sec:approach}, we presented our Extract Method refactoring recommendation approach and reported an initial exploratory study to calibrate our technique. 
In this chapter, we evaluate the proposed approach by investigating two additional research questions:\\


\newcommand{\rqv}{How does our approach perform when compared to state-of-the-art ones?}

\newcommand{\rqvi}{How does our approach perform when evaluated with other systems?}

\noindent {\textbf{ \em RQ \#5}} -- \rqv\\

\noindent {\textbf{ \em RQ \#6}} -- \rqvi\\

%\begin{enumerate}
%\item How does our approach perform compared to state-of-the-art ones?
%\item How does our approach perform on other systems?
%\end{enumerate}
To answer {\textbf{\em RQ \#5}}, we compare the results of our tool with JDeodorant and discuss their differences (Section~\ref{sec_compare}). Besides MyWebMarket, we included two other systems in this comparison (JUnit and JHotDraw).
To answer {\textbf{\em RQ \#6}}, we present a quantitative study using a set of 1,182 well-known Extract Method instances from 13 open-source systems (Section~\ref{sec_13s}).
Last, we conclude this chapter with final remarks (Section~\ref{eval_final_remarks}).



\section{Study 1: Comparative Evaluation}
\label{sec_compare}

In this study, we investigate how our approach performs in comparison with existing ones, focusing on recall and precision.
In order to address this question, we conducted a study with three systems: MyWebMarket, the system we previously used in the exploratory study, and two well-known open-source systems, JUnit and JHotDraw.
Unlike MyWebMarket, we did not have an oracle of Extract Method instances for JUnit and JHotDraw. Therefore, we adopted a strategy to synthesize such oracle by applying \emph{Inline Method} refactoring operations.
The remainder of this section describes the study design (Section~\ref{sec_study1design}) and discusses the results (Section~\ref{sec_study1results}).

%Using these versions, we report our results in terms of recall and precision.


\subsection{Study Design}
\label{sec_study1design}

\subsubsection{Selected Tools for Comparison}

%First, we selected the approaches to use for comparison. While there are numerous approaches in the literature related to code extraction, we are constrained to the following requirements:
Even though the large number of approaches in the literature related to code extraction, we compare our approach with those that satisfy the following requirements:

\begin{itemize}
\item \emph{Fully Automated:} The approach should provide Extract Method refactoring suggestions with no user input or interaction.
\item \emph{Tool Availability:} There must be a publicly available supporting tool.
\item \emph{Java Support:} Since our evaluation relies on Java systems, the tool must support such language.
\end{itemize}
We only considered JDeodorant in this study because it is the only tool that fulfills such requirements.
%JDeodorant is the only approach that we could find that fulfills these requirements. Thus, we only included JDeodorant in this study.

\subsubsection{Target Systems}

We included three systems in this study: MyWebMarket, \mbox{JUnit} (version 3.8), and \mbox{JHotDraw} (version 5.2).
In order to measure recall and precision, we check the suggestions reported by each tool against an oracle, which includes all relevant {\em Extract Method} opportunities for a given method. For this reason, we considered again MyWebMarket to take advantage of the existing oracle. On the other hand, for JUnit and JHotDraw, we synthesized the oracles, as detailed in the next section.

It is worth noting that we specially chose JUnit and JHotDraw for a specific reason. Since they have been designed and implemented by expert developers (especially in the earlier versions we selected), we may assume, with relative certainty, that there is no relevant {\em Extract Method} instances in these systems. This property is important because existing {\em Extract Method} instances would make our oracle incomplete.

\subsubsection{Oracle Construction}
\label{oracle_gen}

To construct an oracle for JUnit and JHotDraw, we judiciously applied {\em Inline Method} refactoring operations in order to create system's versions with well-known {\em Extract Method} instances.
This strategy is based on the assumption that \emph{when a certain method~$m$ invokes $m'$ and the computation performed by $m'$ is inlined into $m$, an Extract Method opportunity is introduced in $m$}. In other words, we assume that the inlined code fragment is expected to be found by tools that identify {\em Extract Method} refactoring opportunities.

We followed these steps to construct the oracle:
\begin{enumerate}

\item We retrieved all methods of the target systems with more than three statements ({\em minimum size threshold $K$}, as recommended by the exploratory study described in Section~\ref{sec:exploratory_study}).

\item For each method $m$ retrieved in Step~1, we retrieved all methods it invokes. Each of these methods, which we denote by $m'$, should satisfy the following preconditions:
\begin{itemize}
\item The size of $m'$ must be at least three statements, i.e., $|m'| \geq 3$, in conformance to the \emph{minimum size threshold} value. Moreover, this rule avoids methods that do not contain complex logic (e.g., getter, setter, and delegate methods).

\item The size ratio between both methods (i.e., $|m'|/|m|$) should lie in the range $[0.1, 2]$. This rule avoids some extreme cases where the inlined method is very small ($|m'|/|m| < 1/10$) or very large ($|m'|/|m| > 2$) when compared to the invoker. Thereupon, we increase the chance of significant Extract Method refactoring opportunities.

\item The \emph{Inline Method} preconditions of the IDE refactoring tool must be respected when inlining the invocation of $m'$.

\item Method $m'$ cannot be one of the methods previously modified by this process, i.e., if a method $m_1$ is inlined into a method $m_2$, then $m_2$ cannot be inlined into another method.
\end{itemize}

\item From the list of method invocations computed at Step~2, we selected a single invocation to apply an {\em Inline Method} refactoring. We gave precedence to methods implemented in other classes and, as a second criterion, to larger methods.

\item The selected invocation was inlined using the IDE refactoring tool and the corresponding Extract Method instance (that reverts the inline) is registered in the oracle.

\end{enumerate}

Table~\ref{tb:systems} presents the number of Extract Method instances in our oracle (25 for JUnit and 56 for JHotDraw). As can be observed, we generated valid instances for 26.0\% and 25.2\% of candidate methods in JUnit and JHotDraw, respectively (i.e.,~methods with at least six statements, assuming the minimum size threshold $K = 3$).

\begin{table}[!ht]
\renewcommand{\arraystretch}{1.5}
\centering
\caption{Study 1: Target systems}
\begin{tabular}{lrrr}
\hline %\\ [-0.2cm]
System       & Oracle size & Total methods & $\geq {2}{\times}{K}$   \\ %[0.05cm]
\hline %\\ [-0.2cm]
JUnit 3.8    & 25 {\small (26.0\%)}  & 470           & 96               \\ %[0.2cm]
JHotDraw 5.2 & 56 {\small (25.2\%)}  & 1,478         & 222              \\ %[0.1cm]
\hline
\multicolumn{4}{l}{\scriptsize Method size must be at least $2 \times K$ to produce candidates.}\\ %[-0.3cm]
\end{tabular}
\label{tb:systems}
\end{table}%


\subsubsection{JExtract Setup}

In this study, we used the default settings of JExtract (as presented in Table~\ref{tab_jextract_settings}), except for parameter {\em Maximum Recommendations per Method}, for which we compared three different settings, namely Top~1, Top~2, and Top~3.
Top-$n$ means that in each valid candidate method JExtract triggers $n$ recommendations at maximum.

\begin{table}[!ht]
\renewcommand{\arraystretch}{1.5}
\centering
\tabcolsep=0.35cm
\caption{Study 1: JExtract Settings}
\begin{tabular}{ll}
\hline
Parameter       & Value \\
\hline %\\ [-0.2cm]
{\em Minimum Statements} & 3 \\
{\em Maximum Recommendations per Method} & 1, 2, and 3\\
{\em Minimum Score Value} & 0.0 \\
{\em Ignore Type List} & \texttt{"java.lang.*,java.util.*"} \\
{\em Ignore Package List} & \texttt{"java,javax,com,org,br"} \\
\hline
\end{tabular}
\label{tab_jextract_settings}
\end{table}








\subsubsection{JDeodorant Setup}

In this study, we used the default settings of JDeodorant, except for the following parameters:
\begin{itemize}
\item {\em Minimum Number of Slice Statements}, which was set to $3$ to be consistent with the setup of our approach ({\em minimum size threshold $K$}).

\item {\em Minimum Number of Statements in Method}, which was set to $6$ because, when $K = 3$, our approach does not generate suggestions for methods with less than $2 \times K$ statements.

\item {\em Maximum Number of Duplicated Statements}, which was set to $0$ because: (i)~we knew in advance that our oracle does not include any suggestion that involves duplicating statements and (ii)~our approach does not suggest duplicating statements.
\end{itemize}

It is important to mention that, in accordance to JDeodorant’s documentation,
we attached the source code of all APIs the target systems depend on.
For instance, we even included the source code of Java API.

%Moreover, we followed the instructions in JDeodorant's documentation to attach in Eclipse IDE the source code of the APIs required by the target systems, which includes the Java API.
 %prepared the systems in Eclipse IDE by attaching the source code of the Java API and dependent libraries of each project, as recommended in JDeodorant's documentation.

\subsection{Results and Discussion}
\label{sec_study1results}

We ran both tools on the three aforementioned systems to collect their recommendations. Table~\ref{tab_rq1} reports recall and precision values achieved by JExtract (separated into three configurations: Top~1, Top~2, and Top~3) and JDeodorant. In the first and second columns of the table, we display the system and the number of Extract Method instances in the oracle, respectively. In the other columns, we repeat for each tool: the number of relevant recommendations found (\#), recall (Rc.), and precision (Pr.).

\begin{table}[h]
\renewcommand{\arraystretch}{1.3}
\caption{Study 1: Comparing JExtract and JDeodorant}
\centering
\tabcolsep=0.16cm
{\footnotesize
\begin{tabular}{lr|rrr|rrr|rrr|rrr}
\hline
& & \multicolumn{9}{c|}{JExtract} & \multicolumn{3}{c}{JDeodorant} \\
 & & \multicolumn{3}{c}{Top~1} & \multicolumn{3}{c}{Top~2} & \multicolumn{3}{c|}{Top~3} &  \\
{System} & \multicolumn{1}{c|}{\#} & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c|}{Pr.} & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.} \\
\hline
JHotDraw 5.2 & 56 & 19 & 0.339 & 0.339 & 26 & 0.464 & 0.236 & 32 & 0.571 & 0.198 & 2 & 0.036 & 0.045 \\
JUnit 3.8 & 25 & 13 & 0.520 & 0.520 & 16 & 0.640 & 0.327 & 18 & 0.720 & 0.250 & 0 & 0.000 & 0.000 \\
MyWebMarket & 14 & 12 & 0.857 & 0.857 & 14 & 1.000 & 0.500 & 14 & 1.000 & 0.333 & 2 & 0.143 & 0.333 \\
\hline
Overall & 95 & 44 & 0.463 & 0.463 & 56 & 0.589 & 0.299 & 64 & 0.674 & 0.232 & 4 & 0.042 & 0.062 \\
\hline
\end{tabular}
\label{tab_rq1}
}
\end{table}

We can draw the following observations from these results:
\begin{itemize}
\item Our approach achieves the best results in MyWebMarket, i.e., 85.7\% recall using the Top~1 recommendation strategy. In \mbox{JUnit} and \mbox{JHotDraw}, on the other hand, we achieved a recall of 52\% and 33.4\%, respectively. This difference is somewhat expected since we based on the findings of the exploratory study with MyWebMarket to design and calibrate our approach.

\item JDeodorant could find 4 of the 95 Extract Method instances in the oracle. Therefore, its overall recall and precision were 4.2\% and 6.2\%.
\end{itemize}



%Our first observation may be explained by two facts: (i) we based on the findings of the exploratory study with MyWebMarket to design our approach, so its natural that it performs better on it; (ii) the Extract Method instances in MyWebMarket, in most of the cases, involve extracting code from different architectural layers. Therefore, we believe that such instances are more explicit are more strongly related


While analyzing the suggestions provided by JExtract and JDeodorant, we observed that some differ from the oracle's suggestion by including/excluding a single statement. To investigate how frequently such scenario occurs, Table~\ref{tab_rq1_delta} reports a second set of recall and precision values, using an oracle that tolerates a difference of one single statement from the expected answer.
In this scenario, there is a significant improvement in the results, specially for JDeodorant. For example, overall recall improves 34\% for JExtract Top~1 and more than 300\% for JDeodorant.

\begin{table}[htbp]
\renewcommand{\arraystretch}{1.3}
\caption{Study 1: Comparing JExtract and JDeodorant (tolerance of one statement)}
\centering
\tabcolsep=0.16cm
{\footnotesize
\begin{tabular}{lr|rrr|rrr|rrr|rrr}
\hline
& & \multicolumn{9}{c|}{JExtract} & \multicolumn{3}{c}{JDeodorant} \\
 & & \multicolumn{3}{c}{Top~1} & \multicolumn{3}{c}{Top~2} & \multicolumn{3}{c|}{Top~3} &  \\
{System} & \multicolumn{1}{c|}{\#} & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c|}{Pr.} & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.} \\
\hline
JHotDraw 5.2 & 56 & 30 & 0.536 & 0.536 & 36 & 0.643 & 0.445 & 40 & 0.714 & 0.401 & 11 & 0.196 & 0.250 \\
JUnit 3.8 & 25 & 15 & 0.600 & 0.600 & 20 & 0.800 & 0.571 & 21 & 0.840 & 0.569 & 5 & 0.200 & 0.333 \\
MyWebMarket & 14 & 14 & 1.000 & 1.000 & 14 & 1.000 & 0.857 & 14 & 1.000 & 0.595 & 2 & 0.143 & 0.500 \\
\hline
Overall & 95 & 59 & 0.621 & 0.621 & 70 & 0.737 & 0.540 & 75 & 0.789 & 0.475 & 18 & 0.189 & 0.292 \\
\hline
\end{tabular}
\label{tab_rq1_delta}
}
\end{table}


In summary, we conclude that, in this particular study, JExtract was significantly superior to JDeodorant, regardless of the configuration used (Top~1, Top~2, or Top~3).
The precise reason why JDeodorant was not able to find most of the recommendations in the oracle requires further investigation. However, it is worth noting that, in contrast to our approach, JDeodorant is able to extract non-contiguous code fragments, reordering and duplicating statements when necessary. Such scenarios are not contemplated in this study, but they are listed as future work in Section\ref{futurework}.


%JExtract conceptually differs from JDeodorant in two main points:
%\begin{itemize}
%\item Our algorithm for generating Extract Method candidates is not based on block based slicing. Our approach considers any range of contiguous statements for extraction, as long as they respect some proposed preconditions. Although this property implies that a larger number of possibilities are explored, it also implies that our approach can identify some opportunities that JDeodorant cannot.

%\item We are more conservative when enforcing rules to preserve program behavior, as statement reordering or duplication are never allowed. On the other hand, JDeodorant is able to extract non-contiguous code fragments, reordering and duplicating statements as necessary. Thus, JDeodorant also identifies opportunities that our approach cannot.
%While this characteristic is a drawback of our approach, it may also be an advantage in terms of soundness. Even with the sophisticated static analysis used in JDeodorant, some subtle dependencies between statements are not detected, for instance, due to interactions with external resources, such as files and databases.
%\item We introduce the notion of a scoring function, which considers the structural dependencies present in the code, and heavily rely on it to show relevant recommendations. In contrast, JDeodorant relies on slicing algorithms (\emph{complete computation slice} and \emph{object state slice}) to generate inherently meaningful recommendations, which are later ranked based only on the duplication ratio.
%\end{itemize}




\subsection{Threats to Validity}

There are two main threats regarding the validity of this study. First, as usual, we cannot extrapolate our results to other systems (external validity). Second, we cannot claim that the Extract Method instances we have based the evaluation on represent the whole spectrum of real refactoring instances normally performed by maintainers. However, we can at least assume that the earlier versions of JUnit and JHotDraw have a fairly good internal design and therefore most methods encapsulate a precise design decision. Therefore, we claim that our inlined Extract Methods denote code that should be refactored with a high confidence.





\section{Study 2: Quantitative Evaluation}
\label{sec_13s}

In this study, we investigate how our approach performs in a wider range of systems.
In order to address this question, we selected 13 well-known open-source Java systems. Similar to the strategy we used for JUnit and JHotDraw, we synthesize a new oracle, which now includes a larger sample of 1,182 Extract Method instances.
The remainder of this section describes the study design (Section~\ref{sec_study_design2}), the main results (Section~\ref{sec_study_results2}), and investigates the impact of alternative ranking strategies (Section~\ref{explore2}).


\subsection{Study Design}
\label{sec_study_design2}


\subsubsection{Target Systems}

We selected a sample of 13 systems from a corpus created by \cite{qualitas.class}, which is a compiled version of the Qualitas Corpus~\citep{qc}.
Table~\ref{tab_systems_rq2} lists the selected systems, which we chose favoring well-known and active projects. Besides, we aimed to cover a diversity of domains (e.g., XML processing, UML modeling, database, code analysis tools, etc.).

\begin{table}[h]
\renewcommand{\arraystretch}{1.3}
\caption{Study 2: Selected systems}
\centering
\tabcolsep=0.16cm
{\footnotesize
\begin{tabular}{lrrrr}
\hline
System & Instances &  Diferent Classes & Source Files & Avg. Method Size \\
\hline
Ant 1.8.2 & 99 & 52 {\tiny (52.5\%)} & 57 & 22.1 \\
ArgoUML 0.34 & 98 & 39 {\tiny (39.8\%)} & 65 & 23.8 \\
Checkstyle 5.6 & 100 & 23 {\tiny (23.0\%)} & 62 & 17.3 \\
Findbugs 1.3.9 & 99 & 51 {\tiny (51.5\%)} & 58 & 22.0 \\
Freemind 0.9.0 & 100 & 46 {\tiny (46.0\%)} & 58 & 19.5 \\
Jasper Reports 3.7.4 & 100 & 45 {\tiny (45.0\%)} & 54 & 25.0 \\
JEdit 4.3.2 & 99 & 51 {\tiny (51.5\%)} & 48 & 24.9 \\
JFreechart 1.0.13 & 100 & 82 {\tiny (82.0\%)} & 52 & 23.1 \\
Log4j 2.0-beta & 88 & 38 {\tiny (43.2\%)} & 62 & 20.0 \\
Quartz 1.8.3 & 70 & 36 {\tiny (51.4\%)} & 41 & 18.1 \\
Squirrel SQL 3.1.2 & 30 & 12 {\tiny (40.0\%)} & 14 & 19.1 \\
Tomcat 7.0.2 & 100 & 54 {\tiny (54.0\%)} & 51 & 25.7 \\
Xerces 2.10.0 & 99 & 37 {\tiny (37.4\%)} & 48 & 39.2 \\
\hline
Overall	& 1,182 & 566 {\tiny (47.9\%)} & 670 & 23.5 \\
\hline
\end{tabular}
\label{tab_systems_rq2}
}
\end{table}

Table~\ref{tab_systems_rq2} also presents: the number of Extract Method instances we synthesized for each systems (second column); the proportion of the instances that were created by inlining method invocations where the class of the invoker was different from the class of the invoked method (third column); the number of source files changed in the process (fourth column); and the average size---in number of statements---of the methods in the oracle (last column). Moreover, Figure~\ref{fig_msize} shows a histogram of the size of the methods in the oracle. As usual, there are few large methods and many medium/small methods.

\begin{figure}[htpb]
\centering
\includegraphics[width=0.9\textwidth]{scripts/msize.pdf}
\caption{Size of the methods in the oracle}
\label{fig_msize}
\end{figure}


\subsubsection{Oracle Generation}
\label{oragen2}

We constructed the oracle using the same process described in Section~\ref{oracle_gen}.
However, in this case, we did not assume that there are no relevant Extract Method instances in the selected systems. We relaxed this constraint to allow the generation of a large dataset, accepting that, in this case, our oracle is possibly incomplete. Nevertheless, we still consider that the instances in the oracle are relevant with a high confidence. Moreover, we introduced the following changes in the process:

\begin{itemize}
\item We ignore the visibility of class members (i.e.,~we changed private, protected, and package members to public) to increase the opportunities to inline invocations of methods that are not from the same class of the invoker. In the previous study, we observed that, due to members' visibility, a small number (8.6\%) of inlined methods were from different classes.

\item Rather than giving precedence to methods implemented in other classes or to larger methods, we randomly chose the method invocations to be inlined. In such way, we eliminate any bias in the size distribution (but still consider only those greater than the \emph{minimum size threshold}).

\item The maximum number of Extract Method instances we generate for a single system is limited to 100, avoiding that the largest systems dominate our sample.

\item We do not inline a method more than once in different invocation points. The intention of this rule is introduce more variety in the Extract Method instances of the oracle.

\item We apply a maximum of five Inline Method refactoring operations in the same file, also with the intention to introduce more variety.
\end{itemize}

In total, we synthesized 1,182 instances, which 47.9\% has originated from a different class, in 670 source files.




\subsection{Results and Discussion}
\label{sec_study_results2}


Table~\ref{tab_rq2} reports recall and precision values achieved by JExtract on the 13 systems. Similarly to the previous study, we present the number of relevant recommendations found~(\#), recall~(Rc.), and precision~(Pr.) for each configuration: Top~1, Top~2, and Top~3.
We must state that precision, in contrast to the previous study, should be viewed as a lower bound to actual precision because the original systems in this scenario have more chance to include relevant Extract Method instances (besides the ones synthesized by inlining methods). Thus, such instances are considered false positives when reported by our approach.

\begin{table}[htbp]
\renewcommand{\arraystretch}{1.3}
\caption{Study 2: Recall and precision}
\centering
\tabcolsep=0.16cm
{\footnotesize
\begin{tabular}{lr|rrr|rrr|rrr}
\hline
 & & \multicolumn{3}{c}{Top~1} & \multicolumn{3}{c}{Top~2} & \multicolumn{3}{c}{Top~3} \\
{System} & \multicolumn{1}{c|}{\#} & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.} \\
\hline
Ant 1.8.2 & 99 & 26 & 0.263 & 0.263 & 43 & 0.434 & 0.221 & 51 & 0.515 & 0.177 \\
ArgoUML 0.34 & 98 & 24 & 0.245 & 0.245 & 39 & 0.398 & 0.200 & 53 & 0.541 & 0.182 \\
Checkstyle 5.6 & 100 & 41 & 0.410 & 0.410 & 64 & 0.640 & 0.323 & 77 & 0.770 & 0.266 \\
Findbugs 1.3.9 & 99 & 27 & 0.273 & 0.273 & 49 & 0.495 & 0.249 & 67 & 0.677 & 0.231 \\
Freemind 0.9.0 & 100 & 34 & 0.340 & 0.340 & 46 & 0.460 & 0.232 & 61 & 0.610 & 0.207 \\
Jasper Reports 3.7.4 & 100 & 28 & 0.280 & 0.280 & 48 & 0.480 & 0.241 & 60 & 0.600 & 0.202 \\
JEdit 4.3.2 & 99 & 26 & 0.263 & 0.263 & 52 & 0.525 & 0.264 & 60 & 0.606 & 0.205 \\
JFreechart 1.0.13 & 100 & 15 & 0.150 & 0.150 & 35 & 0.350 & 0.176 & 50 & 0.500 & 0.168 \\
Log4j 2.0-beta & 88 & 30 & 0.341 & 0.341 & 50 & 0.568 & 0.287 & 62 & 0.705 & 0.239 \\
Quartz 1.8.3 & 70 & 19 & 0.271 & 0.271 & 31 & 0.443 & 0.221 & 41 & 0.586 & 0.197 \\
Squirrel SQL 3.1.2 & 30 & 9 & 0.300 & 0.300 & 12 & 0.400 & 0.203 & 15 & 0.500 & 0.170 \\
Tomcat 7.0.2 & 100 & 30 & 0.300 & 0.300 & 37 & 0.370 & 0.188 & 47 & 0.470 & 0.160 \\
Xerces 2.10.0 & 99 & 34 & 0.343 & 0.343 & 44 & 0.444 & 0.222 & 55 & 0.556 & 0.186 \\
\hline
Overall & 1,182 & 343 & 0.290 & 0.290 & 550 & 0.465 & 0.234 & 699 & 0.591 & 0.201 \\
\hline
\end{tabular}
\label{tab_rq2}
}
\end{table}

We can draw the following observations from these results:
\begin{itemize}
\item The overall results are inferior to the previous study, specially when we inspect Top~1 recall (29.0\% vs.~46.3\%). However, this difference is less evident in Top~2 recall (46.5\% vs.~58.9\%) and Top~3 recall (59.1\% vs.~67.4\%). Nevertheless, we still consider these results acceptable. For example, 59.1\% of the oracle instances are covered when using Top~3 configuration.

\item The results do not vary widely from system to system (standard deviation of 0.062 in Top~1 recall). The stronger differences are observed in: (i) JFreechart, which presents the worst Top~1 recall {\small (15\%)}; and (ii) Checkstyle, which presents the best Top~1 recall {\small (41\%)}. These observations suggest that our tool can be used in systems from different domains.
\end{itemize}

Similar to the previous study, Table~\ref{tab_rq2_error} presents the results when we accept a tolerance of 1 statement from the ideal answer.
In this scenario, there is a significant improvement in the results, specially on Top~1 recall (42.5\% vs. 0.29\%).

\begin{table}[htbp]
\renewcommand{\arraystretch}{1.3}
\caption{Study 2: Recall and precision (tolerance of one statement)}
\centering
\tabcolsep=0.16cm
{\footnotesize
\begin{tabular}{lr|rrr|rrr|rrr}
\hline
 & & \multicolumn{3}{c}{Top~1} & \multicolumn{3}{c}{Top~2} & \multicolumn{3}{c}{Top~3} \\
{System} & \multicolumn{1}{c|}{\#} & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.}  & \multicolumn{1}{c}{\#} & \multicolumn{1}{c}{Rc.} & \multicolumn{1}{c}{Pr.} \\
\hline
Ant 1.8.2 & 99 & 37 & 0.374 & 0.374 & 52 & 0.525 & 0.369 & 59 & 0.596 & 0.340 \\
ArgoUML 0.34 & 98 & 42 & 0.429 & 0.429 & 54 & 0.551 & 0.415 & 59 & 0.602 & 0.357 \\
Checkstyle 5.6 & 100 & 55 & 0.550 & 0.550 & 69 & 0.690 & 0.520 & 79 & 0.790 & 0.488 \\
Findbugs 1.3.9 & 99 & 40 & 0.404 & 0.404 & 63 & 0.636 & 0.467 & 76 & 0.768 & 0.448 \\
Freemind 0.9.0 & 100 & 43 & 0.430 & 0.430 & 60 & 0.600 & 0.414 & 73 & 0.730 & 0.407 \\
Jasper Reports 3.7.4 & 100 & 41 & 0.410 & 0.410 & 61 & 0.610 & 0.407 & 67 & 0.670 & 0.357 \\
JEdit 4.3.2 & 99 & 39 & 0.394 & 0.394 & 56 & 0.566 & 0.411 & 61 & 0.616 & 0.346 \\
JFreechart 1.0.13 & 100 & 39 & 0.390 & 0.390 & 53 & 0.530 & 0.357 & 64 & 0.640 & 0.333 \\
Log4j 2.0-beta & 88 & 38 & 0.432 & 0.432 & 58 & 0.659 & 0.471 & 62 & 0.705 & 0.429 \\
Quartz 1.8.3 & 70 & 33 & 0.471 & 0.471 & 48 & 0.686 & 0.493 & 53 & 0.757 & 0.423 \\
Squirrel SQL 3.1.2 & 30 & 10 & 0.333 & 0.333 & 15 & 0.500 & 0.322 & 18 & 0.600 & 0.295 \\
Tomcat 7.0.2 & 100 & 40 & 0.400 & 0.400 & 47 & 0.470 & 0.335 & 53 & 0.530 & 0.297 \\
Xerces 2.10.0 & 99 & 45 & 0.455 & 0.455 & 55 & 0.556 & 0.374 & 63 & 0.636 & 0.361 \\
\hline
Overall & 1,182 & 502 & 0.425 & 0.425 & 691 & 0.585 & 0.415 & 787 & 0.666 & 0.378 \\
\hline
\end{tabular}
\label{tab_rq2_error}
}
\end{table}



\subsubsection{Impact of Method Size}

Figure~\ref{fig_msizeprec} shows a barchart comparing the recall values achieved considering only methods of a certain size range. We can observe that we achieve better results for small methods. In fact, since the number of candidates is related to the number of statements of the method, we argue that our observation is somewhat expected, i.e.,~it is harder to rank the recommendation of the oracle on the top position when there are numerous candidates in the method.

%We previously discussed the importance of the parameter \emph{Maximum Recomendations per Method} to filter the large number of Extract Method candidates our approach generates in its first phase. In Figure~\ref{fig_candidates} we shown a histogram of the number of valid Extract Method candidates for each method of the oracle. 


%\begin{figure}[htpb]
%\centering
%\includegraphics[width=0.9\textwidth]{scripts/candidates.pdf}
%\caption{Histogram of number of candidates for a method}
%\label{fig_candidates}
%\end{figure}


\begin{figure}[htpb]
\centering
\includegraphics[width=0.9\textwidth]{scripts/msizeprec.pdf}
\caption{Recall for different method size ranges}
\label{fig_msizeprec}
\end{figure}

%\begin{figure}[htpb]
%\centering
%\includegraphics[width=0.9\textwidth]{scripts/candidatesprec.pdf}
%\caption{Recall for different ranges of number of candidates}
%\label{fig_candidatesprec}
%\end{figure}



%sameclass
%Overall	0	28.1	47.5	61.5
%Overall	1	29.9	45.6	57.0



\subsection{Exploring Ranking Strategies}
\label{explore2}

Taking advantage of this larger dataset, we decided to further investigate {\textbf{\em RQ \#2}} from the exploratory study (refer to Section~\ref{sec:exploratory_study}):\\

\noindent{\textbf{\em RQ \#2}} -- \rqii\\

First, we investigated if \emph{Kulczynski} is the best coefficient in face of these 13 systems.
Table~\ref{tab_rq2_coef} shows the recall (using only Top~1 config, for brevity) achieved for each project using each coefficient:
Jaccard (JAC), Sorenson (SOR), Sokal and Sneath~2 (SS2), PSC, Kulczynski (KUL), and Ochiai (OCH).
We can observe that \emph{Kulczynski} is superior in most systems, except two of them: JFreechart and Log4j.
Therefore, this observation corroborates with our decision to use this coefficient.
%Nevertheless, the distinction between two groups 

\begin{table}[htbp]
\renewcommand{\arraystretch}{1.3}
\caption{Study 2: Comparison of similarity coefficients (Top~1 recall)}
\centering
\tabcolsep=0.16cm
{\footnotesize
\begin{tabular}{lrrrrrr}
\hline
System & JAC & SOR & SS2 & PSC & KUL & OCH \\
\hline
Ant 1.8.2 & 0.232 & 0.242 & 0.232 & 0.242 & \textbf{0.263} & 0.232 \\
ArgoUML 0.34 & 0.163 & 0.153 & 0.153 & 0.214 & \textbf{0.245} & 0.224 \\
Checkstyle 5.6 & 0.310 & 0.320 & 0.310 & 0.400 & \textbf{0.410} & 0.390 \\
Findbugs 1.3.9 & 0.253 & 0.253 & 0.253 & \textbf{0.273} & \textbf{0.273} & 0.253 \\
Freemind 0.9.0 & 0.250 & 0.250 & 0.240 & 0.260 & \textbf{0.340} & 0.300 \\
Jasper Reports 3.7.4 & 0.250 & 0.260 & 0.260 & 0.250 & \textbf{0.280} & 0.270 \\
JEdit 4.3.2 & 0.182 & 0.182 & 0.182 & 0.242 & \textbf{0.263} & 0.242 \\
JFreechart 1.0.13 & \textbf{0.170} & \textbf{0.170} & 0.160 & 0.150 & 0.150 & 0.160 \\
Log4j 2.0-beta & 0.318 & 0.318 & 0.307 & 0.341 & 0.341 & \textbf{0.364} \\
Quartz 1.8.3 & 0.200 & 0.186 & 0.186 & 0.243 & \textbf{0.271} & 0.200 \\
Squirrel SQL 3.1.2 & 0.233 & 0.233 & 0.233 & 0.267 & \textbf{0.300} & 0.267 \\
Tomcat 7.0.2 & 0.210 & 0.200 & 0.210 & 0.280 & \textbf{0.300} & 0.290 \\
Xerces 2.10.0 & 0.182 & 0.192 & 0.182 & 0.273 & \textbf{0.343} & 0.303 \\
\hline
Overall & 0.227 & 0.228 & 0.223 & 0.264 & \textbf{0.290} & 0.270 \\
\hline
\end{tabular}
\label{tab_rq2_coef}
}
\end{table}

Second, we investigated if we could improve our results by assigning different weights for each kind of dependency (due to \emph{variables}, \emph{types}, and \emph{packages}).
Table~\ref{tab_rq2_weights} shows in each column a different weighting scheme, denote by $w_v$-$w_t$-$w_p$, which are the weight factors of the scoring function (refer to Section~\ref{sec_scoreformula}).

%\begin{table}[htbp]
%\renewcommand{\arraystretch}{1.3}
%\caption{Study 2: Comparison of weighting schemes (Top~1 recall)}
%\centering
%\tabcolsep=0.16cm
%{\footnotesize
%\begin{tabular}{lrrrrrrrrrrr}
%\hline
%System & 1-1-1 & 1-0-0 & 0-1-0 & 0-0-1 & 2-1-1 & 1-2-1 & 1-1-2 & 100-10-1 & 1-10-100 \\
%\hline
%Ant 1.8.2 & 0.263 & 0.242 & 0.212 & 0.182 & \textbf{0.283} & 0.273 & 0.273 & 0.263 & 0.242 \\
%ArgoUML 0.34 & 0.245 & 0.255 & 0.163 & 0.112 & \textbf{0.286} & 0.224 & 0.214 & 0.276 & 0.204 \\
%Checkstyle 5.6 & 0.410 & 0.500 & 0.240 & 0.180 & 0.450 & 0.390 & 0.400 & \textbf{0.520} & 0.360 \\
%Findbugs 1.3.9 & 0.273 & 0.242 & 0.212 & 0.182 & 0.303 & 0.263 & 0.273 & \textbf{0.313} & 0.263 \\
%Freemind 0.9.0 & 0.340 & \textbf{0.370} & 0.140 & 0.130 & 0.360 & 0.320 & 0.290 & \textbf{0.370} & 0.230 \\
%Jasper Reports 3.7.4 & 0.280 & 0.290 & 0.220 & 0.190 & \textbf{0.320} & 0.300 & 0.250 & 0.310 & 0.270 \\
%JEdit 4.3.2 & 0.263 & \textbf{0.354} & 0.192 & 0.131 & 0.313 & 0.263 & 0.273 & 0.343 & 0.273 \\
%JFreechart 1.0.13 & 0.150 & 0.140 & 0.150 & 0.150 & 0.160 & \textbf{0.180} & 0.150 & 0.170 & 0.170 \\
%Log4j 2.0-beta & 0.341 & 0.261 & 0.148 & 0.216 & \textbf{0.364} & 0.330 & 0.341 & 0.307 & 0.341 \\
%Quartz 1.8.3 & \textbf{0.271} & 0.171 & 0.186 & 0.171 & 0.243 & \textbf{0.271} & \textbf{0.271} & 0.243 & 0.229 \\
%Squirrel SQL 3.1.2 & \textbf{0.300} & \textbf{0.300} & 0.167 & 0.100 & \textbf{0.300} & \textbf{0.300} & \textbf{0.300} & \textbf{0.300} & 0.267 \\
%Tomcat 7.0.2 & 0.300 & \textbf{0.330} & 0.140 & 0.130 & 0.300 & 0.270 & 0.300 & 0.310 & 0.260 \\
%Xerces 2.10.0 & 0.343 & 0.293 & 0.141 & 0.121 & \textbf{0.354} & 0.323 & \textbf{0.354} & 0.313 & 0.273 \\
%\hline
%Overall & 0.290 & 0.291 & 0.179 & 0.156 & 0.312 & 0.284 & 0.283 & \textbf{0.313} & 0.260 \\
%\hline
%\end{tabular}
%\label{tab_rq2_weights}
%}
%\end{table}




\begin{table}[htbp]
\renewcommand{\arraystretch}{1.3}
\caption{Study 2: Comparison of weighting schemes (Top~1 recall)}
\resizebox{\textwidth}{!}{
\centering
\tabcolsep=0.16cm
\begin{tabular}{lrrrrrrrrrrrr}
\hline
System & 1-1-1 & 1-0-0 & 0-1-0 & 0-0-1 & 1-1-0 & 2-1-1 & 1-2-1 & 1-1-2 & 100-10-1 & 1-10-100 \\
\hline
Ant 1.8.2 & 0.263 & 0.242 & 0.212 & 0.182 & 0.273 & \textbf{0.283} & 0.273 & 0.273 & 0.263 & 0.242 \\
ArgoUML 0.34 & 0.245 & 0.255 & 0.163 & 0.112 & \textbf{0.296} & 0.286 & 0.224 & 0.214 & 0.276 & 0.204 \\
Checkstyle 5.6 & 0.410 & 0.500 & 0.240 & 0.180 & 0.410 & 0.450 & 0.390 & 0.400 & \textbf{0.520} & 0.360 \\
Findbugs 1.3.9 & 0.273 & 0.242 & 0.212 & 0.182 & 0.303 & 0.303 & 0.263 & 0.273 & \textbf{0.313} & 0.263 \\
Freemind 0.9.0 & 0.340 & \textbf{0.370} & 0.140 & 0.130 & 0.350 & 0.360 & 0.320 & 0.290 & \textbf{0.370} & 0.230 \\
Jasper Reports 3.7.4 & 0.280 & 0.290 & 0.220 & 0.190 & 0.310 & \textbf{0.320} & 0.300 & 0.250 & 0.310 & 0.270 \\
JEdit 4.3.2 & 0.263 & \textbf{0.354} & 0.192 & 0.131 & 0.313 & 0.313 & 0.263 & 0.273 & 0.343 & 0.273 \\
JFreechart 1.0.13 & 0.150 & 0.140 & 0.150 & 0.150 & \textbf{0.180} & 0.160 & \textbf{0.180} & 0.150 & 0.170 & 0.170 \\
Log4j 2.0-beta & 0.341 & 0.261 & 0.148 & 0.216 & 0.307 & \textbf{0.364} & 0.330 & 0.341 & 0.307 & 0.341 \\
Quartz 1.8.3 & \textbf{0.271} & 0.171 & 0.186 & 0.171 & \textbf{0.271} & 0.243 & \textbf{0.271} & \textbf{0.271} & 0.243 & 0.229 \\
Squirrel SQL 3.1.2 & \textbf{0.300} & \textbf{0.300} & 0.167 & 0.100 & 0.267 & \textbf{0.300} & \textbf{0.300} & \textbf{0.300} & \textbf{0.300} & 0.267 \\
Tomcat 7.0.2 & 0.300 & 0.330 & 0.140 & 0.130 & \textbf{0.340} & 0.300 & 0.270 & 0.300 & 0.310 & 0.260 \\
Xerces 2.10.0 & 0.343 & 0.293 & 0.141 & 0.121 & 0.313 & \textbf{0.354} & 0.323 & \textbf{0.354} & 0.313 & 0.273 \\
\hline
Overall & 0.290 & 0.291 & 0.179 & 0.156 & 0.305 & 0.312 & 0.284 & 0.283 & \textbf{0.313} & 0.260 \\
\hline
\end{tabular}}
\label{tab_rq2_weights}
\end{table}




From these results, we outline the following observations:
\begin{itemize}
\item No weighting scheme drastically improves the results achieved by the default configuration (1-1-1), since the maximum recall achieved is 31.3\% (for the 100-10-1 config).

\item There is not a clear overall winner, i.e., the configuration with the best performance (in bold) varies from system to system.

\item Nevertheless, there is a tendency towards weighting schemes including \emph{variables}, as opposed to the observations on MyWebMarket, where \emph{types} had a stronger influence. In fact, the worst recall were achieved by the schemes that ignores \emph{variables} (0-1-0 and 0-0-1).
\end{itemize}





\subsection{Threats to Validity}

In this study, we must also state the threats we previously discussed.
First, we cannot extrapolate our results to other systems (external validity). However, we included 13 open-source systems from different domains, which provides at least an indication that our approach is general enough to be performed in other systems.
Second, similar to the previous study, we cannot claim that the Extract Method instances we based the evaluation on represent the whole spectrum of real refactoring instances normally performed by maintainers.
However, the selected systems are well-known projects, most of them largely used in industrial environments. Therefore, we can at least assume that they have a professional internal design quality and their methods encapsulate relevant design decisions.


\section{Final Remarks}
\label{eval_final_remarks}

This chapter reported two studies we conducted to evaluate our approach. In Section~\ref{sec_compare}, we compared the effectiveness of JExtract and JDeodorant using three systems: MyWebMarket, JUnit, and JHotDraw. In this particular study, JExtract presented an advantage over JDeodorant. For example, using a typical configuration (Top~3), it achieved overall recall and precision of 67.4\% and 23.2\%, against 4.2\% and 6.2\% for JDeodorant.

In Section~\ref{sec_13s}, we evaluated our tool with 13 open-source systems. JExtract achieved acceptable results in every system, indicating its applicability in different domains. In this study, the overall recall, using Top~3 config, was 59.1\%. Moreover, when investigating alternative ranking strategies we found that: (i) \emph{Kulczynski} was again the best set similarity coefficient; and (ii) different weighting schemes (i.e., values for the weight factors $w_v$, $w_t$, and $w_p$) may yield slightly better results, but not consistently for all systems.\footnote{\scriptsize Datasets and a supporting tool that automates their generation are publicly available at JExtract's web site: \mbox{\url{http://aserg.labsoft.dcc.ufmg.br/jextract}}}
