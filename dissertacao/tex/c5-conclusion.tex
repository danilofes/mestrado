\chapter{Conclusion}
\label{conclusion}

Extract Method is a key refactoring for improving program comprehension and maintainability. Moreover, its one of the most popular refactoring due to its versatility~\citep{fowler99, murphy2006java, MurphyHill2012, tsantalis_empiricalstudy}.
However, existing Extract Method supporting tools are most of the times underused and do not support developers in the task of identifying potential code fragments candidates for extraction~\citep{negara2013,kim2012,MurphyHill2012,Murphy-Hill2008}.

To address this shortcoming, we proposed an approach to identify Extract Method refactoring opportunities that can be directly automated by IDE-based refactoring tools. Our approach ranks the identified opportunities based on the design principle of separation of concerns. Specifically, we assume that the following sets should be as dissimilar as possible: (i) the set of dependencies established by the code fragment to be extracted; and (ii) the set of dependencies established by the remaining statements in the original method.
To compute sets similarity, we rely on \emph{Kulczynski} coefficient, which we chose based on an exploratory study with five other coefficients. A supporting tool, called JExtract, was also implemented.


Our evaluation using a set of synthesized Extract Method opportunities, which were introduced by inlining method invocations, suggests that JExtract is more effective (w.r.t.~recall and precision) than JDeodorant, a state-of-the-art tool.
Moreover, in a second study with a sample of 1,182 synthesized Extract Method instances from 13 open-source systems, our approach achieved an overall recall of 59.1\%.


We organized the remainder of this chapter as follows. First, Section~\ref{contributions} reviews the contributions of our research. Next, Section~\ref{limitations} points the limitations of our approach. Finally, Section~\ref{futurework} presents future work.

\section{Contributions}
\label{contributions}

This research makes the following contributions:
\begin{itemize}
\item An approach that identifies and ranks Extract Method refactoring recommendations, which aim to decompose long or complex methods to improve the system design (Chapter~\ref{sec:approach}).

\item An exploratory study conducted to support the decisions regarding the heuristic we used to rank the suggested refactoring opportunities (Section~\ref{sec:exploratory_study}).

\item A publicly available prototype tool, called JExtract, that implements our approach and hence suggests Extract Method refactoring opportunities for a requested method or for the entire system (Section~\ref{sec:tool}).

\item An evaluation comparing our approach with JDeodorant, a state-of-the-art tool, using two open-source systems and a small scale Web application (Section~\ref{sec_compare}).

\item A quantitative evaluation using a sample of 1,182 synthesized Extract Method instances from 13 open-source systems (Section~\ref{sec_13s}).

\item Two publicly available datasets of well-known Extract Method opportunities: the first with 95 instances distributed among three systems; and the second with 1,182 instances distributed among 13 systems (Chapter~\ref{sec_evaluation}).

\item The proposed approach to create the synthesized datasets, including a publicly available supporting tool to automate the process (Section~\ref{oracle_gen} and~\ref{oragen2}).

\end{itemize}


\section{Limitations}
\label{limitations}

Our approach has the following limitations:
\begin{itemize}
\item Our approach is limited to suggest the extraction of code fragments that can be automatically applied by current IDE refactoring tools.

\item Our approach cannot untangle code fragments before their extraction, i.e., it does not suggest refactoring that requires reordering or duplicating statements previous to the extraction.

\item We do not recommend names for the new methods created when applying a suggested Extract Method refactoring.

\item We did not evaluate with experts whether the recommendations provided by our approach are helpful in real refactoring tasks.
\end{itemize}



\section{Future Work}
\label{futurework}

We intend to complement this research with the following future work:
\begin{itemize}
\item \emph{Proposed approach:} (i) the capability to reorder statements before the code extraction to achieve a better separation of concerns; (ii) the recommendation of names for new methods, possibly using a technique based on the work of \cite{sridhara2011}; and (iii) the combination of Extract Method with Move Method to move misplaced code within a method to a more appropriate class.

\item \emph{Evaluation:} (i) a study with experts to assess the quality of the recommendations provided by the tool in real scenarios; (ii) the construction of a new dataset containing real Extract Method instances identified in historical data from open-source systems and (iii) a study to investigate the impact of ignoring core Java types or packages when computing the dependency sets.

\item \emph{JExtract Tool:} (i) a study on the possibility to recommend refactoring opportunities for a method that is being edited, without an explicit request by the developer; and (ii) improvements in usability and user interface.
\end{itemize}
