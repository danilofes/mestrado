\chapter{Introduction}

In this chapter, we present the motivation of this dissertation (Section~\ref{motivation}). We then state the problem and provide an overview of our solution (Section~\ref{sec:approach_overview}).
Finally, we present the outline of the dissertation (Section~\ref{outline}) and our publications (Section~\ref{publications}).

\section{Motivation}
\label{motivation}

A well-known property of software systems is their need to evolve, driven by the pressure of changing real-world requirements~\citep{lehman1980}. Unfortunately, this evolution process usually leads to deviations from the original design and an increase in the complexity. This phenomenon, usually referred as software aging, makes software maintenance and evolution increasingly costly~\citep{parnas1994software}. 
Although it is difficult to completely avoid software aging, developers can tackle this phenomenon with refactoring.

%The mechanics behind refactorings is usually specified in natural language and does not cover all possible scenarios and preconditions. In fact, even a bug-free implementation for typical refactorings—i.e., refactorings whose scopes are limited to few classes—has proved to be a complex task [Steimann and Thies, 2009].
%\subsection{Refactoring}

Refactoring is defined as the process of changing a software system to improve its internal structure in such a way that it does not alter the external behavior of the code~\citep{opdyke, fowler99}.
Thus, refactoring is an important practice to recover from design deviations and to improve the quality of the source code.
While refactoring is usually associated with software maintenance, it is also employed in the earlier phases of development. For example, practitioners of Test Driven Development (TDD) advocate the use of refactoring as an essential step in a software development cycle: (i) \emph{write a test}; (ii) \emph{make the test work quickly}; and (iii) \emph{refactor to add design decisions one at a time}~\citep{beck2003test}.

\cite{fowler99} also presents a catalog of refactorings, including Extract Method, Inline Method, Move Method, Pull Up Field, Replace Inheritance with Delegation, and many others. Particularly, this dissertation focuses on Extract Method, which we define in the next section.

%Most modern IDEs provide automated support to apply refactorings, receiving input information from developers.

\subsection{Extract Method Refactoring}

When a method contains a code fragment that can be grouped together and the maintainer decides to turn that fragment into a new method, we call this refactoring Extract Method~\citep{fowler99}. Figures~\ref{extract_before} and \ref{extract_after} present a minimal code example, before and after the refactoring, to illustrate its mechanics. In Figure~\ref{extract_before}, the statements at lines~4~and~5 were extracted into a new method, called \mcode{printDetails}, which is shown in Figure~\ref{extract_after}.

\begin{figure}[h]
\begin{lstlisting}
void printOwing(double amount) {
  printBanner();
  //print details
  System.out.println("name:" + _name);
  System.out.println("amount" + amount);
}
\end{lstlisting}
\caption{Code before Extract Method}
\label{extract_before}
\end{figure}

\begin{figure}[h]
\begin{lstlisting}
void printOwing(double amount) {
  printBanner();
  printDetails(amount);
}
void printDetails (double amount) {
  System.out.println ("name:" + _name);
  System.out.println ("amount" + amount);
}
\end{lstlisting}
\caption{Code after Extract Method}
\label{extract_after}
\end{figure}



One of the main motivations for Extract Method is to decompose a method that is too long or difficult to understand. Short methods with meaningful names improve readability and also favor reuse. Besides, overriding methods in subclasses is easier when they are finely grained.
In fact, empirical studies suggest that Extract Method is one of the most popular and versatile refactorings~\citep{murphy2006java, MurphyHill2012, wilking2007empirical}. For example, \cite{tsantalis_empiricalstudy} analyzed history data from three open-source projects and discovered Extract Method instances with nine distinct purposes.



\subsection{Current Tool Support}

Despite the simplicity of the aforementioned example (Figures~\ref{extract_before} and \ref{extract_after}), refactoring is usually a complex task, specially considering all possible scenarios and preconditions involved in such process~\citep{schaferChallenge, algebraicreasoning, steimann09, jungl, opdyke}. Therefore, tool support is crucial to increase productivity and reliability. Currently, most mainstream IDEs offer some kind of refactoring automation. For example, Eclipse provides automated Extract Method application given a selected code fragment, as long as the selection respects some preconditions.

However, despite the availability of supporting tools, recent empirical research shows that these tools, especially those supporting Extract Method refactorings, are most of the times underused~\citep{negara2013,kim2012,MurphyHill2012,Murphy-Hill2008}. For example, in a study including developers working in their natural environment, \cite{negara2013}~found that the number of participants who are aware of the automated support for Extract Method but still apply it manually is higher than the number of participants who predominantly apply it automatically. Moreover, a study from \cite{barriers_murphy} revealed that some users were discouraged to use the Extract Method tool of Eclipse IDE due to usability problems regarding the manual selection of valid code fragments. More important, current tools focus only on automating refactoring application, but developers expend considerable effort on the manual identification of refactoring opportunities.

In face of this scenario, we advocate that recommendation systems designed to identify Extract Method refactoring opportunities can play an important role in modern IDEs. Particularly, they can be effective instruments to increase the popularity of refactoring tools among IDE users since they avoid the manual step of selecting a valid code fragment.
More important, such systems may suggest valuable refactoring opportunities that developers are not aware, alleviating the burden to manually inspect the source code.



%Extract Method is a key refactoring for improving program comprehension. Besides promoting reuse and reducing code duplication, Extract Method contributes to readability and comprehensibility, by encouraging the extraction of self-documenting methods~\cite{fowler99,opdyke}. However, recent empirical research shows that automated refactoring tools, especially those supporting Extract Method refactorings, are most of the times underused~\cite{negara2013,kim2012,murphyhill,MurphyHill2012,Murphy-Hill2008}.  For example, in a study including developers working in their natural environment, Negara et al.~found that the number of participants who are aware of the automated support for Extract Method, but still apply the refactoring manually is higher than the number of participants who predominantly apply this refactoring automatically~\cite{negara2013}. Similarly, Kim et al.~report that 58.3\% of surveyed developers do all of their Extract Method refactorings manually~\cite{kim2012}. Finally, Murphy-Hill et al.~observed that Extract Method accounts for 12\% of the refactoring operations performed by the developers of Eclipse's refactoring tools, but corresponds to only 4.4\% of the refactorings performed by ordinary Eclipse's users~\cite{murphyhill,MurphyHill2012}. Such findings provide evidence that recommendation systems designed to identify Extract Method refactoring opportunities can play an important role in modern IDEs~\cite{tsantalis11}. Particularly, they can be effective instruments to increase the popularity of refactoring tools, mainly among typical IDE users.

%In this paper, we propose a novel approach to identify Extract Method refactoring opportunities that can be directly automated by IDE-based refactoring tools. 
%Basically, such tools impose two key precondition on extract method refactoring candidates: (a) they must represent a continuous block of statements, i.e.,~a block that can be marked using the text selection features of the IDE; (b) their extraction must preserve the program's behavior, e.g.,~the candidate must not include multiple assignments to variables that are required later. 
%Besides an algorithm for identifying extract method candidates that respect this refactoring preconditions, 
%Moreover, we also propose a ranking function to classify the list of initial candidates generated by the proposed approach, according to their potential to improve program comprehension. This function is inspired by the {\em minimize coupling/maximize cohesion} design guideline. More specifically, we assume that {\em the structural dependencies established by top-ranked Extract Method candidates should be very different from the ones established by the remaining statements in the original method}. When this assumption holds, the  extraction tends to encapsulate a well-defined computation with its own set of dependencies (high cohesion) and that is also independent from the original method (low coupling).

%Code~\ref{list:example2} presents an example from \mbox{MyWebMarket}, an illustrative but real web-based system~\cite{csmr2012era}. 
%\input{list/example2.tex}
%In this code, lines~3--9 represent a well-defined computation:  this fragment opens a transaction with the persistence framework (Hibernate) to update a \mcode{Product} object in the underlying database. As we can observe, this code fragment manipulates types and variables (\mcode{Session} and \mcode{Transaction}) that are only used to start and finish an \mcode{update} transaction. In other words, these types and their respective variables are not used by the remaining statements in the method (line 2 and lines 10--16). Finally, lines 3--9 respect the Extract Method refactoring preconditions and they can be extracted without any further modification in the code to a new method using the refactoring tool of an IDE. Therefore, our approach would recommend its extraction to a new method.





\section{Proposed Approach}
\label{sec:approach_overview}

We previously discussed how the automated identification of Extract Method refactoring opportunities may leverage existing tool support and contribute to a widespread adoption of refactoring practices.
In this dissertation, we propose a novel approach, based on structural dependencies, to identify and rank Extract Method refactoring opportunities that can be directly automated by IDE-based refactoring tools. %However, before describing the solution we first state the problem we aim to solve.

%\noindent{\textbf{Problem Statement}}
\subsection{Problem Statement}

%Precisely, we tackle the following problem in this dissertation:
We aim to design, implement, and evaluate a refactoring recommendation approach, which should have the following input and output:
\begin{itemize}
\item \textbf{Input:} The source code of a method.
\item \textbf{Output:} Potential Extract Method refactoring opportunities, such that:
\begin{enumerate}
\item They can be automatically extracted using the existing tool support.
\item They preserve the program's original behavior.
\item They are ranked by relevance, inspired by the {\em minimize coupling/maximize cohesion} design guideline, i.e., they should encapsulate a well-defined computation (high cohesion) that is independent from the input method (low coupling).
\end{enumerate}
\end{itemize}

While the first two properties of the output can be formally specified, the third one is subjective in its essence. The notion of relevance in recommendation systems is subjected to human judgment. However, we desire that the system's notion of relevance is as close as possible from an expert's notion of relevance.

In order to illustrate the proposed recommendation system, we use the method presented in Figure~\ref{example_jhotdraw}, from the JHotDraw system.
In this method, lines~5--9 are closely related to each other, encapsulating a well-defined computation responsible for the initialization of field \mcode{fConnectors}. Specifically, an object of type \mcode{Vector} is instantiated and populated. This code can be better organized by extracting these statements into a new method, preferably with a descriptive name (e.g., \mcode{initFConnectors}).
Thus, a possible output in this case is a recommendation suggesting to extract lines~5--9 to a new method.

\begin{figure}[h]
\begin{lstlisting}
private void initialize() {
  setText("node");
  Font fb = new Font("Helvetica", Font.BOLD, 12);
  setFont(fb);
  fConnectors = new Vector(4);
  fConnectors.addElement(new LocatorConnector(this, Locator.north()));
  fConnectors.addElement(new LocatorConnector(this, Locator.south()));
	fConnectors.addElement(new LocatorConnector(this, Locator.west()));
	fConnectors.addElement(new LocatorConnector(this, Locator.east()));
}
\end{lstlisting}
\caption{An example method from the JHotDraw system}
\label{example_jhotdraw}
\end{figure}


\subsection{Proposed Solution: JExtract}

JExtract, the tool we designed to implement our approach, is an Extract Method refactoring recommendation system. Specifically, given one or more methods, the output of our tool is a ranked list of refactoring opportunities. Moreover, when developers accept a recommendation, JExtract automatically applies it.

Figure~\ref{image_approachoverview} depicts the main elements involved in our recommendation approach, which can be summarized in the following two subproblems:

\begin{figure}[h]
%\centering
\includegraphics[width=1.0\textwidth]{img/jextract-overview.pdf}
\vspace{-0.5cm}
\caption{Overview of the proposed approach}
\label{image_approachoverview}
\end{figure}

\begin{enumerate}
\item \emph{Candidates Generation:} Given a method, JExtract identifies all viable Extract Method candidates, regardless of their relevance.
More specifically, it analyzes the source code and enumerates each Extract Method candidate that respects a set of preconditions necessary to guarantee its viability. Additionally, we enforce a configurable size threshold to avoid suggesting the extraction of very small code fragments (e.g., with one or two statements).

\item \emph{Ranking:} Given a list of Extract Method candidates, JExtract ranks the items by relevance and exclude non-relevant ones.
More specifically, it relies on a scoring function to classify the candidates according to their potential to improve the design of the code. This function, which is the key element of our solution, is based on the {\em minimize coupling/maximize cohesion} design guideline. We assume that {\em the structural dependencies established by top-ranked Extract Method candidates should be very different from the ones established by the remaining statements in the original method}. When this assumption holds, the  extraction tends to encapsulate a well-defined computation with its own set of dependencies (high cohesion) and that is also independent from the original method (low coupling).
\end{enumerate}





\section{Outline of the Dissertation}
\label{outline}

We organized the remainder of this work as follows:
\begin{itemize}
\item \textbf{Chapter~\ref{background}} covers central concepts related to this dissertation, including a discussion on refactoring recommendation systems applied to software engineering. We also present an overview on tools for detecting Extract Method refactoring opportunities and related work.

\item \textbf{Chapter~\ref{sec:approach}} presents the proposed approach. 
We detail the two main phases of the approach (\emph{Candidates Generation} and \emph{Ranking}) and illustrate the whole process using an example method. Furthermore, this chapter reports an initial study we designed to calibrate our approach, exploring ranking strategies and threshold parameters. Finally, we present JExtract, the tool that implements our proposed approach.

\item \textbf{Chapter~\ref{sec_evaluation}} reports the evaluation of our approach. First, we investigate how our approach performs compared to state-of-the-art ones. Second, we report a study with a sample of 1,182 synthesized Extract Method instances from 13 open-source systems. Last, we investigate alternative ranking strategies to confirm the findings of the exploratory study reported in Chapter~\ref{sec:approach}.

\item \textbf{Chapter~\ref{conclusion}} presents the final considerations of this dissertation, outlining its main contributions, limitations, and future work.

\end{itemize}




\section{Publications}
\label{publications}

This dissertation generated the following publications and therefore contains material from them:
\begin{itemize}
\item Danilo Silva, Ricardo Terra, and Marco Tulio Valente. Recommending Automated Extract Method Refactorings. In \emph{22nd IEEE International Conference on Program Comprehension (ICPC)}, pages 146--156, 2014.

\item Danilo Silva, Ricardo Terra, and Marco Tulio Valente. JExtract: An Eclipse Plug-in for Recommending Automated Extract Method Refactorings. \emph{V Congresso Brasileiro de Software:
Teoria e Prática, Sessão de Ferramentas}, pages 1--8, 2014. {\small (Under submission)}.
\end{itemize}
