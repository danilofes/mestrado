pdf(file="scatter.pdf", height=5, width=5, family="sans")
table <- read.table("candidates.csv", header=TRUE, sep=',')
plot(table, xlim=c(0,100), ylim=c(0,100))
dev.off()
