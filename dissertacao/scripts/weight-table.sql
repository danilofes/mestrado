select t.System,
IF(t.s111 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s111, '}'), t.s111) as "s111",
IF(t.s100 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s100, '}'), t.s100) as "s100",
IF(t.s010 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s010, '}'), t.s010) as "s010",
IF(t.s001 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s001, '}'), t.s001) as "s001",
IF(t.s110 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s110, '}'), t.s110) as "s110",
IF(t.s211 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s211, '}'), t.s211) as "s211",
IF(t.s121 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s121, '}'), t.s121) as "s121",
IF(t.s112 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s112, '}'), t.s112) as "s112",
IF(t.s100101 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s100101, '}'), t.s100101) as "s100101",
IF(t.s110100 = greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100), concat('\\textbf{', t.s110100, '}'), t.s110100) as "s110100"
-- greatest(t.s111, t.s100, t.s010, t.s001, t.s110, t.s211, t.s121, t.s112, t.s100101, t.s110100) as "best"
from (
select
  kem_project as "System",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 1-1-1') as "s111",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 1-0-0') as "s100",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 0-1-0') as "s010",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 0-0-1') as "s001",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 1-1-0') as "s110",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 2-1-1') as "s211",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 1-2-1') as "s121",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 1-1-2') as "s112",
-- (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 9-6-4') as "s964",
-- (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 4-6-9') as "s469",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 100-10-1') as "s100101",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_project = kem_project and emi_confid = 'kul 1-10-100') as "s110100"
from qualitas3.knownemi
group by kem_project
union all
select
  'Overall' as "System",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 1-1-1') as "s111",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 1-0-0') as "s100",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 0-1-0') as "s010",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 0-0-1') as "s001",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 1-1-0') as "s110",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 2-1-1') as "s211",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 1-2-1') as "s121",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 1-1-2') as "s112",
-- (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 9-6-4') as "s964",
-- (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 4-6-9') as "s469",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 100-10-1') as "s100101",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.emi where emi_confid = 'kul 1-10-100') as "s110100"
from qualitas3.knownemi
) t;
