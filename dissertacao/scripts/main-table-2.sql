select
  kem_project as "System",
  count(distinct kem_code) as "\#",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 3) as "Pr" /*,
  sum(IF(emi_match = 1, 1, 0)) / count(distinct kem_code) as "total recall"*/
from qualitas3.knownemi
left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
group by kem_project
union all
select
  'Total' as "System",
  count(distinct kem_code) as "\#",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 3) as "Pr" /*,
  sum(IF(emi_match = 1, 1, 0)) / count(distinct kem_code) as "total recall"*/
from qualitas3.knownemi
left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
;
