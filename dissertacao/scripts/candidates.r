data <- read.table("candidates.csv", header=TRUE, sep=',')

pdf(file="candidates.pdf", height=5, width=10, family="sans")
hist(data$c, breaks=max(data$c), main="", xlab="Number of candidates", xlim=c(0,100), col="lightgray")
dev.off()