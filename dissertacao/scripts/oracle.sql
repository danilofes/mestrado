select
  kem_project as "System",
  count(distinct kem_code) as "Instances",
  sum(IF(kem_sameclass <> 1, 1, 0)) as "Diferent Class",
  round(100 * sum(IF(kem_sameclass <> 1, 1, 0)) / count(distinct kem_code), 1) as "Diferent Class %",
  count(distinct kem_file) as "Files"
from qualitas2.knownemi
group by kem_project
;