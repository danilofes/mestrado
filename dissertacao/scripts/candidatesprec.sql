select
  concat('(', a, ', ', b, ']') as "range",
(select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from qualitas3.knownemi left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full' where kem_methodsize > a and kem_methodsize <= b) as "r1",
(select round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 3) from qualitas3.knownemi left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full' where kem_methodsize > a and kem_methodsize <= b) as "r2",
(select round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 3) from qualitas3.knownemi left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full' where kem_methodsize > a and kem_methodsize <= b) as "r3"
from (select 1 as a, 10 as b 
union all select 10, 20
union all select 20, 30
union all select 30, 40
union all select 40, 50
union all select 50, 60
union all select 60, 70
union all select 70, 80
union all select 80, 90
union all select 90, 10000) ranges
;

