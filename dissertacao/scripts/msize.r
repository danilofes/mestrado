data <- read.table("method-size.csv", header=TRUE, sep=',')

pdf(file="msize.pdf", height=5, width=10, family="sans")
hist(data$kem_methodsize, breaks=max(data$kem_methodsize), main="", xlab="Method size", xlim=c(0,100), col="lightgray")
dev.off()