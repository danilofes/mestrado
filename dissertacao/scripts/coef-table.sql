select
  kem_project as "System",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_project = kem_project and emi_confid = 'JAC') as "JAC",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_project = kem_project and emi_confid = 'SOR') as "SOR",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_project = kem_project and emi_confid = 'SS2') as "SS2",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_project = kem_project and emi_confid = 'PSC') as "PSC",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_project = kem_project and emi_confid = 'KUL') as "KUL",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_project = kem_project and emi_confid = 'OCH') as "OCH"
from qualitas2.knownemi
group by kem_project
union all
select
  'Total' as "System",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_confid = 'JAC') as "JAC",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_confid = 'SOR') as "SOR",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_confid = 'SS2') as "SS2",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_confid = 'PSC') as "PSC",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_confid = 'KUL') as "KUL",
  (select round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) from qualitas2.emi where emi_confid = 'OCH') as "OCH"
from qualitas2.knownemi;