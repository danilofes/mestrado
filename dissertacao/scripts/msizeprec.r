pdf(file="msizeprec.pdf", height=5, width=10, family="sans")

table <- read.table("msizeprec.csv", header=TRUE, sep=',')

data <- matrix(c(rbind(table$r1, table$r2 - table$r1, table$r3 - table$r2)), 3)

barplot(data, 
ylab="Recall",
xlab="Method size",
ylim=c(0, 1.0),
#space=0,
#xlim=c(0, ncol(data) + 3),
beside=FALSE, 
names.arg=table$range,
legend=c("Top 1", "Top 2", "Top 3"),
args.legend = list(x = "topright", bty="n")
)
dev.off()


#hist(data$kem_methodsize, breaks=max(data$kem_methodsize), main="", xlab="Size", xlim=c(0,100), col="lightgray")
#dev.off()