select
  kem_methodsize as "kem_methodsize",
  count(distinct emi_code) as "c"
from qualitas3.knownemi
left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
group by kem_code
;