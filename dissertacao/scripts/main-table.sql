select
  kem_project as "System",
  count(distinct kem_code) as "\#",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) as "Fo",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 1) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) as "Fo",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 1) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) as "Fo",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 1) as "Pr" /*,
  sum(IF(emi_match = 1, 1, 0)) / count(distinct kem_code) as "total recall"*/
from qualitas2.knownemi
left join qualitas2.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
group by kem_project
union all
select
  'Total' as "System",
  count(distinct kem_code) as "\#",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) as "Fo",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 1) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) as "Fo",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 1) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) as "Fo",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 1) as "Pr" /*,
  sum(IF(emi_match = 1, 1, 0)) / count(distinct kem_code) as "total recall"*/
from qualitas2.knownemi
left join qualitas2.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
;
