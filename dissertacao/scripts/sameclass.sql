select
  'Overall' as "System",
  kem_sameclass,
  round(100 * sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 1) as "Rc",
  round(100 * sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 1) as "Rc"
from qualitas3.knownemi
left join qualitas3.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
group by kem_sameclass
;