select
  kem_project as "System",
  count(distinct kem_code) as "\#",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_project = kem_project and emi_diff < 2 and emi_rank < 1) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_project = kem_project and emi_diff < 2 and emi_rank < 1)  / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_diff < 2 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 3) as "Pr",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_project = kem_project and emi_diff < 2 and emi_rank < 2) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_project = kem_project and emi_diff < 2 and emi_rank < 2)  / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_diff < 2 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 3) as "Pr",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_project = kem_project and emi_diff < 2 and emi_rank < 3) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_project = kem_project and emi_diff < 2 and emi_rank < 3)  / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_diff < 2 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 3) as "Pr",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'JDeodorant' and  emi_project = kem_project and emi_diff < 2) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'JDeodorant' and  emi_project = kem_project and emi_diff < 2) / count(distinct kem_code), 3) as "Rc",
  (select round(sum(IF(emi_diff < 2 and emi_rank < 1, 1, 0)) / count(distinct emi_code), 3) from jdeo.emi where emi_project = kem_project and emi_confid = 'JDeodorant') as "Pr"
from jdeo.knownemi
left join jdeo.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'main2'
group by kem_project
union all
select
  'Overall' as "System",
  count(distinct kem_code) as "\#",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_diff < 2 and emi_rank < 1) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_diff < 2 and emi_rank < 1)  / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_diff < 2 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 3) as "Pr",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_diff < 2 and emi_rank < 2) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_diff < 2 and emi_rank < 2)  / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_diff < 2 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 3) as "Pr",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_diff < 2 and emi_rank < 3) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'main2' and emi_diff < 2 and emi_rank < 3)  / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_diff < 2 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 3) as "Pr",
  (select count(distinct emi_method) from jdeo.emi where emi_confid = 'JDeodorant' and emi_diff < 2) as "Fo",
  round( (select count(distinct emi_method) from jdeo.emi where emi_confid = 'JDeodorant' and emi_diff < 2) / count(distinct kem_code), 3) as "Rc",
  (select round(sum(IF(emi_diff < 2 and emi_rank < 1, 1, 0)) / count(distinct emi_code), 3) from jdeo.emi where emi_confid = 'JDeodorant') as "Pr"
from jdeo.knownemi
left join jdeo.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'main2'
;
