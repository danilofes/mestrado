select
  kem_project as "System",
  count(distinct kem_code) as "\#",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 3) as "Pr",
  (select sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) from jdeo.emi where emi_project = kem_project and emi_confid = 'JDeodorant') as "Fo",
  (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from jdeo.emi where emi_project = kem_project and emi_confid = 'JDeodorant') as "Rc",
  (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct emi_code), 3) from jdeo.emi where emi_project = kem_project and emi_confid = 'JDeodorant') as "Pr"
from jdeo.knownemi
left join jdeo.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'main2'
group by kem_project
union all
select
  'Overall' as "System",
  count(distinct kem_code) as "\#",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 1, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 2, 1, 0)), 3) as "Pr",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) as "Fo",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code), 3) as "Rc",
  round(sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_code is not null and emi_rank < 3, 1, 0)), 3) as "Pr",
  (select sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) from jdeo.emi where emi_confid = 'JDeodorant') as "Fo",
  (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code), 3) from jdeo.emi where emi_confid = 'JDeodorant') as "Rc",
  (select round(sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct emi_code), 3) from jdeo.emi where emi_confid = 'JDeodorant') as "Pr"
from jdeo.knownemi
left join jdeo.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'main2'
;
