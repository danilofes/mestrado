#!/bin/sh -x

gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=1 -dLastPage=1 -sOutputFile=code1-algo-01.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=2 -dLastPage=2 -sOutputFile=code1-algo-02.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=3 -dLastPage=3 -sOutputFile=code1-algo-03.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=4 -dLastPage=4 -sOutputFile=code1-algo-04.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=5 -dLastPage=5 -sOutputFile=code1-algo-05.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=6 -dLastPage=6 -sOutputFile=code1-algo-06.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=7 -dLastPage=7 -sOutputFile=code1-algo-07.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=8 -dLastPage=8 -sOutputFile=code1-algo-08.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=9 -dLastPage=9 -sOutputFile=code1-algo-09.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=10 -dLastPage=10 -sOutputFile=code1-algo-10.pdf code1-algo.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=11 -dLastPage=11 -sOutputFile=code1-algo-11.pdf code1-algo.pdf

gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=1 -dLastPage=1 -sOutputFile=code1ex-01.pdf code1ex.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=2 -dLastPage=2 -sOutputFile=code1ex-02.pdf code1ex.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=3 -dLastPage=3 -sOutputFile=code1ex-03.pdf code1ex.pdf
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=4 -dLastPage=4 -sOutputFile=code1ex-04.pdf code1ex.pdf
