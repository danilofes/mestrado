package org.danilofes.ex2.d0;

public class ClassA {

	private int af1;
	private ClassB afb;

	public int am1() {
		return af1;
	}

	public int am2() {
		return af1 + afb.bm1(this) + afb.getBf2();
	}

}
