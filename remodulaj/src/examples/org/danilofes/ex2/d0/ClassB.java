package org.danilofes.ex2.d0;

public class ClassB {

	private int bf1;
	private int bf2;

	public int getBf1() {
		return bf1;
	}

	public int getBf2() {
		return bf2;
	}

	public int bm1(ClassA a) {
		return a.am1() * 3;
	}
}
