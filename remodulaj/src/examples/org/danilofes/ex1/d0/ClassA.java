package org.danilofes.ex1.d0;

public class ClassA {

	private int af1;
	private ClassB afb;

	public int am1() {
		return af1;
	}

	public int am2() {
		return af1 + afb.getBf1() + afb.getBf2();
	}

}
