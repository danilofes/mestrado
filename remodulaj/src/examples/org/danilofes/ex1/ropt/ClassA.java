package org.danilofes.ex1.ropt;

public class ClassA {

	private int af1;
	private ClassB afb;

	public int am1() {
		return af1;
	}

	public int am2() {
		int temp = afb.extractAm2();
		return af1 + temp;
	}

}
