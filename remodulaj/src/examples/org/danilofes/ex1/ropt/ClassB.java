package org.danilofes.ex1.ropt;

public class ClassB {

	private int bf1;
	private int bf2;

	public int getBf1() {
		return bf1;
	}

	public int getBf2() {
		return bf2;
	}

	int extractAm2() {
		int temp = getBf1() + getBf2();
		return temp;
	}

}
