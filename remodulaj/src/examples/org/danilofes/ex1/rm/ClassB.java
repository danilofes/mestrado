package org.danilofes.ex1.rm;

public class ClassB {

	private int bf1;
	private int bf2;

	public int getBf1() {
		return bf1;
	}

	public int getBf2() {
		return bf2;
	}

	public int am2(ClassA classA) {
		return classA.af1 + getBf1() + getBf2();
	}

}
