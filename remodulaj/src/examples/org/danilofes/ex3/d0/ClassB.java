package org.danilofes.ex3.d0;

public class ClassB {

	public int bm1() {
		return 1;
	}

	public int bm2(ClassA a) {
		return a.am2();
	}

	public int bm3(ClassA a) {
		return a.am2() + bm2(a);
	}

}
