package org.danilofes.ex3.d0;

public class ClassA {

	private ClassB afb;

	public int am1() {
		return 1;
	}

	public int am2() {
		return afb.bm2(this);
	}

	public int am3() {
		return am2() + afb.bm2(this);
	}

}
