package junit.runner;

/**
 * This class defines the current version of JUnit
 */
public class Version {
	public Version() {
		// don't instantiate
	}

	public static String id() {
		return "3.8";
	}
}
