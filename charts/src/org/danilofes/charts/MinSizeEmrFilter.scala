package org.danilofes.charts

class MinSizeEmrFilter(val minSize: Int) extends EmrFilter {

  override def name = s"minSize${minSize}"

  override def filterFn() = (emr: Emr) => {
    emr.extractedSize >= minSize && (emr.totalSize - emr.extractedSize) >= minSize
  }

}