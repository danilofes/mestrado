package org.danilofes.charts

trait EmrFilter {

  def name: String

  def filterFn(): (Emr) => Boolean

  def and(emrFilter: EmrFilter): EmrFilter = {
    val f1 = this
    val f2 = emrFilter
    new EmrFilter {
      override def name = (f1.name + " " + f2.name).trim()
      override def filterFn() = {
        val fn1 = f1.filterFn()
        val fn2 = f2.filterFn()
        (emr: Emr) => {
          fn1(emr) && fn2(emr)
        }
      }
    }
  }

}