package org.danilofes.charts

import java.io.File
import scala.collection.mutable.ListBuffer
import scala.io.Source
import ScoringFn.BAL_KUL_TVM
import ScoringFn.BAL_PSC_TVM
import ScoringFn.JAC_T
import ScoringFn.JAC_TV
import ScoringFn.JAC_TVM
import ScoringFn.KUL_T
import ScoringFn.KUL_TV
import ScoringFn.KUL_TVM
import ScoringFn.PSC_T
import ScoringFn.PSC_TV
import ScoringFn.PSC_TVM
import ScoringFn.P_JAC_TV
import ScoringFn.P_KUL_TVM
import ScoringFn.P_PSC_TVM
import java.io.PrintWriter
import java.io.FileOutputStream

object ChartPRScore extends App {

  val basePath = args(0)
  val outputFile = s"$basePath/prscore"
  val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile + ".r"))
  run(basePath)
  out.close()

  def run(basePath: String) {
    val ms3 = new MinSizeEmrFilter(3)
    val oracle = relevantSet(new File(s"${basePath}/goldset.txt"))
    //val oracleFull = relevantSet(new File(s"${basePath}/goldsetFull.txt"))
    val ignoreMissing = new IgnoreMissingMethodsFilter(oracle)
    val f1 = new FirstKEmrFilter(1)
    val f2 = new FirstKEmrFilter(2)
    val f3 = new FirstKEmrFilter(3)

    //precisionVsRecall("prscore1", basePath, oracleFull, ms3)
    precisionVsRecall("prscore", basePath, oracle, ms3)
    //precisionVsRecall("prscore3", basePath, oracle, ms3 and ignoreMissing)
    precisionVsRecall("prscore3", basePath, oracle, ms3 and f3)
    precisionVsRecall("prscore2", basePath, oracle, ms3 and f2)
    precisionVsRecall("prscore1", basePath, oracle, ms3 and f1)

    val minScore65 = new MinScoreEmrFilter(0.65)
    val minScore75 = new MinScoreEmrFilter(0.75)
    val minScore80 = new MinScoreEmrFilter(0.8)
    val minScore50 = new MinScoreEmrFilter(0.5)
    
    precisionAndRecall(KUL_TVM, ms3 and f1 and minScore65, oracle)
    precisionAndRecall(KUL_TVM, ms3 and f1 and minScore50, oracle)
    precisionAndRecall(KUL_TVM, ms3 and f1 and minScore75, oracle)
    precisionAndRecall(KUL_TVM, ms3 and f2 and minScore75, oracle)
    
    Console.println(s"Results saved at $outputFile")
  }

  def relevantSet(file: File): Set[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val values = line.split('\t')
      new Emr(values(0), values(1), values(2), 0, 0, 0.0)
    } toSet
  }

  def rankFromFile(file: File): Iterator[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val cols = line.split('\t')
      new Emr(cols(0), cols(1), cols(2), cols(3).toInt, cols(4).toInt, cols(5).toDouble)
    }
  }

  private def precisionVsRecall(title: String, basePath: String, relevants: Set[Emr], baseFilter: EmrFilter) {
    val page = ChartHelper.rPage(title, precisionVsRecallChart(basePath, relevants, baseFilter))
    out.println(page)
  }

  private def precisionVsRecallChart(basePath: String, relevants: Set[org.danilofes.charts.Emr], baseFilter: EmrFilter): String = {
	val emrRank = rankFromFile(new File(s"${basePath}/${KUL_TVM}.txt"))
	val filteredRank = emrRank.filter(baseFilter.filterFn())
	val values = precisionAtRecall(filteredRank, relevants)
	val names = List("Precision", "Recall")
    ChartHelper.rPlot("", names, values, (0.0, 1.0), (0.0, 1.0), "Score", "")
  }

  def precisionAtRecall(actual: Iterator[Emr], relevants: Set[Emr]): Array[Array[Tuple2[Double, Double]]] = {
    //var values = new ListBuffer[Tuple2[Double, Double]]

    var seriesPrecision = new ListBuffer[Tuple2[Double, Double]]
    var seriesRecall = new ListBuffer[Tuple2[Double, Double]]
    seriesRecall.insert(0, Tuple2(1.0, 0.0))
    
    val relevantsSeen = collection.mutable.Set[Emr]()

    var relevantsFound = 0
    var totalFound = 0
    var totalRelevants = relevants.size
    for (doc <- actual) {
      // Ignora duplicatas
      if (!relevantsSeen.contains(doc)) {
        totalFound += 1
        if (relevants.contains(doc)) {
          relevantsSeen += doc
          relevantsFound += 1
          //Console.printf("%.2f; %.2f\n", recall, precision)
        }
        val recall = relevantsFound.toDouble / totalRelevants
        val precision = relevantsFound.toDouble / totalFound
        seriesPrecision.insert(0, Tuple2(doc.score, precision))
        seriesRecall.insert(0, Tuple2(doc.score, recall))
      }
    }
    
    Array(seriesPrecision.toArray, seriesRecall.toArray)
  }

  def precisionAndRecall(approach: String, emrFilter: EmrFilter, relevants: Set[Emr]) {
    val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
	val filteredRank = emrRank.filter(emrFilter.filterFn())
    
    var relevantsFound = 0
    var totalFound = 0
    var totalRelevants = relevants.size
    for (doc <- filteredRank) {
        totalFound += 1
        if (relevants.contains(doc)) {
          relevantsFound += 1
        }
    }
    val precision = relevantsFound.toDouble / totalFound
    val recall = relevantsFound.toDouble / totalRelevants
    Console.println(s"${emrFilter.name} $precision\t$recall")
  }

}