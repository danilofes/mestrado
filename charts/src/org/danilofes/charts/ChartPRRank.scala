package org.danilofes.charts

import java.io.File
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source
import ScoringFn._
import java.io.PrintWriter
import java.io.FileOutputStream

object ChartPRRank extends App {

  val basePath = args(0)
  val outputFile = s"$basePath/prrank.r"
  val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile))
  run(basePath)
  out.close()

  def run(basePath: String) {
    val ms3 = new MinSizeEmrFilter(3)
    val allRelevants = relevantSet(new File(s"${basePath}/goldset.txt"))

    //precisionVsRecall("overall minSize3", basePath, allRelevants, ms3 and ignoreMissing)
    val page = ChartHelper.rPage("prrank",
      precisionAtRankChart(basePath, allRelevants, ms3)
    )
    out.println(page)

    Console.println(s"Results saved at $outputFile")
  }

  def relevantSet(file: File): Set[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val values = line.split('\t')
      new Emr(values(0), values(1), values(2), 0, 0, 0.0)
    } toSet
  }

  def rankFromFile(file: File): Seq[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val cols = line.split('\t')
      new Emr(cols(0), cols(1), cols(2), cols(3).toInt, cols(4).toInt, cols(5).toDouble)
    }.toSeq
  }

  private def precisionVsRecall(title: String, basePath: String, relevants: Set[Emr], baseFilter: EmrFilter) {
    val approaches1 = Seq(KUL_TVM)
    //val approaches2 = Seq()
    //val approaches3 = Seq(KUL_T, KUL_TV, KUL_TVM)
    //val approaches4 = Seq(PSC_T, PSC_TV, PSC_TVM)
    //val approaches5 = Seq(KUL_TVM, BAL_KUL_TVM, P_KUL_TVM)
    //val approaches6 = Seq(PSC_TVM, BAL_PSC_TVM, P_PSC_TVM)
    val page = ChartHelper.rPage(title,
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches1)
    )
    out.println(page)
  }

  private def precisionVsRecallChart(basePath: String, relevants: Set[Emr], baseFilter: EmrFilter, approachs: Seq[String]): String = {
    val values = new ListBuffer[Array[Tuple2[Double, Double]]]
    val names = ListBuffer[String]()
    for (approach <- approachs) yield {
      val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
      val filteredRank = emrRank.filter(baseFilter.filterFn())
      val series: Array[Tuple2[Double, Double]] = getSeries(filteredRank, relevants)
      names += approach
      values += series
    }
    ChartHelper.rPlot(baseFilter.name, names.toList, values.toArray)
  }

  private def getSeries(rank: Seq[Emr], relevants: Set[Emr]): Array[Tuple2[Double, Double]] = {
    val groups = groupByMethod(rank, relevants)
    var all = relevants.size
    var relFound = 0
    var allFound = 0
    
    var series = new ListBuffer[Tuple2[Double, Double]]
    
    val rankList = rank.toList
    for (i <- 0 to (rankList.size - 1)) {
      var something = false
      for (group <- groups; if group.length > i) {
        if (relevants.contains(group(i))) {
          relFound += 1
          something = true
        }
        allFound += 1
      }
      if (something) {
        val tuple = Tuple2(100.0 * relFound.toDouble / all, 100.0 * relFound.toDouble / allFound)
        series += tuple
      }
    }
    if (!series.isEmpty) {
      series.insert(0, Tuple2(0.0, series(0)._2))
    }
    
    series.toArray
  }

  private def precisionAtRankChart(basePath: String, relevants: Set[Emr], baseFilter: EmrFilter): String = {
    val values = new ListBuffer[Array[Tuple2[Double, Double]]]
    val names = List("Precision", "Recall")
    val emrRank = rankFromFile(new File(s"${basePath}/${KUL_TVM}.txt"))
    val filteredRank = emrRank.filter(baseFilter.filterFn())
    val series: Array[Array[Tuple2[Double, Double]]] = getPrecisionAndRecall(filteredRank, relevants)
    ChartHelper.rPlotRankPosition("", names, series)
  }

  private def getPrecisionAndRecall(rank: Seq[Emr], relevants: Set[Emr]): Array[Array[Tuple2[Double, Double]]] = {
    val groups = groupByMethod(rank, relevants)
    var all = relevants.size
    var relFound = 0
    var allFound = 0
    
    var seriesPrecision = new ListBuffer[Tuple2[Double, Double]]
    var seriesRecall = new ListBuffer[Tuple2[Double, Double]]
    
    val rankList = rank.toList
    for (i <- 0 to (rankList.size - 1)) {
      var something = false
      for (group <- groups; if group.length > i) {
        if (relevants.contains(group(i))) {
          relFound += 1
        }
        allFound += 1
        something = true
      }
      if (something) {
        val precision = relFound.toDouble / allFound
        val recall = relFound.toDouble / all
        seriesPrecision += Tuple2(i + 1, precision)
        seriesRecall += Tuple2(i + 1, recall)
      }
    }
    //if (!seriesPrecision.isEmpty) {
    //  seriesPrecision.insert(0, Tuple2(0.0, seriesPrecision(0)._2))
    //}
    
    Array(seriesPrecision.toArray, seriesRecall.toArray)
  }
  
  private def groupByMethod(rank: Seq[Emr], relevants: Set[org.danilofes.charts.Emr]): Array[Array[Emr]] = {
    relevants.map(emr => {
      rank.filter(rec => rec.classAndMethod == emr.classAndMethod).toArray
    }).toArray
  }

}