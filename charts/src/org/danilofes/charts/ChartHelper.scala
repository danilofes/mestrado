package org.danilofes.charts

import scala.collection.mutable.ListBuffer

object ChartHelper {

  def lineTypes = Array(1, 5, 2, 3)
  
  def rPage(fname: String, plots: Seq[String]): String = {
    val sb = new StringBuilder
    // embed the fonts
    // http://stat.ethz.ch/R-manual/R-devel/library/grDevices/html/pdf.html
    sb.append("pdf(file=\"%s.pdf\", height=18, width=12, family=\"sans\")\n".format(fname))
    sb.append("par(mfrow=c(3,2))\n")
    for (plot <- plots) {
    	sb.append(plot);
    	sb.append('\n');
    }
    sb.append("dev.off()\n")
    sb.toString
  }

  def rPage(fname: String, plot: String): String = {
    val sb = new StringBuilder
    // embed the fonts
    // http://stat.ethz.ch/R-manual/R-devel/library/grDevices/html/pdf.html
    sb.append("pdf(file=\"%s.pdf\", height=5, width=5, family=\"sans\")\n".format(fname))
    sb.append(plot);
    sb.append('\n');
    sb.append("dev.off()\n")
    sb.toString
  }
  
  def rPlot(query: String, names: List[String], values: Array[Array[Tuple2[Double, Double]]]): String = {
    rPlot(query, names, values, (0.0, 1.0), (0.0, 1.0), "Recall", "Precision")
  }

  def rPlot(title: String, names: List[String], values: Array[Array[Tuple2[Double, Double]]], xrange: Tuple2[Double, Double], yrange: Tuple2[Double, Double], xlab: String, ylab: String): String = {

    val sb = new StringBuilder

    sb.append(s"""
plot(c(${xrange._1}, ${xrange._2}), c(${yrange._1}, ${yrange._2}), type="n", xlab="${xlab}", ylab="${ylab}")
title("${title}")""")

    for (i <- 0 until values.length) {

      val sx: List[Double] = values(i).toList.map(_._1)
      val sy: List[Double] = values(i).toList.map(_._2)

      sb.append(s"""
x <- c(${sx.mkString(", ")})
y <- c(${sy.mkString(", ")})
lines(x, y, type="l", lty=${lineTypes(i)}, pch=${20 + i})
""")

    }

    val snames = names.map("\"" + _ + "\"").mkString(", ")
    val lty = lineTypes.mkString(", ")
    
    sb.append(s"""
legend("topright", c(${snames}), cex=0.8, lty=c(${lty}), lwd=1, bty="n", inset=0.0)
""")

    sb.toString
  }
  
  def rPlotRankPosition(query: String, names: List[String], values: Array[Array[Tuple2[Double, Double]]]): String = {
    val line0 = values(0)
    val maxX = 6.0
    rPlot(query, names, values, (1, maxX), (0.0, 1.0), "Recomendations per method", "")
  }
  
  def precisionAtRecall(actual: Iterator[String], relevants: Set[String]): Array[Tuple2[Double, Double]] = {
    var values = new ListBuffer[Tuple2[Double, Double]]

    val relevantsSeen = collection.mutable.Set[String]()

    var relevantsFound = 0
    var totalFound = 0
    var totalRelevants = relevants.size
    for (doc <- actual) {
      // Ignora duplicatas
      if (!relevantsSeen.contains(doc)) {
        totalFound += 1
        if (relevants.contains(doc)) {
          relevantsSeen += doc
          relevantsFound += 1
          val recall = relevantsFound.toDouble / totalRelevants
          val precision = relevantsFound.toDouble / totalFound
          values += Tuple2(recall, precision)
          //Console.printf("%.2f; %.2f\n", recall, precision)
        }

      }
    }
    if (relevantsFound < totalRelevants) {
//      val recall = 100.0 * (relevantsFound.toDouble / totalRelevants)
//      values += Tuple2(recall + 0.1, 0.0)
//      values += Tuple2(100.0, 0.0)
    }
    if (!values.isEmpty) {
      values.insert(0, Tuple2(0.0, values(0)._2))
    }

    //Console.println("RELEVANTS ")
    //for (r <- relevantsSeen) {
    //  Console.println(r)
    //}
    
    values.toArray
  }

  def precisionAtRank(rank: Int, actual: Seq[String], relevants: Set[String]): Double = {

    val relevantsSeen = collection.mutable.Set[String]()

    var relevantsFound = 0
    var totalFound = 0
    for (doc <- actual) {
      // Ignora duplicatas
      if (!relevantsSeen.contains(doc)) {
        totalFound += 1
        if (relevants.contains(doc)) {
          relevantsSeen += doc
          relevantsFound += 1
        }
        if (totalFound == rank) {
          return 100.0 * (relevantsFound.toDouble / totalFound)
        }
      }
    }

    return 0.0
  }

}