package org.danilofes.charts

object NoEmrFilter extends EmrFilter {

  override def name = ""

  override def filterFn() = (emr: Emr) => true

}