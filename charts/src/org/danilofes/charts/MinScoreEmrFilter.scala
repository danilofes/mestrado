package org.danilofes.charts

class MinScoreEmrFilter(val minScore: Double) extends EmrFilter {

  override def name = s"minScore${minScore}"

  override def filterFn() = (emr: Emr) => {
    emr.score >= minScore
  }

}