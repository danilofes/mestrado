package org.danilofes.charts

import java.io.File
import scala.collection.mutable.ListBuffer
import scala.io.Source
import ScoringFn.BAL_KUL_TVM
import ScoringFn.BAL_PSC_TVM
import ScoringFn.JAC_T
import ScoringFn.JAC_TV
import ScoringFn.JAC_TVM
import ScoringFn.KUL_T
import ScoringFn.KUL_TV
import ScoringFn.KUL_TVM
import ScoringFn.PSC_T
import ScoringFn.PSC_TV
import ScoringFn.PSC_TVM
import ScoringFn.P_JAC_TV
import ScoringFn.P_KUL_TVM
import ScoringFn.P_PSC_TVM
import java.io.PrintWriter
import java.io.FileOutputStream

object Main extends App {

  val basePath = args(0)
  val outputFile = s"$basePath/charts.r"
  val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile))
  run(basePath)
  out.close()

  def run(basePath: String) {
    val ms3 = new MinSizeEmrFilter(3)
    val allRelevants = relevantSet(new File(s"${basePath}/oraculo.txt"))
    val onlySequentialRelevants = relevantSet(new File(s"${basePath}/oraculo2.txt"))
    val ignoreMissing = new IgnoreMissingMethodsFilter(onlySequentialRelevants)

    precisionVsRecall("overall minSize3 first1", basePath, allRelevants, ms3 and new FirstKEmrFilter(1))
    precisionVsRecall("overall minSize3 first3", basePath, allRelevants, ms3 and new FirstKEmrFilter(3))
    precisionVsRecall("overall minSize3", basePath, allRelevants, ms3)
    precisionVsRecall("restricMethods minSize3 first1", basePath, onlySequentialRelevants, ignoreMissing and ms3 and new FirstKEmrFilter(1))
    precisionVsRecall("restricMethods minSize3 first3", basePath, onlySequentialRelevants, ignoreMissing and ms3 and new FirstKEmrFilter(3))
    precisionVsRecall("restricMethods minSize3", basePath, onlySequentialRelevants, ignoreMissing and ms3)

    precisionVsRecallMinSize(basePath, onlySequentialRelevants, ignoreMissing)
    
    Console.println(s"Results saved at $outputFile")
  }

  def relevantSet(file: File): Set[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val values = line.split('\t')
      new Emr(values(0), values(1), values(2), 0, 0, 0.0)
    } toSet
  }

  def rankFromFile(file: File): Iterator[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val cols = line.split('\t')
      new Emr(cols(0), cols(1), cols(2), cols(3).toInt, cols(4).toInt, cols(5).toDouble)
    }
  }

  private def precisionVsRecall(title: String, basePath: String, relevants: Set[Emr], baseFilter: EmrFilter) {
    val approaches1 = Seq(JAC_T, KUL_T, PSC_T)
    val approaches2 = Seq(JAC_TVM, KUL_TVM, PSC_TVM)
    val approaches3 = Seq(KUL_T, KUL_TV, KUL_TVM)
    val approaches4 = Seq(PSC_T, PSC_TV, PSC_TVM)
    val approaches5 = Seq(KUL_TVM, BAL_KUL_TVM, P_KUL_TVM)
    val approaches6 = Seq(PSC_TVM, BAL_PSC_TVM, P_PSC_TVM)
    val page = ChartHelper.rPage(title, Seq(
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches1),
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches2),
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches3),
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches4),
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches5),
      precisionVsRecallChart(basePath, relevants, baseFilter, approaches6)
    ))
    out.println(page)
  }

  private def precisionVsRecallMinSize(basePath: String, relevants: Set[Emr], baseFilter: EmrFilter) {
    val approaches = Seq(JAC_TV, P_JAC_TV, KUL_TV, PSC_TV)
    val page = ChartHelper.rPage(baseFilter.name, Seq(
      precisionVsRecallChart(basePath, relevants, baseFilter and new MinSizeEmrFilter(1), approaches),
      precisionVsRecallChart(basePath, relevants, baseFilter and new MinSizeEmrFilter(2), approaches),
      precisionVsRecallChart(basePath, relevants, baseFilter and new MinSizeEmrFilter(3), approaches),
      precisionVsRecallChart(basePath, relevants, baseFilter and new MinSizeEmrFilter(4), approaches),
      precisionVsRecallChart(basePath, relevants, baseFilter and new MinSizeEmrFilter(5), approaches),
      precisionVsRecallChart(basePath, relevants, baseFilter and new MinSizeEmrFilter(6), approaches)
    ))
    out.println(page)
  }

  private def precisionVsRecallChart(basePath: String, relevants: Set[org.danilofes.charts.Emr], baseFilter: EmrFilter, approachs: Seq[String]): String = {
    val values = new ListBuffer[Array[Tuple2[Double, Double]]]
    val names = ListBuffer[String]()
    for (approach <- approachs) {
      val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
      val filteredRank = emrRank.filter(baseFilter.filterFn())
      val series = ChartHelper.precisionAtRecall(filteredRank.map(_.id), relevants.map(_.id))
      names += approach
      values += series
    }
    ChartHelper.rPlot(baseFilter.name, names.toList, values.toArray)
  }

}