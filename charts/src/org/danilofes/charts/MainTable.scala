package org.danilofes.charts

import java.io.File
import scala.io.Source
import ScoringFn._
import java.io.PrintWriter
import java.io.FileWriter
import java.io.FileOutputStream

object MainTable extends App {

  val basePath = args(0)

  run(basePath)

  def run(basePath: String) {
    val ms3 = new MinSizeEmrFilter(3)
    val golset = relevantSet(new File(s"${basePath}/goldset.txt"))
    
//    barChart((_.substring(0, 3)), List(
//      JAC_TVM, SOR_TVM, OCH_TVM, PSC_TVM, KUL_TVM, SS2_TVM
//    ).map(a => (a, ms3)), onlySequentialRelevants, s"$basePath/coef")

    Console.println("JAC");
    barChart((_.substring(4).replace('M', 'P')), List(
      JAC_TVM, JAC_TV, JAC_TM, JAC_VM, JAC_T, JAC_V, JAC_M).map(a => (a, ms3)), golset, s"$basePath/jac")
    Console.println("SOR");
    barChart((_.substring(4).replace('M', 'P')), List(
      SOR_TVM, SOR_TV, SOR_TM, SOR_VM, SOR_T, SOR_V, SOR_M).map(a => (a, ms3)), golset, s"$basePath/sor")
    Console.println("OCH");
    barChart((_.substring(4).replace('M', 'P')), List(
      OCH_TVM, OCH_TV, OCH_TM, OCH_VM, OCH_T, OCH_V, OCH_M).map(a => (a, ms3)), golset, s"$basePath/och")
    Console.println("PSC");
    barChart((_.substring(4).replace('M', 'P')), List(
      PSC_TVM, PSC_TV, PSC_TM, PSC_VM, PSC_T, PSC_V, PSC_M).map(a => (a, ms3)), golset, s"$basePath/psc")
    Console.println("KUL");
    barChart((_.substring(4).replace('M', 'P')), List(
      KUL_TVM, KUL_TV, KUL_TM, KUL_VM, KUL_T, KUL_V, KUL_M).map(a => (a, ms3)), golset, s"$basePath/kul")
    Console.println("SS2");
    barChart((_.substring(4).replace('M', 'P')), List(
      SS2_TVM, SS2_TV, SS2_TM, SS2_VM, SS2_T, SS2_V, SS2_M).map(a => (a, ms3)), golset, s"$basePath/ss2")
    
    barChartMinSize(golset, s"$basePath/minsize")
    
    maxRecall(KUL_TVM, golset, ms3)
    
    Console.println(s"Done.")
  }

  def relevantSet(file: File): Set[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val values = line.split('\t')
      new Emr(values(0), values(1), values(2), 0, 0, 0.0)
    } toSet
  }

  def rankFromFile(file: File): Iterator[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val cols = line.split('\t')
      new Emr(cols(0), cols(1), cols(2), cols(3).toInt, cols(4).toInt, cols(5).toDouble)
    }
  }

  private def tieRatio(approach: String, emrFilter: EmrFilter): Double = {
    val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
    val filteredRank = emrRank.filter(emrFilter.filterFn()).toArray
    var ties: Int = 0
    for (i <- 1 to (filteredRank.size - 1)) {
      if (filteredRank(i - 1).score == filteredRank(i).score) {
        ties += 1
      }
    }
    ties.toDouble / filteredRank.size
  }

  private def firstHitRatio(approach: String, relevants: Set[Emr], baseFilter: EmrFilter, k: Int): Double = {
    val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
    val filter = baseFilter and new IgnoreMissingMethodsFilter(relevants) and new FirstKEmrFilter(k)
    val filteredRank = emrRank.filter(filter.filterFn())
    var found = 0
    for (rec <- filteredRank) {
      if (relevants.contains(rec)) {
        found += 1
      }
    }
    found.toDouble / relevants.size
  }

  private def maxRecall(approach: String, relevants: Set[Emr], baseFilter: EmrFilter): Double = {
    val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
    val filter = baseFilter and new IgnoreMissingMethodsFilter(relevants)
    val filteredRank = emrRank.filter(filter.filterFn())
    var found = 0
    var oracle = relevants
    for (rec <- filteredRank) {
      if (oracle.contains(rec)) {
        found += 1
        oracle = oracle - rec
      }
    }
    Console.println("Total recall " + (found.toDouble / relevants.size));
    // Imprime as n�o encontradas
    for (rec <- oracle) {
      Console.println(rec);
    }
    found.toDouble / relevants.size
  }
  
  private def table(approachs: List[String], filters: EmrFilter, onlySequentialRelevants: Set[Emr], outputFile: String): Unit = {
    val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile))
    out.println("rank\ttop1precision\ttop2precision\ttop3precision")
    val rows = approachs map { approach =>
      val f1Hits = firstHitRatio(approach, onlySequentialRelevants, filters, 1)
      val f2Hits = firstHitRatio(approach, onlySequentialRelevants, filters, 2)
      val f3Hits = firstHitRatio(approach, onlySequentialRelevants, filters, 3)
      //val ties = tieRatio(approach, ms3)
      (approach, f1Hits, f2Hits, f3Hits)
    }
    val sorted = rows.sortWith((t1, t2) => {
      (t1._2 > t2._2) ||
      (t1._2 == t2._2 && t1._3 > t2._3) ||
      (t1._2 == t2._2 && t1._3 == t2._3 && t1._4 > t2._4)
    })
    
    for (t <- sorted) {
      out.println(s"${t._1}\t${t._2}\t${t._3}\t${t._4}")
    }
    out.close()
  }

  private def barChart(barNameFn: String => String, approachs: List[Tuple2[String, EmrFilter]], onlySequentialRelevants: Set[Emr], outputFile: String): Unit = {
    val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile + ".r"))
    val outPdfFile = outputFile + ".pdf"
    
    out.println(s"""pdf(file="${outPdfFile}", height=5, width=5, family=\"sans\")""")
    
    val k = 3
    val values: Array[Tuple2[String, Array[Double]]] = approachs.map(approach => {
      (approach._1, Array.ofDim[Double](k))
    }).toArray
    
    for (i <- 0 until approachs.size) {
      for (j <- 0 until k) {
        val approach = approachs(i)
        values(i)._2(j) = firstHitRatio(approach._1, onlySequentialRelevants, approach._2, j + 1)
      }
    }
    
    val sorted = values
    /*
    val sorted = values.sortWith((t1, t2) => {
      (t1._2(0) > t2._2(0)) ||
      (t1._2(0) == t2._2(0) && t1._2(1) > t2._2(1)) ||
      (t1._2(0) == t2._2(0) && t1._2(1) == t2._2(1) && t1._2(2) > t2._2(2))
    })
    */

    for (i <- 0 until approachs.size) {
      Console.println(s"${sorted(i)._1}\t${sorted(i)._2(0)}\t${sorted(i)._2(1)}\t${sorted(i)._2(2)}");
      for (j <- (k - 1) until 0 by -1) {
        sorted(i)._2(j) = sorted(i)._2(j) - sorted(i)._2(j - 1)
      }
    }
    
    //for (i <- 0 until approachs.size) {
      //val t = sorted(i)
      //out.println(s"${t._1}\t${t._2(0)}\t${t._2(1)}\t${t._2(2)}")
    //}
    
    val data = s"""matrix(c(${sorted.map(_._2).flatten.mkString(", ")}), ${k})"""
    
    out.println(s"""data <- ${data}""")
    out.println(s"""barplot(data, """)
    //out.println(s"""main="${title}",""")
    out.println(s"""ylab="Recall",""")
    out.println(s"""ylim=c(0, 1.0),""")
    //out.println(s"""horiz=TRUE,""")
    out.println(s"""xlim=c(0, ncol(data) + 3),""")
    out.println(s"""beside=FALSE, """)
    val names = sorted.map(t => barNameFn(t._1)).mkString("\"", "\", \"", "\"")
    out.println(s"""names.arg=c(${names}),""")
    out.println(s"""legend=c("Top 1", "Top 2", "Top 3"),""")
    out.println(s"""args.legend=list(bty="n")""")
    out.println(s""")""")
    out.println(s"""dev.off()""")
    
    out.close()
  }
 
  private def barChartMinSize(onlySequentialRelevants: Set[Emr], outputFile: String): Unit = {
    val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile + ".r"))
    val outPdfFile = outputFile + ".pdf"
    
    out.println(s"""pdf(file="${outPdfFile}", height=5, width=5, family=\"sans\")""")
    
    val k = 3
    val approachs = 1 to 6
    val values: Array[Tuple2[String, Array[Double]]] = approachs.map(approach => {
      (approach.toString, Array.ofDim[Double](k))
    }).toArray
    
    for (i <- 0 until approachs.size) {
      for (j <- 0 until k) {
        val approach = approachs(i)
        values(i)._2(j) = firstHitRatio(KUL_TVM, onlySequentialRelevants, new MinSizeEmrFilter(approach), j + 1)
      }
    }
    
    val sorted = values

    for (i <- 0 until approachs.size) {
      for (j <- (k - 1) until 0 by -1) {
        sorted(i)._2(j) = sorted(i)._2(j) - sorted(i)._2(j - 1)
      }
    }
    
    //for (i <- 0 until approachs.size) {
      //val t = sorted(i)
      //out.println(s"${t._1}\t${t._2(0)}\t${t._2(1)}\t${t._2(2)}")
    //}
    
    val data = s"""matrix(c(${sorted.map(_._2).flatten.mkString(", ")}), ${k})"""
    
    out.println(s"""data <- ${data}""")
    out.println(s"""barplot(data, """)
    //out.println(s"""main="Size Threshold K",""")
    out.println(s"""ylab="Recall",""")
    out.println(s"""ylim=c(0, 1.0),""")
    //out.println(s"""horiz=TRUE,""")
    out.println(s"""xlim=c(0, ncol(data) + 3),""")
    out.println(s"""beside=FALSE, """)
    val names = approachs.mkString("\"K=", "\", \"K=", "\"")
    out.println(s"""names.arg=c(${names}),""")
    out.println(s"""legend=c("Top 1", "Top 2", "Top 3")""")
    out.println(s""")""")
    out.println(s"""dev.off()""")
    
    out.close()
  }
}