package org.danilofes.charts

import java.io.File
import scala.collection.mutable.ListBuffer
import scala.io.Source
import ScoringFn.BAL_KUL_TVM
import ScoringFn.BAL_PSC_TVM
import ScoringFn.JAC_T
import ScoringFn.JAC_TV
import ScoringFn.JAC_TVM
import ScoringFn.KUL_T
import ScoringFn.KUL_TV
import ScoringFn.KUL_TVM
import ScoringFn.PSC_T
import ScoringFn.PSC_TV
import ScoringFn.PSC_TVM
import ScoringFn.P_JAC_TV
import ScoringFn.P_KUL_TVM
import ScoringFn.P_PSC_TVM
import java.io.PrintWriter
import java.io.FileOutputStream

object ChartPR extends App {

  val basePath = args(0)
  val outputFile = s"$basePath/pr.r"
  val out: PrintWriter = new PrintWriter(new FileOutputStream(outputFile))
  run(basePath)
  out.close()

  def run(basePath: String) {
    val allRelevants = relevantSet(new File(s"${basePath}/goldset.txt"))

    val ms3 = new MinSizeEmrFilter(3)
    val ignore = new IgnoreMissingMethodsFilter(allRelevants)
    
    precisionVsRecall("prora", basePath, allRelevants, ms3 and ignore)
    precisionVsRecall("prall", basePath, allRelevants, ms3)

    Console.println(s"Results saved at $outputFile")
  }

  def relevantSet(file: File): Set[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val values = line.split('\t')
      new Emr(values(0), values(1), values(2), 0, 0, 0.0)
    } toSet
  }

  def rankFromFile(file: File): Iterator[Emr] = {
    Source.fromFile(file).getLines().filterNot(_.isEmpty()).map { (line: String) =>
      val cols = line.split('\t')
      new Emr(cols(0), cols(1), cols(2), cols(3).toInt, cols(4).toInt, cols(5).toDouble)
    }
  }

  private def precisionVsRecall(title: String, basePath: String, relevants: Set[Emr], f: EmrFilter) {
    val page = ChartHelper.rPage(title,
      precisionVsRecallChart(basePath, relevants, f)
    )
    out.println(page)
  }

  private def precisionVsRecallChart(basePath: String, relevants: Set[org.danilofes.charts.Emr], ms3: EmrFilter): String = {
    val approach = KUL_TVM
    
    val filters = List(
        ms3 and new FirstKEmrFilter(1),
        ms3 and new FirstKEmrFilter(2),
        ms3 and new FirstKEmrFilter(3),
        ms3
    )
    val names = List("Top 1", "Top 2", "Top 3", "All")
    
    val values = new ListBuffer[Array[Tuple2[Double, Double]]]
    for (f <- filters) {
      val emrRank = rankFromFile(new File(s"${basePath}/${approach}.txt"))
      val filteredRank = emrRank.filter(f.filterFn())
      val series = ChartHelper.precisionAtRecall(filteredRank.map(_.id), relevants.map(_.id))
      values += series
    }
    ChartHelper.rPlot("", names, values.toArray)
  }

}