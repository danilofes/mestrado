package org.danilofes.charts

class Emr(
  val className: String,
  val method: String,
  val slice: String,
  val totalSize: Int,
  val extractedSize: Int,
  val score: Double) {

  def id = s"${className}\t${method}\t${slice}";

  def classAndMethod = s"${className}\t${method}";

  override def hashCode() = id.hashCode()
  
  override def equals(obj: Any) = obj match {
    case emr: Emr => this.id == emr.id
    case _ => false
  }

  override def toString = id
}