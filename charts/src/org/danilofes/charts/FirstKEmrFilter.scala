package org.danilofes.charts

import scala.collection.mutable

class FirstKEmrFilter(val k: Int) extends EmrFilter {

  override def name = s"first${k}"

  override def filterFn() = {
    val dejavu = mutable.Map[String, Int]();
    (emr: Emr) => {
      val classAndMethod = emr.classAndMethod
      val count: Int = dejavu.get(classAndMethod) match {
        case None => 0
        case Some(c) => c
      }
      dejavu.put(classAndMethod, count + 1);
      count < k
    }
  }

}