package org.danilofes.charts

import scala.annotation.migration

class IgnoreMissingMethodsFilter(relevants: Set[Emr]) extends EmrFilter {

  val relevantMethods: Set[String] = relevants.map(_.classAndMethod)

  override def name = "restrictMethods"

  override def filterFn() = (emr: Emr) => {
    relevantMethods.contains(emr.classAndMethod)
  }

}