select
  emi_confid,
  emi_project,
  (select count(*) from jextract.knownemi where kem_project = emi_project) as "oracle",
  count(distinct kem_code),
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code) as "top 1 recall",
  sum(IF(emi_rank < 1, 1, 0)) as "top 1 total",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code) as "top 2 recall",
  sum(IF(emi_rank < 2, 1, 0)) as "top 2 total",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code) as "top 3 recall",
  sum(IF(emi_rank < 3, 1, 0)) as "top 3 total",
  sum(IF(emi_match = 1, 1, 0)) as "total found"
from jextract.emi
left join jextract.knownemi on kem_project = emi_project and kem_file = emi_file and kem_method = emi_method
group by emi_confid, emi_project;


select
  kem_methodsize,
  count(distinct kem_code) as "count",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code) as "top 1 recall",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code) as "top 2 recall",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code) as "top 3 recall"
from jextract.emi
left join jextract.knownemi on kem_project = emi_project and kem_file = emi_file and kem_method = emi_method
group by kem_methodsize;


select
  emi_confid,
  emi_project,
  (n-1)/10 as "threshold",
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / sum(IF(emi_rank < 1, 1, 0)) as "top 1 precision",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / sum(IF(emi_rank < 1, 2, 0)) as "top 2 precision",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / sum(IF(emi_rank < 1, 3, 0)) as "top 3 precision"
from jextract.emi
join jextract.seq on n <= 10 and emi_score > (n-1)/10
group by emi_confid, emi_project, n;


select
  emi_confid,
  /*emi_project,*/
  (n-1)/10 as "threshold",
  count(emi_code) as "#"
from jextract.emi
left join jextract.seq on n <= 10 and (emi_score > (n-1)/10 and emi_score <= (n)/10)
/*where emi_match = 1*/
group by emi_confid, /*emi_project, */n;

select
  emi_rank + 1,
  count(emi_code) as "#"
from jextract.emi
where emi_match = 1
group by emi_rank;

select
  emi_rank + 1,
  count(emi_code) as "#"
from jextract.emi
group by emi_rank;




select
  kem_project, emi_confid,
  count(distinct kem_code),
  sum(IF(emi_match = 1 and emi_rank < 1, 1, 0)) / count(distinct kem_code) as "top 1 recall",
  sum(IF(emi_match = 1 and emi_rank < 2, 1, 0)) / count(distinct kem_code) as "top 2 recall",
  sum(IF(emi_match = 1 and emi_rank < 3, 1, 0)) / count(distinct kem_code) as "top 3 recall",
  sum(IF(emi_match = 1, 1, 0)) / count(distinct kem_code) as "total recall"
from qualitas2.knownemi
left join qualitas2.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
group by kem_project, emi_confid;


select kem_project, kem_file, kem_method, count(distinct emi_code)
from qualitas2.knownemi
left join qualitas2.emi on emi_project = kem_project and emi_file = kem_file and emi_method = kem_method and emi_confid = 'full'
group by kem_project, kem_file, kem_method
having sum(IF(emi_match = 1, 1, 0)) = 0;


select
  kem_project,
  count(kem_code)
from qualitas2.knownemi
group by kem_project;

