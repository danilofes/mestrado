package org.danilofes.remodulaj.refactoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.internal.corext.refactoring.code.ExtractMethodRefactoring;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public class Utils {

	public static void sort(List<ExtractMethodRecomendation> recomendations, EmrScoringFn scoringFn, boolean groupByMethod) {
		EmrComparator comparator = new EmrComparator(scoringFn, groupByMethod);
		Collections.sort(recomendations, comparator);
		for (int i = 0, len = recomendations.size(); i < len; i++) {
			recomendations.get(i).setRank(i);
		}
	}

	public static List<ExtractMethodRecomendation> filterSameMethod(List<ExtractMethodRecomendation> recomendations, int maxCount) {
		ArrayList<ExtractMethodRecomendation> filtered = new ArrayList<ExtractMethodRecomendation>();
		Map<String, Integer> dejavu = new HashMap<String, Integer>();
		for (ExtractMethodRecomendation rec : recomendations) {
			String classAndMethod = rec.className + rec.method;
			if (!dejavu.containsKey(classAndMethod)) {
				dejavu.put(classAndMethod, 0);
			}
			Integer count = dejavu.get(classAndMethod) + 1;
			dejavu.put(classAndMethod, count);
			if (count <= maxCount) {
				filtered.add(rec);
			}
		}
		return filtered;
	}

	public static void asString(StringBuilder sb, Set<String> methodSet) {
		sb.append("{\n");
		for (String item : methodSet) {
			sb.append("  ");
			sb.append(item);
			sb.append("\n");
		}
		sb.append("}\n");
	}

	public static String explain(SetsSimilarity<String> ssimT, SetsSimilarity<String> ssimV, SetsSimilarity<String> ssimM) {
		StringBuilder sb = new StringBuilder();
		sb.append("# Types:\n");
		printSets(ssimT, sb);
		sb.append("# Variables:\n");
		printSets(ssimV, sb);
		sb.append("# Modules:\n");
		printSets(ssimM, sb);
		return sb.toString();
	}

	private static void printSets(SetsSimilarity<String> ssimT, StringBuilder sb) {
		sb.append(String.format("a=%d ", ssimT.getA()));
		Utils.asString(sb, ssimT.intersection);
		sb.append(String.format("b=%d ", ssimT.getB()));
		Utils.asString(sb, ssimT.set1);
		sb.append(String.format("c=%d ", ssimT.getC()));
		Utils.asString(sb, ssimT.set2);
	}

	public static Statement findEnclosingStatement(ASTNode astNode) {
		ASTNode node = astNode;
		while (node != null) {
			if (node instanceof Statement) {
				return (Statement) node;
			}
			node = node.getParent();
		}
		return null;
	}

	public static boolean canExtract(ICompilationUnit src, int start, int length) {
		try {
			ExtractMethodRefactoring refactoring = new ExtractMethodRefactoring(src, start, length);
			RefactoringStatus status = refactoring.checkAllConditions(new NullProgressMonitor());
			return status.isOK();
		} catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}

}
