package org.danilofes.remodulaj.popup.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.danilofes.remodulaj.inline.ProjectInliner;
import org.danilofes.remodulaj.model.EmrGenerator;
import org.danilofes.remodulaj.refactoring.EmrFileExporter;
import org.danilofes.remodulaj.refactoring.EmrFileReader;
import org.danilofes.remodulaj.refactoring.ExtractMethodRecomendation;
import org.danilofes.remodulaj.refactoring.JavaProjectAnalyser;
import org.danilofes.remodulaj.views.EmrSettingsDialog;
import org.danilofes.remodulaj.views.ExtractMethodRecomendationsView;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

public class ProjectMenuAction extends ObjectMenuAction<IProject> {

	/**
	 * Constructor for Action1.
	 */
	public ProjectMenuAction() {
		super(IProject.class);
	}

	@Override
	void handleAction(IAction action, IProject project) throws Exception {
		// MessageDialog.openInformation(shell, "Remodulaj", actionId);
		String actionId = action.getId();
		if (actionId.equals("org.danilofes.remodulaj.inlineMethods")) {
			new ProjectInliner().run(project);
			MessageDialog.openInformation(this.getShell(), "Remodulaj", "Inline methods complete.");
			return;
		}

		if (actionId.equals("org.danilofes.remodulaj.extractGoldSet")) {
			List<ExtractMethodRecomendation> emrList = new ProjectInliner().extractGoldSet(project);
			EmrFileExporter exporter = new EmrFileExporter(emrList, project.getLocation().toString() + "/goldset.txt");
			exporter.export();
			MessageDialog.openInformation(this.getShell(), "Remodulaj", "Gold set extracted.");
			return;
		}

		List<ExtractMethodRecomendation> recomendations;
		boolean checkPreconditions = false;
		if (actionId.equals("org.danilofes.remodulaj.importEmr")) {
			recomendations = importFromFile(project);
		} else if (actionId.equals("org.danilofes.remodulaj.showGoldset")) {
			EmrFileReader reader = new EmrFileReader();
			recomendations = reader.read(project.getLocation().toString() + "/goldset.txt");
			checkPreconditions = true;
		} else if (actionId.equals("org.danilofes.remodulaj.findEmr")) {
			recomendations = findEmr(project);
		} else {
			recomendations = Collections.emptyList();
		}

		// Sort the recomendations.
		JavaProjectAnalyser analyser = new JavaProjectAnalyser(recomendations, checkPreconditions);
		analyser.analyseProject(project);

		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		ExtractMethodRecomendationsView view = (ExtractMethodRecomendationsView) activePage
		        .showView("org.danilofes.remodulaj.views.ExtractMethodRecomendationsView");
		view.setRecomendations(recomendations, project);

		if (recomendations.isEmpty()) {
			MessageDialog.openInformation(this.getShell(), "Remodulaj", "No recomendations found.");
		}

	}

	private List<ExtractMethodRecomendation> importFromFile(IProject project) throws Exception {
		FileDialog fileDialog = new FileDialog(this.getShell());
		fileDialog.setText("Select File");
		// Set filter on .txt files
		fileDialog.setFilterExtensions(new String[] { "*.txt" });
		// Put in a readable name for the filter
		fileDialog.setFilterNames(new String[] { "Textfiles(*.txt)" });
		// Open Dialog and save result of selection
		String selected = fileDialog.open();

		EmrFileReader reader = new EmrFileReader();
		List<ExtractMethodRecomendation> recomendations = reader.read(selected);

		return recomendations;
	}

	private List<ExtractMethodRecomendation> findEmr(IProject project) throws Exception {
		EmrSettingsDialog dialog = new EmrSettingsDialog(this.getShell());
		if (dialog.open() == Window.OK) {
			Integer minSize = dialog.getMinSize();
			// int k = dialog.getFirstK();

			List<ExtractMethodRecomendation> recomendations = new ArrayList<ExtractMethodRecomendation>();
			EmrGenerator analyser = new EmrGenerator(recomendations, minSize);
			analyser.generateRecomendations(project);

			// List<ExtractMethodRecomendation> filtered =
			// Utils.filterSameMethod(recomendations, k);
			return recomendations;
		}

		return Collections.emptyList();
	}

}
