package org.danilofes.remodulaj.popup.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.danilofes.remodulaj.model.EmrGenerator;
import org.danilofes.remodulaj.refactoring.ExtractMethodRecomendation;
import org.danilofes.remodulaj.refactoring.JavaProjectAnalyser;
import org.danilofes.remodulaj.views.EmrSettingsDialog;
import org.danilofes.remodulaj.views.ExtractMethodRecomendationsView;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

public class MethodMenuAction extends ObjectMenuAction<IMethod> {

	/**
	 * Constructor for Action1.
	 */
	public MethodMenuAction() {
		super(IMethod.class);
	}

	@Override
	void handleAction(IAction action, IMethod method) throws Exception {
		// MessageDialog.openInformation(shell, "Remodulaj", actionId);
		IProject project = method.getJavaProject().getProject();

		List<ExtractMethodRecomendation> recomendations;
		String actionId = action.getId();
		if (actionId.equals("org.danilofes.remodulaj.methodmenu.findEmr")) {
			recomendations = findEmr(method);
		} else {
			recomendations = new ArrayList<ExtractMethodRecomendation>();
		}

		// Sort the recomendations.
		JavaProjectAnalyser analyser = new JavaProjectAnalyser(recomendations, false);
		analyser.analyseMethod(method);

		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		ExtractMethodRecomendationsView view = (ExtractMethodRecomendationsView) activePage
		        .showView("org.danilofes.remodulaj.views.ExtractMethodRecomendationsView");
		view.setRecomendations(recomendations, project);

		if (recomendations.isEmpty()) {
			MessageDialog.openInformation(this.getShell(), "Remodulaj", "No recomendations found.");
		}
	}

	private List<ExtractMethodRecomendation> findEmr(IMethod method) throws Exception {
		EmrSettingsDialog dialog = new EmrSettingsDialog(this.getShell());
		if (dialog.open() == Window.OK) {
			Integer minSize = dialog.getMinSize();
			// int k = dialog.getFirstK();

			List<ExtractMethodRecomendation> recomendations = new ArrayList<ExtractMethodRecomendation>();
			EmrGenerator analyser = new EmrGenerator(recomendations, minSize);
			analyser.generateRecomendations(method);

			// List<ExtractMethodRecomendation> filtered =
			// Utils.filterSameMethod(recomendations, k);
			return recomendations;
		}
		return Collections.emptyList();
	}

}
