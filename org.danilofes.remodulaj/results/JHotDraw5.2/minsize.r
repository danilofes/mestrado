pdf(file="E:/Danilo/Temp/minsize.pdf", height=5, width=5, family="sans")
data <- matrix(c(0.375, 0.125, 0.0892857142857143, 0.375, 0.125, 0.0892857142857143, 0.375, 0.125, 0.0892857142857143, 0.23214285714285715, 0.125, 0.0892857142857143, 0.21428571428571427, 0.07142857142857142, 0.125, 0.19642857142857142, 0.10714285714285712, 0.0535714285714286), 3)
barplot(data, 
ylab="Recall",
ylim=c(0, 1.0),
xlim=c(0, ncol(data) + 3),
beside=FALSE, 
names.arg=c("K=1", "K=2", "K=3", "K=4", "K=5", "K=6"),
legend=c("Top 1", "Top 2", "Top 3")
)
dev.off()
