pdf(file="E:/Danilo/Temp/minsize.pdf", height=5, width=5, family="sans")
data <- matrix(c(0.48, 0.16000000000000003, 0.07999999999999996, 0.48, 0.16000000000000003, 0.07999999999999996, 0.48, 0.16000000000000003, 0.07999999999999996, 0.48, 0.12, 0.08000000000000007, 0.28, 0.03999999999999998, 0.03999999999999998, 0.16, 0.0, 0.0), 3)
barplot(data, 
ylab="Recall",
ylim=c(0, 1.0),
xlim=c(0, ncol(data) + 3),
beside=FALSE, 
names.arg=c("K=1", "K=2", "K=3", "K=4", "K=5", "K=6"),
legend=c("Top 1", "Top 2", "Top 3")
)
dev.off()
