pdf(file="E:/Danilo/Temp/och.pdf", height=5, width=5, family="sans")
data <- matrix(c(0.44, 0.2, 0.040000000000000036, 0.4, 0.24, 0.07999999999999996, 0.36, 0.28, 0.12, 0.32, 0.27999999999999997, 0.08000000000000007, 0.28, 0.19999999999999996, 0.12, 0.28, 0.19999999999999996, 0.12, 0.12, 0.24, 0.12), 3)
barplot(data, 
ylab="Recall",
ylim=c(0, 1.0),
xlim=c(0, ncol(data) + 3),
beside=FALSE, 
names.arg=c("TV", "TVP", "VP", "V", "T", "TP", "P"),
legend=c("Top 1", "Top 2", "Top 3")
)
dev.off()
