\section{Trabalhos Relacionados}
\label{sec:related-works}

Podemos encontrar vários trabalhos na literatura que procuram identificar e recomendar de forma automatizada refatorações no código fonte, cada um com objetivos e limitações distintas. Nas seções a seguir são descritas algumas das principais abordagens existentes.


\subsection{Recomendação de \emph{Extract Method}}

Durante o trabalho de mestrado desenvolvemos uma abordagem nova para identificar oportunidades de refatoração \emph{Extract Method} que pode ser diretamente automatizada pelo suporte ferramental das IDEs~\citep{silva2014}. A abordagem utiliza uma função de \emph{ranking} para classificar uma lista de candidatos gerados, de acordo com o potencial de melhorar a modularização do código. Esta função é inspirada pelo princípio de minimizar acoplamento/maximizar a coesão. Mais especificamente, assumimos que o conjunto das dependências estruturais estabelecidas no fragmento de código a ser extraído deve ser muito diferente daquele estabelecido no código restante do método original. Quando essa condição ocorre, a extração tende a encapsular uma computação bem definida (alta coesão) e que também é independente do restante do método (baixo acoplamento).

A função de \emph{ranking} proposta considera que um \emph{statement} pode estabelecer dependências com três tipos de entidades: variáveis, tipos e pacotes. Os conjuntos dessas dependências são construídos, tanto para o código a ser extraído quando para o código restante no método original. A função de \emph{ranking} é então definida como a dissimilaridade entre estes conjuntos, usando para tal o coeficiente de similaridade de conjuntos Kulczynski.

A abordagem foi avaliada em dois estudos diferentes. No primeiro deles, foi feito um estudo exploratório baseado no sistema \mbox{MyWebMarket} com o objetivo de experimentar diferentes maneiras de computar a função de \emph{ranking}. Após este primeiro estudo, foi realizado um segundo estudo com 81 oportunidades de extração de método sintetizadas nos sistemas \mbox{JUnit} e \mbox{JHotDraw}. Essas oportunidades foram sintetizadas aplicando-se a refatoração \emph{Inline Method}, procurando garantir que elas fossem o mais reais possível. Neste estudo foram obtidas precisões de 48\% no \mbox{JUnit} e 38\% no \mbox{JHotDraw}. Apesar destes valores serem menores que o desejável, eles são expressivamente superiores aos obtidos com a principal abordagem existente, mesmo obtendo uma maior revocação.



%
\cite{tsantalis2011identification} também propõem uma metodologia e ferramenta para identificação de oportunidades de decomposição de métodos em sistemas Java. A metodologia usa como base uma técnica de análise estática de código fonte conhecida como \emph{backward slicing}, na qual \emph{statements} que afetam a execução de uma determinada parte de interesse no código são selecionados por meio das dependências de dados ou de controle. Selecionado um \emph{slice}, a decomposição do método pode ser feita extraindo-o para um novo método, ou seja, por meio da refatoração \emph{extract method}.

Dois critérios são usados para seleção desses \emph{slices} de código candidatos a extração:
\begin{itemize}
  \item A computação do valor de uma variável, denominado \emph{complete computation slice}
  \item O código que afeta o estado de um objeto, denominado \emph{object state slice}
\end{itemize}

No primeiro critério, \emph{complete computation slice}, o parâmetro necessário para geração do \emph{slice} é uma variável. Todos os \emph{statements} que escrevem nessa variável são selecionados inicialmente como sementes, e a seleção é expandida usando um \emph{backward slicing}. O segundo critério, \emph{object state slice}, tem como parâmetro uma referência a um objeto. De forma análoga, todos os \emph{statements} que alteram o estado do objeto (diretamente ou por meio de invocações de métodos) são selecionados como semente, e a seleção é expandida também usando um \emph{backward slicing}. Embora esses critérios sejam capazes de extrair métodos coesos, eles não são suficientes para cobrir todas as possibilidades de decomposição de método.

Para avaliar a ferramenta desenvolvida, os autores consideraram diferentes aspectos desejados para a solução, como precisão, utilidade das recomendações, preservação do comportamento do sistema e o impacto em métricas de qualidade. Um primeiro conjunto de avaliações foi realizado com parte do código do sistema \emph{JFreeChart}. As sugestões geradas pela ferramenta foram avaliadas por um especialista independente que respondeu às seguintes questões:
\begin{enumerate}
  \item O método extraído representa uma funcionalidade distinta e independente do método original?
  \item A extração do método contribui para a resolução de algum problema de \emph{design}?
\end{enumerate}
Das 64 recomendações avaliadas, 57 (89\%) foram aprovadas na primeira questão. Na segunda questão, 27 (42\%) tiveram resposta positiva. Por esses resultados, os autores procuraram mostrar que os métodos extraídos são úteis e coesos.

Após a aplicação das refatorações, foram executados todos os testes unitários do \emph{JFreeChart}, que não encontraram erros. Isto é um indício que a ferramenta não sugere transformações de código que mudam o comportamento original do sistema, o que é um premissa básica de uma refatoração válida. Além disso, os autores verificaram que as refatorações tiveram um impacto positivo nas métricas de coesão propostas por \cite{ott1993slice}.

Um segundo conjunto de avaliações foi conduzido com outros sistemas para medir a precisão e revocação. Dois programadores experientes analisaram sistemas nos quais já tinham trabalhado e geraram uma lista de recomendações de decomposição de métodos. Os avaliadores receberam como entrada uma lista de métodos para analisar e não tinham conhecimento da metodologia proposta. As sugestões dos programadores foram consideradas nesse contexto como o conjunto de resposta ideal, e os resultados da ferramenta foram contrastados com esses conjuntos. No geral, foi obtida uma precisão de 52\% e revocação de 75\%.


%
\subsection{Recomendação de \emph{Move Method}}

Em um trabalho recente, \cite{jmove} apresentaram uma técnica para identificar oportunidades de refatorações \emph{Move Method} com o objetivo de resolver o problema de \emph{design} conhecido como \emph{feature envy}, onde um método de uma classe se relaciona mais com uma outra classe do que com a sua própria classe. Essa abordagem é baseada na similaridade de conjuntos de dependências. Esta técnica foi implementada por um sistema de recomendação denominado JMove, que detecta métodos localizados em classes inadequadas e sugere mover tais métodos para a classe mais relacionada com o mesmo. A abordagem define a noção de similaridade entre um método e uma classe, utilizando esta heurística para definir qual a classe ideal para abrigar um método.


Este trabalho é um evolução do desenvolvido por \cite{tsantalis2009identification}, onde também é proposta uma metodologia para identificação de oportunidades de refatoração \emph{Move Method}. O algoritmo usado se baseia na noção de distância entre entidades de código (atributos/métodos) para as classes do sistema para sugerir uma lista de refatorações que preservam o comportamento e aprimoram a métrica de qualidade proposta. 

A metodologia descrita foi implementada pelos autores e integrada ao projeto \emph{JDeodorant}. \emph{JDeodorant} é um \emph{plug-in} da plataforma de desenvolvimento \emph{Eclipse} que reúne um conjunto de ferramentas que analisam um projeto Java e propõem operações de refatoração para sanar alguns tipos de problemas de \emph{design} do código fonte, como \emph{god class} e \emph{long method} \citep{fokaefs2012identification, tsantalis2010identification, tsantalis2011identification}. Esses problemas são conhecidos na comunidade como \emph{code smells}. Além de propor as recomendações, a ferramenta também é capaz de aplicar de forma automatizada aquelas desejadas pelo usuário.

Finalmente, vale a pena ressaltar que existem outras técnicas para identificar oportunidades de refatoração \emph{Move Method} baseadas, por exemplo, em algoritmos de busca~\citep{keeffe06,Seng2006}, Relational Topic Model (RTM)~\citep{methodbook_tse}, regras baseadas em métricas~\citep{Marinescu:2004:DSM:1018431.1021443}, etc.


\subsection{Recomendação de refatorações baseadas em busca}

Uma maneira de lidar com o problema de recomendação de refatorações é tratá-lo como um problema de busca. \cite{o2006search} propõem a ferramenta \emph{Code-Imp}, que é uma plataforma para executar refatorações automáticas para linguagem Java. As refatorações são exploradas como um problema de busca visando maximizar o valor da função objetivo, que no trabalho em questão é baseada em um conjunto de vinte e sete métricas. Assim, dada essa função, são aplicadas refatorações a fim de mover-se através do espaço de soluções buscando encontrar uma configuração que maximize a função objetivo, mas preservando o comportamento original do sistema. São usadas estratégias tradicionais como \emph{hill-climbing} e \emph{simulated annealing} para busca no espaço de estados. As refatorações disponíveis nos experimentos são compostas de 6 operações relacionadas a herança.

Uma abordagem semelhante é utilizada por \cite{seng2006search}, modelando também o problema como uma busca no espaço de soluções. A principal diferença é que o problema é tratado com um algoritmo evolucionário que mapeia as soluções, que são um conjunto de refatorações, em um indivíduo. Partindo de uma população inicial, diversas mutações e cruzamentos evoluem essas soluções, que eventualmente podem acabar convergindo em uma solução próxima da ótima. Nesse trabalho foi tratado apenas refatorações do tipo \emph{move method}, mas a abordagem é genérica o suficiente para ser aplicada a outras refatorações. Uma das desvantagens de abordagens baseadas em busca é que a qualidade da solução depende fortemente da função objetivo.


\subsection{Recomendação de refatorações para recuperação de arquitetura}

Outra linha de sistemas de recomendação de refatorações existentes visa recuperar a arquitetura de um sistema. \cite{terra2013recommendation} apresentam um sistema que recebe um conjunto de violações arquiteturais e sugere um conjunto de refatorações para corrigi-las. Nessa abordagem, primeiramente é necessário usar uma ferramenta de conformidade arquitetural para detectar as violações. A ferramenta possui uma gama de 32 tipos de refatorações que podem ser aplicadas para diferentes tipos de violações.


\subsection{Remodularização baseada em \emph{clustering}}

\emph{Clustering} é uma técnica frequentemente usada para gerar visualizações ou propor remodularização de sistemas. Essas soluções normalmente trabalham em um nível de abstração maior, como classes, pacotes ou módulos. \cite{anquetil1999experiments} fazem um estudo sobre o uso de \emph{clustering} em remodularização, avaliando diferentes algoritmos e métricas. \cite{mitchell2006automatic} apresentam a ferramenta \emph{Bunch}, que modela o sistema como um grafo de entidades e produz uma decomposição em partições. 

