
%\var{JExtract} implements a novel approach for recommending automated Extract Method refactorings.

%The tool was designed as a plug-in for the current mainstream IDE that automatically identifies, ranks, and applies the refactoring

\var{JExtract} is a tool that analyzes the source code of methods and recommends Extract Method refactoring opportunities, as illustrated in Figure~\ref{image_approachoverview}. First, the tool generates all Extract Method possibilities for each method. Second, these possibilities are ranked according to a scoring function based on the similarity between sets of dependencies established in the code.
%Last, the best ranked candidates are provided as the output of the tool, allowing developers to inspect the recommendations and apply the one they desire.

\begin{figure}[htpb]
\centering
\includegraphics[width=1.0\textwidth]{img/jextract-overview.pdf}
\vspace{-19pt}
\caption{The \vart{JExtract} tool}
%\vspace{-1pt}
\label{image_approachoverview}
\end{figure}

This main section of the paper is organized as follows.
Subsection~\ref{sec:approach} provides an overview of our approach for identifying
%In the remainder of this section, we first provide an overview of our approach for identifying 
Extract Method refactoring opportunities. Subsection~\ref{sec:archinterface} describes the design and implementation of the tool. Finally,
Subsection~\ref{sec:examples} presents the results of our evaluation in
open-source systems. A detailed description of the recommendation technique behind JExtract is present in a recent full technical paper~\cite{2014_icpc}.



\subsection{{Proposed Approach}} 
\label{sec:approach}

The approach is divided in three phases: {\em Generation of Candidates}, {\em Scoring}, and {\em Ranking}. 

\subsubsection{{Generation of candidates}} 
\label{sec:candidate_generation}

This phase is responsible for identifying all possible Extract Method refactoring opportunities. First, we split the methods into blocks, which consist of sequential statements that follow a linear control flow. As an example, Figure~\ref{image_example1latex} presents method \var{mouseRelease} of class \var{SelectionClassifierBox}, extracted from ArgoUML. We can notice that each statement is labeled using the \var{SX.Y} pattern, where $\tt{X}$ and $\tt{Y}$ denote the block and the statement, respectively. For example, $\tt S2.3$ is the third statement of the second block, which declares a variable $\tt{cw}$.

\begin{figure}[H]
\centering
\includegraphics[width=0.80\textwidth]{img/example1.pdf}
\vspace{-6pt}
\caption{An Extract Method candidate in a method of \mbox{ArgoUML} (\vart{S3.2} to \vart{S3.5})}
\label{image_example1latex}
\vspace{-6pt}
\end{figure}


%into block 
%
%shows how the statements are grouped into blocks by labeling each of them using the $\tt{SX.Y}$ pattern, where $\tt{X}$ and $\tt{Y}$ denote the block and the statement, respectively. For example, $\tt S2.3$ is the third statement of the second block, which is the declaration of the variable $\tt{cw}$.


%The first step of the candidate generation process consists of analyzing the control flow of the given method to break its statements into blocks. Those blocks consist of sequential statements that follow a linear control flow. Figure~\ref{image_example1latex} shows how the statements are grouped into blocks by labeling each of them using the $\tt{SX.Y}$ pattern, where $\tt{X}$ and $\tt{Y}$ denote the block and the statement, respectively. For example, $\tt S2.3$ is the third statement of the second block, which is the declaration of the variable $\tt{cw}$.


Second, we generate all Extract Method candidates based on Algorithm~\ref{algo:generation} (extracted from~\cite{2014_icpc}).

\vspace{-6pt}
\begin{algorithm}[!ht]
{\scriptsize
\caption{Candidates generation algorithm~\cite{2014_icpc}}
{\textbf{Input:} A method $M$\\}
{\textbf{Output:} List with Extract Method candidates}
\begin{algorithmic}[1]
\State $Candidates \gets \emptyset$
\ForAll{$block\ B \in M$}
  \State $n \gets statements(B)$
  \For{$i \gets 1, n$}
    \For{$j \gets i, n$}
      \State $C \gets subset(B, i, j)$
      \If{$isValid(C)$}
        \State $Candidates \gets Candidates + C$
      \EndIf
    \EndFor
  \EndFor
\EndFor
\end{algorithmic}
\label{algo:generation}
}
\end{algorithm}
\vspace{-20pt}
%The three nested loops in Algorithm~\ref{algo:generation} (lines~2, 4, and 5) enforce that 
%the list of selected statements attend the following preconditions:

\vspace*{0.4cm}
\mbox{Fundamentally}, the three nested loops in Algorithm~\ref{algo:generation} (lines~2, 4, and 5) enforce that the list of selected statements attend the following preconditions:
%After breaking the method into blocks, we iterate through each sub-sequence of statements that can be extracted from each block, as described in Algorithm~\ref{algo:generation}.
%

\begin{itemize}
\item Only continuous statements inside a block are selected. In Figure~\ref{image_example1latex}, for example, it is not possible to select a candidate with $\tt S3.2$ and $\tt S3.4$ without including $\tt S3.3$.\\[-0.3cm]


\item The selected statements are part of a single block of statements. In Figure~\ref{image_example1latex}, for example, it is not possible to generate a candidate with both $\tt S2.6$ and $\tt S3.1$ since they belong to distinct blocks.\\[-0.3cm]


\item When a statement is selected, the respective children statements are also included. In Figure~\ref{image_example1latex}, for example, when statement $\tt S2.6$ is selected, its 
children statements $\tt S3.1$ to $\tt S3.7$ are also included.
\end{itemize}

Last but not least, we do not ensure that every iteration of the loop yields an Extract Method candidate because: (i)~a candidate recommendation must respect a size threshold defined by parameter \textit{Minimum Extracted Statements}. The value is preset to $3$ {\small (changeable)},
which means that an Extract Method candidate has to have at least three statements; and (ii)~a candidate recommendation must respect the preconditions defined by the Extract Method refactoring engine.


\subsubsection{{Scoring}}
\label{sec:scoring_function}

%This phase is responsible for scoring the possible Extract Method refactoring opportunities generated in the previous phase.
%
%After the generation of candidates, a scoring function is used to rank and filter the candidates that will be shown as Extract Method recommendations. 
%  
%Given a method~$m'$, a
This phase is responsible for scoring the possible Extract Method refactoring opportunities generated in the previous phase, using a technique inspired by a Move Method recommendation heuristic~\cite{jmove}.
Assume $m'$ as the selection of statements of an Extract Method candidate and $m''$ the remaining statements in the original method~$m$.
%The proposed scoring function is centered on the structural dissimilarity between $m'$ and~$m''$.
The proposed heuristic aims to minimize the structural similarity between $m'$ and~$m''$.
\vspace{6pt}

\noindent{\bf Structural Dependencies:} The set of dependencies established by a selection of statements $S$ with variables, types, and packages is denoted by $\mathit{Dep}_{\mathit{var}}(S)$, $\mathit{Dep}_{\mathit{type}}(S)$, and $\mathit{Dep}_{\mathit{pack}}(S)$, respectively. These sets are constructed as described next.

\vspace{3pt}

\begin{itemize}
\item{\em Variables:} If a statement $s$ from a selection of statements $S$ declares, assigns, or reads a variable $v$, then $v$ is added to $\mathit{Dep}_{\mathit{var}}(S)$. In a similar way, reads from and writes to formal parameters and fields are considered.\\[-0.2cm]

\item{\em Types:} If a statement $s$ from a selection of statements $S$ uses a type (class or interface) $T$, then $T$ is added to $\mathit{Dep}_{\mathit{type}}(S)$.\\[-0.2cm] %The reason to consider this form of dependency is that methods should also hide the services provided by well-defined sets of types. For example, in Code~\ref{list:example2} a method can be extracted to hide the services provided by the \mcode{Session} and \mcode{Transaction} types, used to connect with Hibernate. Specifically, we consider that the following scenarios characterize the use of a type:
%\begin{itemize}
%\item If $s$ calls a method $m$, the type $T$ that declares $m$ is included in $\mathit{Dep}_{\mathit{type}}(S)$.
%\item If $s$ reads or writes to a field $f$, the type $T$ that declares $f$ is included in $\mathit{Dep}_{\mathit{type}}(S)$.
%\item If $s$ creates an object of a type $T$, then $T$ is included in $\mathit{Dep}_{\mathit{type}}(S)$.
%\item If $s$ declares a variable $v$, the type $T$ of $v$ is included in $\mathit{Dep}_{\mathit{type}}(S)$. 
%\item If $s$ handles an exception of type $T$, then $T$ is included in $\mathit{Dep}_{\mathit{type}}(S)$.
%\end{itemize}

\item{\em Packages:} For each type $T$ included in $\mathit{Dep}_{\mathit{type}}(S)$, as described in the previous item, the package where $T$ is implemented and all its parent packages are also included in $\mathit{Dep}_{\mathit{pack}}(S)$.
%For example, if a type \mbox{\mcode{foo.bar.Baz}} is used, then \mcode{foo} and \mcode{foo.bar} are included in $\mathit{Dep}_{\mathit{type}}(S)$.
%The reason to consider this form of dependency is analogous to types, but taken at a higher level of abstraction. It is worth noting that common root packages, such as \mcode{com} or \mcode{java}, are ignored as they do not denote meaningful modules.

\end{itemize}
\vspace{3pt}

For instance, assume $m'$ as the highlighted code in Figure~\ref{image_example1latex} (i.e., an Extract Method candidate) and $m''$ the remaining statements in the original method~\var{mouseReleased}.
On one hand,
$\mathit{Dep}_{\mathit{var}}(m') = \left\{ {\tt{metaType}, \tt{fc}, \tt{fcb}}\right\}$.
On the other hand, the set $\mathit{Dep}_{\mathit{var}}(m'') = \left\{ {\tt{metaType}, \tt{btn}, \tt{cy}, \tt{cx}, \tt{cw}, \tt{ch}, \tt{buttons}, \tt{me}, \tt{rect}}\right\}$.
In this case, the intersection between these two sets contains only ${\tt{metaType}}$. Moreover, the computation of ${\tt fc}$ and ${\tt fcb}$ is isolated from the remaining code. Therefore, one can claim that $m'$ is cohesive and decoupled from $m''$, i.e.,~a good separation of concerns is achieved. \\

%Considering the code in Figure~\ref{image_example1latex} as an example, the set $\mathit{Dep}_{\mathit{var}}$ for the highlighted code is $\left\{ {\tt{metaType}, \tt{fc}, \tt{fcb}}\right\}$. On the other hand, the set $\mathit{Dep}_{\mathit{var}}$ for the remaining statements is $\left\{ {\tt{metaType}, \tt{btn}, \tt{cy}, \tt{cx}, \tt{cw}, \tt{ch}, \tt{buttons}, \tt{me}, \tt{rect}}\right\}$.
%In this case, the intersection between the two sets is $\left\{ {\tt{metaType}}\right\}$.

% Entity sets for example1
%\scriptsize
%\begin{align*}
%Pack_{\bigcap} &= \left\{ {\tt{argouml}, \tt{argouml.uml}, \tt{argouml.uml.diagram}, \tt{argouml.uml.diagram.ui}, \tt{tigris}, \tt{tigris.gef}}\right\} \\
%Pack_{M'} &= \left\{ {\tt{java.awt}, \tt{java.awt.event}, \tt{javax.swing}, \tt{tigris.gef.presentation}}\right\} \\
%Pack_{C} &= \left\{ {\tt{argouml.uml.diagram.staticstructure}, \tt{argouml.uml.diagram.staticstructure.ui}, \tt{tigris.gef.base}}\right\} \\
%Type_{\bigcap} &= \left\{ {}\right\} \\
%Type_{M'} &= \left\{ {\tt{Rectangle}, \tt{MouseEvent}, \tt{InputEvent}, \tt{Icon}, \tt{SelectionClassifierBox}, \tt{Button}, \tt{Fig}}\right\} \\
%Type_{C} &= \left\{ {\tt{FigCompartment}, \tt{FigCompartmentBox}, \tt{FigClassifierBox}, \tt{Selection}}\right\} \\
%Var_{\bigcap} &= \left\{ {\tt{metaType}}\right\} \\
%Var_{M'} &= \left\{ {\tt{btn}, \tt{cy}, \tt{cx}, \tt{cw}, \tt{ch}, \tt{buttons}, \tt{me}, \tt{rect}}\right\} \\
%Var_{C} &= \left\{ {\tt{fc}, \tt{fcb}}\right\}
%\end{align*}
%\normalsize


\noindent{\bf Scoring Function}: To compute the dissimilarity between $m'$ and $m''$, we rely on the distance between the dependency sets $Dep'$ and $Dep''$ using the Kulczynski similarity coefficient~\cite{terra-seke-13,jmove}:
\[
dist(Dep',Dep'') = 1 - \frac{1}{2}\Big[\frac{a}{(a+b)}+\frac{a}{(a+c)}\Big]
\]
\noindent where  ${a}= |Dep'\, \bigcap\, Dep''|$,  ${b}= |Dep' \setminus Dep''|$, and \mbox{${c}= | Dep'' \setminus Dep'|$}.\\

Thus, let $m'$ be the selection of statements of an Extract Method candidate for method $m$. Let also $m''$ be the remaining statements in $m$. The score of $m'$ is defined as:\\

$
\begin{array}{ll}
\mathit{score}(m')  = & 1/3 \times \mathit{dist}(\mathit{Dep}_{\mathit{var}}(m'),\mathit{Dep}_{\mathit{var}}(m''))\,\,\, + \\[.1cm]
                     & 1/3 \times \mathit{dist}(\mathit{Dep}_{\mathit{type}}(m'),\mathit{Dep}_{\mathit{type}}(m''))\,\,\, + \\[.1cm]
                     & 1/3 \times \mathit{dist}(\mathit{Dep}_{\mathit{pack}}(m'),\mathit{Dep}_{\mathit{pack}}(m'')) \\[.1cm]
\end{array}
$\\

The scoring function is centered on the observation that 
a good Extract Method candidate should encapsulate the use of variables, types, and packages. In other words, we should maximize the distance between the dependency sets $Dep'$ and $Dep''$.

\subsubsection{{Ranking}}
%
This phase is responsible for ranking and filtering the Extract Method candidates
based on the score computed in the previous phase.
Basically, we sort the candidates and filter them according to the following parameters: (i)~{\em Maximum Recommendations per Method}. The value is preset to 3 {\small (changeable)}, which means that the tool triggers up to three recommendations for each method; and (ii)~{\em Minimum Score Value}, which has to be configured when the user desires
to setup a minimum dissimilarity threshold. 
%The value is preset to 0.0 {\small (changeable)}, which means that the tool does
%not filter methods 
%the extracted method %($m'$) 
%must be at least 50\% {dissimilar} to the remaining statements in the original method.
% ($m''$).  






\subsection{{Internal Architecture and Interface}}
\label{sec:archinterface}

We implemented \var{JExtract} as a plug-in on top of the Eclipse platform. 
Therefore, we rely mainly on native Eclipse APIs, such as Java Development Tools (JDT)
and Language Toolkit (LTK). 
The current \var{JExtract} implementation follows an architecture with five main modules:\\[-0.1cm]



%We have implemented DCLfix as an extension of the DCLcheck Eclipse plug-in. As il- lustrated in Figure 2a, DCLfix exploits preexisting data structures, such as the graph of existing dependencies, the defined architectural constraints, and the detected violations. Moreover, DCLfix also reuses functions implemented in DCLcheck, e.g., to check whether a type can establish a particular de	pendency with another type.%The current DCLfix implementation follows an architecture with three main modules:


%The current \mbox{JExtract} implementation %has six classes and 1,170 LOC and 
%follows an architecture with four main modules:

\begin{enumerate}

\item {\em Code Analyzer}: This module provides the following services to other
modules: (a)~it builds the structure of block and statements {\small (refer to Subsection~\ref{sec:candidate_generation})}; (b)~it extracts the structural dependencies
{\small (refer to Subsection~\ref{sec:scoring_function})}; and (c)~it checks if an Extract
Method candidate satisfies the underlying Eclipse Extract Method refactoring preconditions.
% of the blocks and statements s the source code to build a model representing each method. This model includes the block structure and entity dependencies. \\[-0.2cm]
In fact, this module contains most communication between \var{JExtract}
and Eclipse APIs (e.g., \var{org.eclipse.jdt.core} and \var{org.eclipse.ltk.core.refactoring}). \\[-0.1cm]

%1a. JDT (AST)
%      Gerar SX.Y (modelo de blocos) 
%      meta-informa��es
%      org.eclipse.jdt.core
%1b. Testar pr�-condi�oes
%      Verifica��o na API de refactoring do Eclipse para testar pr�-condi��es.
%      org.eclipse.ltk.core.refactoring
%1c. Depend�ncia do m�todo (Dep(S))

\item {\em Candidate Generator}: This module generates all Extract Method candidates based on Algorithm~\ref{algo:generation} and hence depends on service (a) of module {\em Code Analyzer}.\\[-0.1cm]
%Algoritmo 1. Fazendo testes Code Analyzer. (pre-condi��es)


\item {\em Scorer}: This module calculates the dissimilarity of the Extract Method candidates generated by module {\em Candidate Generator} {\small (refer to Subsection~\ref{sec:scoring_function})} and hence
depends on service (b) of module {\em Code Analyzer}.\\[-0.1cm]


\item {\em Ranker}:
This module ranks and filters the Extract Method candidates generated by  
module {\em Candidate Generator} and scored by module {\em Scorer}. It depends
on service~(c) of module {\em Code Analyzer} to filter candidates not satisfying preconditions.
\\[-0.1cm]



\item {\em UI}: This module consists of the front-end of the tool, which relies on the Eclipse UI API (\var{org.eclipse.ui}) to implement two menu extensions, six actions, and one main view.
Moreover, it depends on module UI from LTK (\var{org.eclipse.ltk.ui.refactoring}) to
delegate the refactoring appliance to the underlying Eclipse Extract Method refactoring tool.\\[-0.1cm]
%Pontos de extens�o em menus.
%Uma View principal.
%E v�rios a��es (action).
%Apply Refactoring (chama o underlying Extract Method refactoring tool wizard (preenchido os dados))
%   - org.eclipse.ltk.ui.refactoring



%\item {\em Code Analyzer}: 
%Analyzes the source code to build a model representing each method. This model includes the block structure and entity dependencies. \\[-0.2cm]
%
%\item {\em Ranker}: 
%Provides the scoring function and filtering criteria to Extract Method candidates.\\[-0.2cm]
%
%\item {\em Generator}: 
%Generates Extract Method recommendations. Depends on the Ranker and Code Analyzer.\\[-0.2cm]
%
%\item {\em UI}: 
%User interface of the tool, containing the actions and views of the plug-in.\\[-0.2cm]

\end{enumerate}

Such architecture permits the extension of our tool. For example, the {\em Scorer} module may be replaced by one that employs a new heuristic based on semantic and structural information. As another example, the {\em Candidate Generator} module may be extended to support the identification of non-contiguous code fragments.


\begin{figure}[htpb]
\vspace{-6pt}
\centering
\includegraphics[width=0.8\textwidth]{img/ui.png}
\caption{JExtract UI}
\label{img_uilatex}
\vspace{-6pt}
\end{figure}

Figure~\ref{img_uilatex} presents \var{JExtract}'s UI, displaying method \var{mouseReleased} previously presented in Figure~\ref{image_example1latex}. When a developer triggers \var{JExtract} to identify Extract Method refactoring opportunities for this method, it opens the {\em Extract Method Recommendations} view to report the potential recommendations. In this case, the best candidate consists of the extraction of statements $\tt S3.2$ to $\tt S3.5$ whose dissimilarity score is 0.7148.

%Figure~\ref{img_uilatex} illustrates shows a screenshot of the tool, in which we can see the {\em Extract Method Recommendations view} at the bottom of the screen, displaying the recommendations found for the example method of Figure~\ref{image_example1latex}.



%a constraint of the form Controller cannot?depend HibernateDAO. This constraint prevents the Controller layer from manipulating directly Hibernate Data Access Objects (DAOs). Assume also a class Controller that declares a variable of a type ProductHibernateDAO. When the developer requests a recommended fix for such violation, DCLfix indicates the most appropriate refactoring (see Figure 2b). The provided recommendation suggests replacing the declaration of the unauthorized type ProductHibernateDAO with its interface IProductDAO (which corresponds to recom- mendation D1 in Table 1). This recommendation is particularly useful to handle viola- tions due to references to a concrete implementation of a service, instead of its general interface.




\subsection{{Evaluation}}
\label{sec:examples}

We conducted two different but complementary empirical studies.

\vspace{5pt}
\noindent{\bf Study \#1}:
%
In our previous paper~\cite{2014_icpc}, we evaluated the recommendations provided by our tool on three systems to assess precision and recall. 
We extended this study to consider minor modifications to the ranking method and to compare the results with \var{JDeodorant}, a state-of-the-art tool that identifies Extract Method opportunities~\cite{tsantalis11}.
%
For each system~$S$, we apply random Inline Method refactoring operations to obtain a modified version~$S'$. 
%
%To perform that evaluation, we fist need a set of known Extract Method opportunities we expect to be found to be used as an oracle. To build the oracle for the first system, \mbox{MyWebMarket}, we based on a past modularization study from the second author of this paper~\cite{csmr2012era}. \mbox{MyWebMarket} was built as monolithic system, which was then modularized on subsequent versions. 
%For this reason, we were able to use the actual Extract Method refactoring operations applied on the modularization process as our oracle.
%For the two other systems, we used a strategy of modifying the original versions by applying Inline Method refactoring operations.
%
We assume that good Extract Method opportunities are the ones that revert the modifications (i.e., restoring $S$ from $S'$).

%the system had a better modularization before the modification and reverting the {modification} would be a good Extract Method opportunity 
%(possibly the best of its method). 

%IMPORTANTE
%footnote sobre MyWebMarket


%IMPORTANTE
%This assumption may not hold true for all modifications, but it is reasonable to expect that it will hold true for most cases, specially when we consider that the chosen systems were designed by expert software engineers.



\begin{table}[H]
\vspace{-6pt}
\caption{Study \#1 -- Recall and precision results} %for the first selection of systems}
\begin{center}
{\footnotesize
\begin{tabular}{lr|rr|rr|rr|rr}
\hline
& & \multicolumn{6}{c|}{\vart{JExtract}} & \multicolumn{2}{c}{\vart{JDeodorant}} \\
 & & \multicolumn{2}{c}{\bf Top-1} & \multicolumn{2}{c}{\bf Top-2} & \multicolumn{2}{c|}{\bf Top-3} &  \\
{\bf System} & \multicolumn{1}{c|}{\bf\#} & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c}{\bf Prec.}  & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c}{\bf Prec.}  & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c|}{\bf Prec.} & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c}{\bf Prec.} \\
\hline
JHotDraw~5.2 & 56 & 19 {\scriptsize(34\%)} & 34\% & 26 {\scriptsize(46\%)} & 24\% & 32 {\scriptsize(57\%)} & 20\% & 2 {\scriptsize(4\%)} & 5\% \\
JUnit~3.8 & 25 & 13 {\scriptsize(52\%)} & 52\% & 16 {\scriptsize(64\%)} & 33\% & 18 {\scriptsize(72\%)} & 25\% & 0 {\scriptsize(0\%)} & 0\% \\
MyWebMarket & 14 & 12 {\scriptsize(86\%)} & 86\% & 14 {\scriptsize(100\%)} & 50\% & 14 {\scriptsize(100\%)} & 33\% & 2 {\scriptsize(14\%)} & 33\% \\
\hline
{\bf\em Total} & {\bf\em 95} & {\bf\em 44 {\scriptsize(46\%)}} & 46\% & {\bf\em 56 {\scriptsize(59\%)}} & {\bf\em 30\%} & {\bf\em 64 {\scriptsize(67\%)}} & {\bf\em 23\%} & {\bf\em 4 {\scriptsize(4\%)}} & {\bf\em 6\%} \\
\hline
\end{tabular}
\label{table:evaluation1}
}
\end{center}
\vspace{-6pt}
\end{table}

Table~\ref{table:evaluation1} reports recall and precision values achieved using \var{JExtract} with three different configurations {\small ({\em Top-k Recommendations per Method})}. 
While a high parameter value favors recall {\small (e.g., Top-3)}, a low one favors precision {\small (e.g., Top-1)}. Table~\ref{table:evaluation1} also presents results achieved using \var{JDeodorant} with its default settings.
%On one hand, the higher the parameter value, the higher the recall. On the other hand, the lower the parameter value, the higher the precision. Thus, while Top-3 favors recall, Top-1 favors precision. 
As the main finding, \var{JExtract} outperforms \var{JDeodorant} regardless of the configuration used.
%For example, \var{JExtract} in the Top-1 configuration achieves recall and precision results 1000\% and 640\% higher than \var{JDeodorant}, \mbox{respectively}.

%Table~\ref{table:evaluation1} shows the recall and precision values achieved using three different configurations of the parameter {\em Maximum Recommendations per Method} (Top-1, Top-2, and Top-3), considering only recommendations for methods covered by our oracle. While the Top-1 configuration favors precision, Top-3 favors recall. Nevertheless, \mbox{JExtract} outperforms \mbox{JDeodorant} in any of the three configurations used. For example, the best scenario for \mbox{JExtract} is the Top-1 configuration, where recall and precision are 683\% and 840\% higher, respectively.\\

\vspace{5pt}
\noindent{\bf Study \#2}:
%
%\subsubsection{Extended study with Open-source systems}
%\label{sec:extendedstudy}
%
We replicate the previous study in other ten popular open-source Java systems
to assess how the precision and recall rates would vary. Nevertheless, we do not compare our results with \var{JDeodorant} since we were not able to reliably provide the source code of all required libraries, as demanded by JDeodorant.
%\footnote{\scriptsize JDeodorant's analysis relies on the source code of library/API classes to work properly. As we were not able to reliably provide the source code for all dependent libraries of the involved systems, we did not used the results given by JDeodorant in the extended experiment as it would not be a fair comparison.}

%Similarly to the previous study, we introduced known Extract Method instances by inlining method invocations, yielding a total of $5477$ known instances.

%We extended the previous case studies, including a selection of 10 popular open-source Java systems, to assess how the precision and recall rates would vary. Similarly to the previous study, we introduced known Extract Method instances by inlining method invocations, yielding a total of $5477$ known instances.

\begin{table}[htbp]
\vspace{-6pt}
\caption{Study \#2 -- Recall and precision results}
\begin{center}
{\footnotesize
\begin{tabular}{l@{\hspace{3pt}}r|rr|rr|rr}
\hline
% & & \multicolumn{2}{c|}{Top-1} & \multicolumn{2}{c|}{Top-2} & \multicolumn{2}{c}{Top-3} \\
%System & \# & Rec. & Prec.  & Rec. & Prec.  & Rec. & Prec. \\
& & \multicolumn{6}{c}{\vart{JExtract}} \\
 & & \multicolumn{2}{c}{\bf Top-1} & \multicolumn{2}{c}{\bf Top-2} & \multicolumn{2}{c}{\bf Top-3}  \\
{\bf System} & \multicolumn{1}{c|}{\bf \#} & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c}{\bf Prec.}  & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c}{\bf Prec.}  & \multicolumn{1}{c}{\bf Recall} & \multicolumn{1}{c}{\bf Prec.} \\
\hline
Ant~1.8.2 & 964 & 235 {\scriptsize (24.4\%)} & 24.4\%  & 363 {\scriptsize (37.7\%)} & 19.1\% & 460 {\scriptsize (47.7\%)} & 16.3\% \\
ArgoUML~0.34 & 439 & 98 {\scriptsize (22.3\%)} & 22.3\%  & 160 {\scriptsize (36.4\%)} & 18.3\% & 186 {\scriptsize (42.4\%)} & 14.4\% \\
Checkstyle~5.6 & 533 & 227 {\scriptsize (42.6\%)} & 42.6\%  & 338 {\scriptsize (63.4\%)} & 31.9\% & 389 {\scriptsize (73.0\%)} & 24.7\% \\
FindBugs~1.3.9 & 714 & 179 {\scriptsize (25.1\%)} & 25.1\%  & 278 {\scriptsize (38.9\%)} & 19.7\% & 350 {\scriptsize (49.0\%)} & 16.7\% \\
FreeMind~0.9.0 & 348 & 85 {\scriptsize (24.4\%)} & 24.4\%  & 134 {\scriptsize (38.5\%)} & 19.4\% & 181 {\scriptsize (52.0\%)} & 17.8\% \\
JFreeChart~1.0.13 & 1,090 & 204 {\scriptsize (18.7\%)} & 18.7\%  & 396 {\scriptsize (36.3\%)} & 18.2\% & 536 {\scriptsize (49.2\%)} & 16.5\% \\
JUnit~4.10 & 35 & 11 {\scriptsize (31.4\%)} & 32.4\%  &  17 {\scriptsize (48.6\%)} & 26.6\% &  22 {\scriptsize (62.9\%)} & 23.7\% \\
Quartz~1.8.3 & 239 & 99 {\scriptsize (41.4\%)} & 41.4\%  & 125 {\scriptsize (52.3\%)} & 26.5\% & 142 {\scriptsize (59.4\%)} & 20.4\% \\
SQuirreL~SQL~3.1.2 & 39 & 15 {\scriptsize (38.5\%)} & 38.5\%  &  18 {\scriptsize (46.2\%)} & 23.7\% &  20 {\scriptsize (51.3\%)} & 18.2\% \\
Tomcat~7.0.2 & 1,076 & 214 {\scriptsize (19.9\%)} & 19.9\%  & 325 {\scriptsize (30.2\%)} & 15.2\% & 409 {\scriptsize (38.0\%)} & 12.8\% \\
\hline
{\bf\em Total} & {\bf\em 5,477} & {\bf\em 1,367 {\scriptsize (25.0\%)}} & {\bf\em 25.0\%} & {\bf\em 2,154 {\scriptsize (39.3\%)}} & {\bf\em 19.8\%} & {\bf\em 2,695 {\scriptsize (49.2\%)}} & {\bf\em 16.7\%} \\
\hline
\end{tabular}
\label{table:evaluation2}
}
\end{center}
\vspace{-6pt}
\end{table}

Table~\ref{table:evaluation2} reports the 
recall and precision values achieved using the same settings from the previous study. 
On one hand, the overall recall value ranges from 25\% to 49.2\%. On the other hand, the overall precision value ranges from 25\% to 16.7\%. 
We argue these values are acceptable for two reasons: (i)~we only consider as correct a recommendation that matches exactly the one at the oracle; thus, a slight difference of including (or excluding) a statement is enough to be considered a miss; and (ii) the modified methods may have preexisting Extract Method opportunities, besides the ones we introduced, that will be considered wrong by our oracle.% 

%\footnote{\scriptsize JDeodorant's analysis relies on the source code of library/API classes to work properly. As we were not able to reliably provide the source code for all dependent libraries of the involved systems, we did not used the results given by JDeodorant in the extended experiment as it would not be a fair comparison.}

%Table~\ref{table:evaluation2} shows the results of our tool using the same settings from the previous study. The recall rate including all systems ranges from 25\% to 49.2\%. By the other side, precision ranges from 25\% to 16.7\%. We consider these values to be acceptable for two main reasons. We only consider to be correct a recommendation that exactly matches the one at the oracle, so that a slight difference of including (or excluding) a statement is enough to be considered a miss. Additionally, the modified methods may have existing Extract Method opportunities, besides the ones we introduced, that will be considered wrong by our oracle.% 
%\footnote{\scriptsize JDeodorant's analysis relies on the source code of library/API classes to work properly. As we were not able to reliably provide the source code for all dependent libraries of the involved systems, we did not used the results given by JDeodorant in the extended experiment as it would not be a fair comparison.}


