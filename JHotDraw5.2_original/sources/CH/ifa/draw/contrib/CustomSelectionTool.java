/*
 * @(#)CustomSelectionTool.java 5.2
 *
 */

package CH.ifa.draw.contrib;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import CH.ifa.draw.framework.DrawingView;
import CH.ifa.draw.framework.Figure;
import CH.ifa.draw.standard.SelectionTool;

/**
 * A SelectionTool, which recognizes double clicks and popup menu triggers.
 * If a double click or popup trigger is encountered a hook method is called,
 * which handles the event. This methods can be overriden in subclasse to 
 * provide customized behaviour. 
 * Popup menus must be registered with a Figure using the setAttribute() method.
 * The key which associates a popup menu as an attribute is Figure.POPUP_MENU.
 *
 * @author  Wolfram Kaiser
 * @version JHotDraw 5.2    31.08.1999
 */
public class CustomSelectionTool extends SelectionTool {

    /**
     * Create an instance of this SelectionTool for the given view
     *
     * @param   view    DrawingView for which the SelectionTool operates
     */
	public CustomSelectionTool(DrawingView view) {
		super(view);
	}
	
	/**
	 * MouseListener method for mouseDown events. If the popup trigger has been
	 * activated, then the appropriate hook method is called.
	 *
	 * @param   e   MouseEvent which should be interpreted
	 * @param   x   x coordinate of the MouseEvent
	 * @param   y   y coordinate of the MouseEvent
	 */
	public void mouseDown(MouseEvent e, int x, int y) {
		// isPopupTrigger() at mouseDown() is only notified at UNIX systems
		if (e.isPopupTrigger()) {
		    /*{*/Figure figure = drawing().findFigure(e.getX(), e.getY());
			if (figure != null) {
			    Object attribute = figure.getAttribute(Figure.POPUP_MENU);
			    if (attribute == null) {
			        figure = drawing().findFigureInside(e.getX(), e.getY());
			    }
				if (figure != null) {
			    	showPopupMenu(figure, e.getX(), e.getY(), e.getComponent());
			    }
			}/*}*/
		}
		else {
			super.mouseDown(e, x, y);
			handleMouseDown(e, x, y);
		}
	}
	
	/**
	 * MouseListener method for mouseDrag events. Usually, mouse drags are
	 * ignored for popup menus or double clicks.
	 *
	 * @param   e   MouseEvent which should be interpreted
	 * @param   x   x coordinate of the MouseEvent
	 * @param   y   y coordinate of the MouseEvent
	 */
	public void mouseDrag(MouseEvent e, int x, int y) {
		if (!e.isPopupTrigger()) {
			super.mouseDrag(e, x, y);
		}
	}

    /** 
	 * MouseListener method for mouseUp events. Depending on the kind of event
	 * the appropriate hook method is called (popupMenuUp for popup trigger,
	 * doubleMouseClick for a double click, and mouseUp() and mouseClick() for
	 * normal mouse clicks).
	 *
	 * @param   e   MouseEvent which should be interpreted
	 * @param   x   x coordinate of the MouseEvent
	 * @param   y   y coordinate of the MouseEvent
     */
	public void mouseUp(MouseEvent e, int x, int y) {
		if (e.isPopupTrigger()) {
		    /*{*/Figure figure = drawing().findFigure(e.getX(), e.getY());
			if (figure != null) {
			    Object attribute = figure.getAttribute(Figure.POPUP_MENU);
			    if (attribute == null) {
			        figure = drawing().findFigureInside(e.getX(), e.getY());
			    }
				if (figure != null) {
			    	showPopupMenu(figure, e.getX(), e.getY(), e.getComponent());
			    }
			}/*}*/
		}
		else if (e.getClickCount() == 2) {
			handleMouseDoubleClick(e, x, y);
		}
		else {
			super.mouseUp(e, x, y);
			handleMouseUp(e, x, y);
			handleMouseClick(e, x, y);
		}
	}
	
	/**
	 * Hook method which can be overriden by subclasses to provide
	 * specialised behaviour in the event of a mouse down.
	 */
	public void handleMouseDown(MouseEvent e, int x, int y) {
	}

		/**
	 * Hook method which can be overriden by subclasses to provide
	 * specialised behaviour in the event of a mouse up.
	 */
	public void handleMouseUp(MouseEvent e, int x, int y) {
	}

	/**
	 * Hook method which can be overriden by subclasses to provide
	 * specialised behaviour in the event of a mouse click.
	 */
	public void handleMouseClick(MouseEvent e, int x, int y) {
	}

	/**
	 * Hook method which can be overriden by subclasses to provide
	 * specialised behaviour in the event of a mouse double click.
	 */
	public void handleMouseDoubleClick(MouseEvent e, int x, int y) {
	}

	/**
	 * Hook method which can be overriden by subclasses to provide
	 * specialised behaviour in the event of a popup trigger.
	 */
	public void handlePopupMenu(MouseEvent e, int x, int y) {
		final int tmp1 = e.getX();
		final int tmp2 = e.getY();
		Figure figure = drawing().findFigure(tmp1, tmp2);
        if (figure != null) {
            Object attribute = figure.getAttribute(Figure.POPUP_MENU);
            if (attribute == null) {
                figure = drawing().findFigureInside(tmp1, tmp2);
            }
        	if (figure != null) {
            	final Component tmp3 = e.getComponent();
				/*{*/Object attribute1 = figure.getAttribute(Figure.POPUP_MENU);
				if ((attribute1 != null) && (attribute1 instanceof JPopupMenu)) {
				    JPopupMenu popup = (JPopupMenu)attribute1;
				    if (popup instanceof PopupMenuFigureSelection) {
				        ((PopupMenuFigureSelection)popup).setSelectedFigure(figure);
				    }
				    // calculate offsets for internal MDI frames
				    Point newLocation = new Point(tmp1, tmp2);
				    adjustOffsets(tmp3.getParent(), newLocation);
					popup.setLocation(newLocation);
					popup.setInvoker(tmp3);
					popup.setVisible(true);
				}/*}*/
            }
		}
	}

	/**
	 * This method displays a popup menu, if there is one registered with the
	 * Figure (the Figure's attributes are queried for Figure.POPUP_MENU which
	 * is used to indicate an association of a popup menu with the Figure).
	 *
	 * @param   figure      Figure for which a popup menu should be displayed
	 * @param   x           x coordinate where the popup menu should be displayed
	 * @param   y           y coordinate where the popup menu should be displayed
	 * @param   component   Component which invoked the popup menu
	 */
	public void showPopupMenu(Figure figure, int x, int y, Component comp) {
        Object attribute = figure.getAttribute(Figure.POPUP_MENU);
        if ((attribute != null) && (attribute instanceof JPopupMenu)) {
    	    JPopupMenu popup = (JPopupMenu)attribute;
    	    if (popup instanceof PopupMenuFigureSelection) {
    	        ((PopupMenuFigureSelection)popup).setSelectedFigure(figure);
    	    }
    	    // calculate offsets for internal MDI frames
    	    Point newLocation = new Point(x, y);
    	    final Container tmp1 = comp.getParent();
			adjustOffsets(tmp1, newLocation);
    		popup.setLocation(newLocation);
    		popup.setInvoker(comp);
    		popup.setVisible(true);
		}
	}
	
	/**
	 * Internal MDI frames have offsets where a popup menu should be
	 * shown (in JDK 1.2).
	 * This method sums up iteratively all x and y offsets of all
	 * parent compontents until the top parent component is reached.
	 */
	public void adjustOffsets(Component comp, Point offsetPoint) {
		if (comp != null) {
			Point compLocation = comp.getLocation();
			offsetPoint.translate(compLocation.x, compLocation.y);
			final Container tmp1 = comp.getParent();
			adjustOffsets(tmp1, offsetPoint);
		}
	}
}