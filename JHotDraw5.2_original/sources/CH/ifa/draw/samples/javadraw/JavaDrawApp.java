/*
 * @(#)JavaDrawApp.java 5.2
 *
 */

package CH.ifa.draw.samples.javadraw;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

import CH.ifa.draw.framework.*;
import CH.ifa.draw.standard.*;
import CH.ifa.draw.figures.*;
import CH.ifa.draw.util.*;
import CH.ifa.draw.application.*;
import CH.ifa.draw.contrib.*;

public  class JavaDrawApp extends MDI_DrawApplication {

    public Animator            fAnimator;
    static public String       fgSampleImagesPath = "CH/ifa/draw/samples/javadraw/sampleimages/";
    static public String       fgSampleImagesResourcePath = "/"+fgSampleImagesPath;

    public JavaDrawApp() {
        super("JHotDraw");
    }

    /**
     * Factory method which create a new instance of this
     * application.
     *
     * @return	newly created application
     */
	public DrawApplication createApplication() {
		return new JavaDrawApp();
	}
	
    public void open() {
        super.open();
    }

    //-- application life cycle --------------------------------------------

    public void destroy() {
        super.destroy();
        endAnimation();
    }

    //-- DrawApplication overrides -----------------------------------------

    public void createTools(JToolBar palette) {
        super.createTools(palette);

        Tool tool = new TextTool(view(), new TextFigure());
        palette.add(createToolButton(IMAGES+"TEXT", "Text Tool", tool));

        tool = new ConnectedTextTool(view(), new TextFigure());
        palette.add(createToolButton(IMAGES+"ATEXT", "Connected Text Tool", tool));

        tool = new URLTool(view());
        palette.add(createToolButton(IMAGES+"URL", "URL Tool", tool));

        tool = new CreationTool(view(), new RectangleFigure());
        palette.add(createToolButton(IMAGES+"RECT", "Rectangle Tool", tool));

        tool = new CreationTool(view(), new RoundRectangleFigure());
        palette.add(createToolButton(IMAGES+"RRECT", "Round Rectangle Tool", tool));

        tool = new CreationTool(view(), new EllipseFigure());
        palette.add(createToolButton(IMAGES+"ELLIPSE", "Ellipse Tool", tool));

        tool = new CreationTool(view(), new LineFigure());
        palette.add(createToolButton(IMAGES+"LINE", "Line Tool", tool));

        tool = new ConnectionTool(view(), new LineConnection());
        palette.add(createToolButton(IMAGES+"CONN", "Connection Tool", tool));

        tool = new ConnectionTool(view(), new ElbowConnection());
        palette.add(createToolButton(IMAGES+"OCONN", "Elbow Connection Tool", tool));

        tool = new ScribbleTool(view());
        palette.add(createToolButton(IMAGES+"SCRIBBL", "Scribble Tool", tool));

        tool = new PolygonTool(view());
        palette.add(createToolButton(IMAGES+"POLYGON", "Polygon Tool", tool));

        tool = new BorderTool(view());
        palette.add(createToolButton(IMAGES+"BORDDEC", "Border Tool", tool));
    }

    public Tool createSelectionTool() {
        return new MySelectionTool(view());
    }

    public void createMenus(JMenuBar mb) {
		super.createMenus(mb);
		mb.add(createAnimationMenu());
		mb.add(createImagesMenu());
		mb.add(createWindowMenu());
    }

    public JMenu createAnimationMenu() {
		JMenu menu = new JMenu("Animation");
		JMenuItem mi = new JMenuItem("Start Animation");
		mi.addActionListener(
		    new ActionListener() {
		        public void actionPerformed(ActionEvent event) {
		            startAnimation();
		        }
		    }
		);
		menu.add(mi);

		mi = new JMenuItem("Stop Animation");
		mi.addActionListener(
		    new ActionListener() {
		        public void actionPerformed(ActionEvent event) {
		            /*{*/if (fAnimator != null) {
					    fAnimator.end();
					    fAnimator = null;
					}/*}*/
		        }
		    }
		);
		menu.add(mi);
		return menu;
	}

    public JMenu createWindowMenu() {
		JMenu menu = new JMenu("Window");
		JMenuItem mi = new JMenuItem("New View");
		mi.addActionListener(
		    new ActionListener() {
		        public void actionPerformed(ActionEvent event) {
		            newView();
		        }
		    }
		);
		menu.add(mi);
		mi = new JMenuItem("New Window");
		mi.addActionListener(
		    new ActionListener() {
		        public void actionPerformed(ActionEvent event) {
		            newWindow();
		        }
		    }
		);
		menu.add(mi);
		return menu;
	}

    public JMenu createImagesMenu() {
		CommandMenu menu = new CommandMenu("Images");
		File imagesDirectory = new File(fgSampleImagesPath);
		try {
		    String[] list = imagesDirectory.list();
		    for (int i = 0; i < list.length; i++) {
		        String name = list[i];
		        String path = fgSampleImagesResourcePath+name;
		        final InsertImageCommand tmp1 = new InsertImageCommand(name, path, view());
				/*{*/JMenuItem m = new JMenuItem(tmp1.name());
				m.addActionListener(menu);
				menu.add(m);
				menu.fCommands.addElement(tmp1);/*}*/
		    }
		} catch (Exception e) {}
		return menu;
	}

    public Drawing createDrawing() {
        return new BouncingDrawing();
        //return new StandardDrawing();
    }

    //---- animation support --------------------------------------------

    public void startAnimation() {
        if (drawing() instanceof Animatable && fAnimator == null) {
            fAnimator = new Animator((Animatable)drawing(), view());
            fAnimator.start();
        }
    }

    public void endAnimation() {
        if (fAnimator != null) {
            fAnimator.end();
            fAnimator = null;
        }
    }

    //-- main -----------------------------------------------------------

	public static void main(String[] args) {
		JavaDrawApp window = new JavaDrawApp();
		window.open();
    }
}
