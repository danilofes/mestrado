/*
 * @(#)StorableOutput.java 5.2
 *
 */

package CH.ifa.draw.util;

import java.util.*;
import java.io.*;
import java.awt.Color;

/**
 * An output stream that can be used to flatten Storable objects.
 * StorableOutput preserves the object identity of the stored objects.
 *
 * @see Storable
 * @see StorableInput
 */


public  class StorableOutput extends Object {

    public PrintWriter     fStream;
    public Vector          fMap;
    public int             fIndent;

    /**
     * Initializes the StorableOutput with the given output stream.
     */
    public StorableOutput(OutputStream stream) {
        fStream = new PrintWriter(stream);
        fMap = new Vector();
        fIndent = 0;
    }

    /**
     * Writes a storable object to the output stream.
     */
    public void writeStorable(Storable storable) {
        if (storable == null) {
            fStream.print("NULL");
            space();
            return;
        }

        if (mapped(storable)) {
            /*{*/int ref = fMap.indexOf(storable);
			
			fStream.print("REF");
			space();
			fStream.print(ref);
			space();/*}*/
            return;
        }

        incrementIndent();
        startNewLine();
        map(storable);
        fStream.print(storable.getClass().getName());
        space();
        storable.write(this);
        space();
        decrementIndent();
    }

    /**
     * Writes an int to the output stream.
     */
    public void writeInt(int i) {
        fStream.print(i);
        space();
    }

    public void writeColor(Color c) {
        writeInt(c.getRed());
        writeInt(c.getGreen());
        writeInt(c.getBlue());
    }

    /**
     * Writes an int to the output stream.
     */
    public void writeDouble(double d) {
        fStream.print(d);
        space();
    }

    /**
     * Writes an int to the output stream.
     */
    public void writeBoolean(boolean b) {
        if (b)
            fStream.print(1);
        else
            fStream.print(0);
        space();
    }

    /**
     * Writes a string to the output stream. Special characters
     * are quoted.
     */
    public void writeString(String s) {
        fStream.print('"');
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch(c) {
                case '\n': fStream.print('\\'); fStream.print('n'); break;
                case '"' : fStream.print('\\'); fStream.print('"'); break;
                case '\\': fStream.print('\\'); fStream.print('\\'); break;
                case '\t': fStream.print('\\'); fStream.print('\t'); break;
                default: fStream.print(c);
            }

        }
        fStream.print('"');
        space();
    }

    /**
     * Closes a storable output stream.
     */
    public void close() {
        fStream.close();
    }

    public boolean mapped(Storable storable) {
        return fMap.contains(storable);
    }

    public void map(Storable storable) {
        if (!fMap.contains(storable))
            fMap.addElement(storable);
    }

    public void writeRef(Storable storable) {
        int ref = fMap.indexOf(storable);

        fStream.print("REF");
        space();
        fStream.print(ref);
        space();
    }

    public void incrementIndent() {
        fIndent += 4;
    }

    public void decrementIndent() {
        fIndent -= 4;
        if (fIndent < 0) fIndent = 0;
    }

    public void startNewLine() {
        fStream.println();
        for (int i=0; i<fIndent; i++)
            space();
    }

    public void space() {
        fStream.print(' ');
    }

}
