/*
 * @(#)PolyLineConnector.java 5.2
 *
 */

package CH.ifa.draw.figures;

import java.awt.*;
import java.io.IOException;
import CH.ifa.draw.framework.*;
import CH.ifa.draw.standard.*;
import CH.ifa.draw.util.*;

/**
 * PolyLineConnector finds connection points on a
 * PolyLineFigure.
 *
 * @see PolyLineFigure
 */
public class PolyLineConnector extends ChopBoxConnector {

    /*
     * Serialization support.
     */
    static final public long serialVersionUID = 6018435940519102865L;

    public PolyLineConnector() {
        super();
    }

    /**
     * Constructs a connector with the given owner figure.
     */
    public PolyLineConnector(Figure owner) {
        super(owner);
    }

    public Point chop(Figure target, Point from) {
        PolyLineFigure p = (PolyLineFigure)owner();
        // *** based on PolygonFigure's heuristic
        Point ctr = p.center();
        int cx = -1;
        int cy = -1;
        long len = Long.MAX_VALUE;

        // Try for points along edge

        final int tmp5 = from.x;
		final int tmp6 = from.y;
		for (int i = 0; i < p.pointCount()-1; i++) {
            Point p1 = p.pointAt(i);
            Point p2 = p.pointAt(i+1);
            final int tmp1 = p1.x;
			final int tmp2 = p1.y;
			final int tmp3 = p2.x;
			final int tmp4 = p2.y;
			final int tmp7 = ctr.x;
			final int tmp8 = ctr.y;
			Point chop = Geom.intersect(tmp1,
                                 tmp2,
                                 tmp3,
                                 tmp4,
                                 tmp5,
                                 tmp6,
                                 tmp7,
                                 tmp8);
            if (chop != null) {
                long cl = Geom.length2(chop.x, chop.y, tmp5, tmp6);
                if (cl < len) {
                    len = cl;
                    cx = chop.x;
                    cy = chop.y;
                }
            }
        }
        // if none found, pick closest vertex
        //if (len ==  Long.MAX_VALUE) {
        { // try anyway
            for (int i = 0; i < p.pointCount(); i++) {
                Point pp = p.pointAt(i);
                long l = Geom.length2(pp.x, pp.y, tmp5, tmp6);
                if (l < len) {
                    len = l;
                    cx = pp.x;
                    cy = pp.y;
                }
            }
        }
        return new Point(cx, cy);
    }
}

