/*
 * @(#)DuplicateCommand.java 5.2
 *
 */

package CH.ifa.draw.standard;

import java.util.*;

import CH.ifa.draw.util.*;
import CH.ifa.draw.framework.*;

/**
 * Duplicate the selection and select the duplicates.
 */
public class DuplicateCommand extends FigureTransferCommand {

   /**
    * Constructs a duplicate command.
    * @param name the command name
    * @param view the target view
    */
    public DuplicateCommand(String name, DrawingView view) {
        super(name, view);
    }

    public void execute() {
        FigureSelection selection = fView.getFigureSelection();

        fView.clearSelection();

        Vector figures = (Vector)selection.getData(FigureSelection.TYPE);
        /*{*/FigureEnumeration e = new FigureEnumerator(figures);
		while (e.hasMoreElements()) {
		    Figure figure = e.nextFigure();
		    figure.moveBy(10, 10);
		    figure = fView.add(figure);
		    fView.addToSelection(figure);
		}/*}*/
        fView.checkDamage();
    }

    public boolean isExecutable() {
        return fView.selectionCount() > 0;
    }

}


