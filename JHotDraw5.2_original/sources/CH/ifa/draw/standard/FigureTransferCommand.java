/*
 * @(#)FigureTransferCommand.java 5.2
 *
 */

package CH.ifa.draw.standard;

import java.util.*;
import CH.ifa.draw.util.*;
import CH.ifa.draw.framework.*;

/**
 * Common base clase for commands that transfer figures
 * between a drawing and the clipboard.
 */
abstract class FigureTransferCommand extends Command {

    public DrawingView fView;

   /**
    * Constructs a drawing command.
    * @param name the command name
    * @param view the target view
    */
    public FigureTransferCommand(String name, DrawingView view) {
        super(name);
        fView = view;
    }

   /**
    * Deletes the selection from the drawing.
    */
    public void deleteSelection() {
       fView.drawing().removeAll(fView.selection());
       fView.clearSelection();
    }

   /**
    * Copies the selection to the clipboard.
    */
    public void copySelection() {
        FigureSelection selection = fView.getFigureSelection();
        Clipboard.getClipboard().setContents(selection);
    }

   /**
    * Inserts a vector of figures and translates them by the
    * given offset.
    */
    public void insertFigures(Vector figures, int dx, int dy) {
        FigureEnumeration e = new FigureEnumerator(figures);
        while (e.hasMoreElements()) {
            Figure figure = e.nextFigure();
            figure.moveBy(dx, dy);
            figure = fView.add(figure);
            fView.addToSelection(figure);
        }
    }

}


